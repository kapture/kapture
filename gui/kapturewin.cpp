/*
 *      kapturewin.cpp -- Kapture
 *
 *      Copyright (C) 2006-2007
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#include <QCheckBox>
#include <QAbstractButton>
#include <QImageReader>
#include <QMessageBox>

#include "mouselabel.h"
#include "kapturewin.h"
#include "webcam.h"
#include "pluginmanager.h"

/*
 * This Software is at a developpement state.
 * This is under GNU Public License.
 * You can use, modify or redistribute it.
 *
 * FIXME:
 * 	- Add comments in the code.
 * 	- The timer can be replaced by Dbus (Qt >= 4.2)
 * 	- Use the V4l2Control class written for meiacontroller
 * TODO:
 * 	- Add more goodies (B&W, ...)
 * 	- Add support for more controls (Pan-tilts,...)
 */

KaptureWin::KaptureWin()
	: QMainWindow()
{
	int i = 0;
	//FIXME:Video device won't be used any more, use the video device pool and let user choose.
	otherVideoDevice = false;
	for( ;i < qApp->arguments().size(); i++)
	{
		//printf("Element %i = %s\n", i, qApp->arguments().at(i).toLatin1().constData());
		if (qApp->arguments().at(i) == QString("-d"))
		{
			otherVideoDevice = true;
			videoDevice = qApp->arguments().at(i + 1);
		}
	}
	if (!otherVideoDevice)
		videoDevice = QString("/dev/video0");
	int defaultSat, defaultBright, defaultCont;

	crIsActivated = false;
	xmppWinCreated = false;
	ui.setupUi(this);
	capsDone = 0;

	ui.mainViewWidget->resize(320, 240);
	
	ui.comboBoxSize->clear();
	
	camera = new Webcam(videoDevice);
	getDeviceCapabilities(); // Open device and fill the param's comboBoxes (doesn't close it)

	camera->defaultCtrlVal(Webcam::Saturation, defaultSat); // Get default Saturation
	camera->defaultCtrlVal(Webcam::Brightness, defaultBright);  // Get default Brightness
	camera->defaultCtrlVal(Webcam::Contrast, defaultCont);  // Get default Contrast
	
	int defaultFreq, defaultSharp;
	camera->defaultCtrlVal(Webcam::PowerLineFreq, defaultFreq);  // Get default Frequency
	camera->defaultCtrlVal(Webcam::Sharpness, defaultSharp);  // Get default Sharpness
	panTiltSupported = false; //camera->panTiltSupported();

	/*
	 * FIXME:load those values from config.
	 */
	camera->changeCtrl(Webcam::Saturation, defaultSat); // Saturation to default
	camera->changeCtrl(Webcam::Brightness, defaultBright);  // Brightness to default
	camera->changeCtrl(Webcam::Contrast, defaultCont);  // Contrast to default
	camera->changeCtrl(Webcam::PowerLineFreq, defaultFreq);  // Frequency to default
	camera->changeCtrl(Webcam::Sharpness, defaultSharp);  // Contrast to default
	if (panTiltSupported)
		camera->changeCtrl(Webcam::PanTiltReset); // Reset to the center position

	ui.freqBox->setChecked((defaultFreq == 1) ? true : false);

	imageZoomed = new QImage(60, 60, QImage::Format_RGB32); 
	
	connect(ui.btnVideo,    SIGNAL(clicked () ), this, SLOT(startStopVideo() ) );
	connect(ui.btnPhoto,    SIGNAL(clicked () ), this, SLOT(savePhotoWait() ) );
    //connect(&waitCamera,    SIGNAL(timeout () ), this, SLOT(getDeviceCapabilities() ) );
	connect(&keepZoomer,    SIGNAL(timeout () ), this, SLOT(keepZoomerTimeOut()) );
	connect(camera,    	SIGNAL(imageReady () ), this, SLOT(getImage() ));
	if (panTiltSupported)
	{
		connect(ui.mainViewWidget, SIGNAL(turnRightEvent()), camera, SLOT(turnRight()));
		connect(ui.mainViewWidget, SIGNAL(turnLeftEvent()), camera, SLOT(turnLeft()));
		connect(ui.mainViewWidget, SIGNAL(turnDownEvent()), camera, SLOT(turnDown()));
		connect(ui.mainViewWidget, SIGNAL(turnUpEvent()), camera, SLOT(turnUp()));
	}
	/*else
	{
		ui.mainViewWidget->ui.rightBtn->hide();
		ui.mainViewWidget->ui.leftBtn->hide();
		ui.mainViewWidget->ui.upBtn->hide();
		ui.mainViewWidget->ui.downBtn->hide();
	}*/
	
	m_pluginManager = new PluginManager();
	ui.pluginsBox->setPluginManager(m_pluginManager);
}

KaptureWin::~KaptureWin()
{
    delete imageZoomed;
    delete m_pluginManager;
	delete camera;
}

void KaptureWin::mError(int ret)
{
	switch (ret)
	{
		case 16:
			ui.mainViewWidget->setText("Camera is already in use. You still can control the camera.");
			break;
		default :
			ui.mainViewWidget->setText("An unknown error occured.");
			printf("An unknown error occured (%d)", ret);
			break;
	}
}

void KaptureWin::getDeviceCapabilities()
{
	int selected = 0;
	int result;
	if ((result = camera->open()) == EXIT_SUCCESS)
	{
        //if(waitCamera.isActive())
            //waitCamera.stop();

		ui.comboBoxFormat->clear();
		ui.comboBoxSize->clear();

		formatList = camera->getFormatList(formatName);
		if (formatList.count() <= 0)
		{
			//cleanup();
			return;
		}
		
		for (int i = 0; i < formatList.size() ; i++)
			this->ui.comboBoxFormat->addItem(formatName.at(i));
	
		if(capsDone != 0)
			printf("\n");
		
		printf(" * Device is%s: %s\n", otherVideoDevice ? " [default] " : " ", videoDevice.toLatin1().constData());
		
		camera->setPixelFormat(formatList[0]);
		QList<QSize> sizes = camera->getSizesList();
		for (int i = 0; i < sizes.size(); i++)
		{
			this->ui.comboBoxSize->addItem(QString("%1x%2").arg(sizes.at(i).width()).arg(sizes.at(i).height()));
			if (sizes.at(i).width() == 320 && sizes.at(i).height() == 240)
			{
				selected = i;
				//camera->setFormat(320, 240, formatList.at(ui.comboBoxFormat->currentIndex()));
			}
		}
		if (sizes.count() <= 0)
		{
			//cleanup();
			return;
		}

		// I set the first frame size if the default size doesn't exist with the first format
		changeSize(ui.comboBoxSize->itemText(selected));
		ui.comboBoxSize->setCurrentIndex(selected);

		ui.btnPhoto->setEnabled(true);
		ui.btnVideo->setEnabled(true);
		ui.comboBoxSize->setEnabled(true);
		
		/*
		 * It must be connected here because if it doesn't find any webcam at start, 
		 * it will change format in this function even if no camera is pluged in.
		 * 
		 */
		
		connect(ui.comboBoxSize, SIGNAL( currentIndexChanged(const QString &) ), this, SLOT(changeSize(const QString &) ) );
		connect(ui.comboBoxFormat, SIGNAL( currentIndexChanged(const QString &) ), this, SLOT(changeFormat(const QString &) ) );
	}
	else
	{
		if (capsDone == 0)
		{
			ui.comboBoxSize->clear();
			ui.btnPhoto->setEnabled(false);
			ui.btnVideo->setEnabled(false);
			ui.comboBoxSize->setEnabled(false);
			printf(" * No webcam pluged in (unable to open %s)\n", videoDevice.toLatin1().constData());
			printf(" * Waiting for you .");
			fflush(stdout);
			ui.mainViewWidget->setText("No Webcam found !\n");
            //waitCamera.start(2000);
			//TODO: This utility could use QDbus...
		}
	}
	capsDone++;
}

void KaptureWin::changeSize(const QString& itemSelected)
{
	char checkCar;
	unsigned int newWidth = 0, newHeight = 0;
	
	printf(" * Selected Format : %s\n", itemSelected.toLatin1().constData());
	
	sscanf(itemSelected.toLatin1().constData(), "%d%c%d", &newWidth, &checkCar, &newHeight);
	if (checkCar != 'x')
		return;

	if (newWidth > 320 && newHeight > 240)
	{
		if (crIsActivated)
			crStartStop();
		ui.crButton->setEnabled(false);
	}
	else
	{
		ui.crButton->setEnabled(true);
	}

	if (camera->isStreaming())
	{
		camera->stopStreaming();
		int ret;
		if ((ret = camera->setSize(QSize(newWidth, newHeight))) == EXIT_FAILURE)
		{
			printf("***An error occured while setting size... Ret = %d (errno = %d)\n", ret, errno);
			ui.mainViewWidget->setText("A problem occured while setting size. Check the device.");
			ui.btnVideo->setText("Start video");
			mError(ret);
		}
		else
		{
			ui.mainViewWidget->resize(newWidth, newHeight);
			camera->startStreaming();
		}
	}
	else
	{
		printf("Resizing to %dx%d\n", newWidth, newHeight);
		ui.mainViewWidget->resize(newWidth, newHeight);
	}

	m_currentSize = QSize(newWidth, newHeight);
}

void KaptureWin::changeFormat(const QString & itemSelected)
{
	ui.comboBoxSize->disconnect(SIGNAL( currentIndexChanged(const QString &) ), this, SLOT(changeSize(const QString &) ));
	bool wasStreaming = false, found = false;
	int lastWidth  = camera->currentWidth();
	int lastHeight = camera->currentHeight();

	ui.comboBoxSize->clear();
	printf(" * Changing format to %s\n", itemSelected.toLatin1().constData());
	if (camera->isStreaming())
	{
		camera->stopStreaming();
		wasStreaming = true;
	}
	
	camera->setPixelFormat(formatList.at(ui.comboBoxFormat->currentIndex()));
	
	QList<QSize> sizes = camera->getSizesList();
	if (sizes.count() <= 0)
	{
		//cleanup();
		return;
	}
	
	this->ui.comboBoxSize->clear();

	for (int i = 0; i < sizes.size(); i++)
	{
		//sprintf(formatString, "%dx%d", sizes.at(i).width(), sizes.at(i).height() );
		this->ui.comboBoxSize->addItem(QString("%1x%2").arg(sizes.at(i).width()).arg(sizes.at(i).height()));
		if (sizes.at(i).width() == lastWidth && sizes.at(i).height() == lastHeight)
		{
			this->ui.comboBoxSize->setCurrentIndex(i);
			found = true;
		}
	}
	// I set the best little frame size if the last size doesn't still exists with the new format 
	if(!found)
	{
		this->ui.comboBoxSize->setCurrentIndex(0);
		changeSize(this->ui.comboBoxSize->itemText(0));
	}

	if (wasStreaming)
		camera->startStreaming();

	connect(ui.comboBoxSize, SIGNAL( currentIndexChanged(const QString &) ), this, SLOT(changeSize(const QString &) ) ); //Slot must be changeFormat()
}

void KaptureWin::startStopVideo()
{
	int ret = 0;
	if (!camera)
		return;

	if(camera->isStreaming())
	{
		if (camera->stopStreaming() == EXIT_SUCCESS)
		{
			ui.btnVideo->setText("Start video");
			keepZoomer.start(100);
		}
	}
	else
	{
		if (!camera->isOpened())
			camera->open();

		if (camera->currentSize().isEmpty())
		{
			camera->setSize(m_currentSize);
		}

		if (camera->currentPixelFormat() < 0)
		{
			camera->setPixelFormat(m_currentPixelFormat);
		}

		ret = camera->startStreaming();
		if (ret == EXIT_FAILURE)
		{
			ui.mainViewWidget->setText("An error occured while restarting the camera.");
			printf("***Unable to start streaming. Ret = %d\n", ret);
		}
		else
		{
			ui.btnVideo->setText("Stop video");
			keepZoomer.stop();
		}
	}
}

void KaptureWin::savePhotoWait()
{
	ui.btnPhoto->setEnabled(false);
	QTimer::singleShot(ui.waitDelay->value() * 1000, this, SLOT(savePhoto()));
}

void KaptureWin::savePhoto()
{
	char fileName[50];
	int i = 0;
	FILE *file;
	sprintf(fileName,"%s%s%d%s", getenv("HOME"), "/Kapture-", i, ".jpg");
	while((file = fopen(fileName,"r")) != NULL)
	{
		fclose(file);
		i++;
		sprintf(fileName,"%s%s%d%s", getenv("HOME"), "/Kapture-", i, ".jpg");
	}
	if(imageFromCamera.save(fileName, 0, 100))
		printf(" * File \"%s\" successfull saved.\n", fileName);
	else
		printf("/*\\Saving \"%s\" failed\n", fileName);
	
	ui.btnPhoto->setEnabled(true);
}

void KaptureWin::getImage()
{
    QPixmap pixmap = camera->getFrame();
    if (pixmap.isNull())
    {
        ui.comboBoxSize->disconnect(SIGNAL( currentIndexChanged(const QString &) ), this, SLOT(changeFormat(const QString &) ));
		startStopVideo();
		camera->close();
		capsDone = 0;
		//cleanup();
        //waitCamera.start(2000);
		return;
	}

    //QImage imageToShow;

	
    /*QList<QImage*> imgs;
	imgs << &imageFromCamera;
    m_pluginManager->applyPlugins(imgs);*/

    /*imageToShow = processZoom(imageFromCamera);

    pixmap = QPixmap::fromImage(imageToShow);

    pixmap = QPixmap::fromImage(imageFromCamera);*/

	if (!pixmap.isNull())
	{
		ui.mainViewWidget->setPixmap(pixmap);
		/*if(crIsActivated)
		{
			//Show the modified Picture (Color Replaced).
			showColorReplaced();
		}*/
	}
}

QImage KaptureWin::processZoom(QImage& image)
{
	QImage mainImage;
	int i, j;
	int X = ui.mainViewWidget->curPosX();
	int Y = ui.mainViewWidget->curPosY();
	char pos[20];
	char rgbPixelText[30];
	

	// Repositioning the square in the frame.
	if(X >= ui.mainViewWidget->width()-8)
		X = ui.mainViewWidget->width()-9;
	if(Y >= ui.mainViewWidget->height()-9)
		Y = ui.mainViewWidget->height()-9;

	if(X < 8)
		X = 8;
	if(Y < 8)
		Y = 8;
	
	// Do zoom
	ui.zoomFrameLabel->setPixmap(zoomAtCursor(X, Y, image));
	
	// Show Square
	for(i = (X-8); i <= (X + 8); i++)
	{
		for(j = (Y-8); j <= (Y + 8); j++)
		{
			if((i == (X-8)) || (i == (X+8)))
				image.setPixel(i, j, qRgb(255, 0, 0));
			else
				if((j == (Y-8)) || (j == (Y+8)))
					image.setPixel(i, j, qRgb(255, 0, 0));
			
		}
	}

	sprintf(pos, "Position : %dx%d", X, Y);
	ui.zoomPosLabel->setText(pos);
	sprintf(rgbPixelText, "R : %d, G : %d, B : %d",qRed(image.pixel(X, Y)),
			qGreen(image.pixel(X, Y)),
			qBlue(image.pixel(X, Y)));
	ui.rgbPixelLabel->setText(rgbPixelText);
	/*if (!crIsActivated)
	{
		QPalette palette;
		palette.setColor(QPalette::Foreground, QColor ("black"));
		ui.rgbPixelLabel->setPalette(palette);
	}*/

	
	return image;
}

QPixmap KaptureWin::zoomAtCursor(int xCurPos, int yCurPos, QImage imageToZoomIn) // Return the little zoomed frame.
{
	/*if (camera->currentWidth() > 320 && camera->currentHeight() > 240)
		return QPixmap::QPixmap(60, 60);*/

	int X,Y,i=0,j=0;
	X = xCurPos;
	Y = yCurPos;
	if((X > 7) && (X < 313) && (Y > 7) && (Y < 233))
	{
		// We place the seeker at (0,0)
		X = X - 8;
		Y = Y - 8;
		
		for(; i<60; i++) // Lines
		{
			for(; j<60; j++) // Columns
			{
				if((j%4) == 0)
					X++;
				if( (i>24) && (j>23) && (j<36) && (i<37) && (i!=29) && (i!=30) && (i!=31) && (i!=32) && (j!=28) && (j!=29) && (j!=30) && (j!=31) )
				{
					imageZoomed->setPixel(j, i, qRgb(0, 0, 0) );
				}
				else
				{
					imageZoomed->setPixel(j, i, imageToZoomIn.pixel(X,Y) );
				}

			}
			j = 0;
			X = xCurPos - 8; // x is reinitialized.
			if ((i%4) == 0)
				Y++;
		}
	}

	return QPixmap::fromImage(*imageZoomed);
}

void KaptureWin::crStartStop()
{
/*
	if(crIsActivated)
	{
		crIsActivated = false;
		this->resize(this->width(), 120);
		ui.crBox->hide();
	}
	else
	{
		crIsActivated = true;
		this->resize(this->width(), 500);
		ui.crBox->show();
	}
*/
}

void KaptureWin::colorChanged()
{
/*
	QPixmap *color = new QPixmap(42, 42);
	color->fill(QColor(ui.redSlider->sliderPosition(), 
			   ui.greenSlider->sliderPosition(), 
			   ui.blueSlider->sliderPosition(),255 ));
	this->ui.colorShow->setPixmap(*color);
	ui.redSliderValue->display(ui.redSlider->sliderPosition() );
	ui.greenSliderValue->display(ui.greenSlider->sliderPosition() );
	ui.blueSliderValue->display(ui.blueSlider->sliderPosition() );
	color->~QPixmap();
*/
}


void KaptureWin::showColorReplaced()
{
/*
	int height;
	int width;
	struct levels {
		int red ;
		int green ;
		int blue ;
	} level;
	QImage image;
	QRgb qrgb;	
	int lwRedValue, lwGreenValue, lwBlueValue, lwRedSeverity, lwGreenSeverity, lwBlueSeverity;
	QPalette palette;
	
	if(camera->isStreaming())
		image = ui.mainViewWidget->pixmap()->toImage();
	else
		image = imageFromCamera;

	crImage.fill(0);
	height = image.height();
	width  = image.width();
	
	lwRedValue = ui.redSlider->sliderPosition();
	lwGreenValue = ui.greenSlider->sliderPosition(); 
	lwBlueValue = ui.blueSlider->sliderPosition();
	lwRedSeverity = ui.redSeverityBox->value();
	lwGreenSeverity = ui.greenSeverityBox->value();
	lwBlueSeverity = ui.blueSeverityBox->value();
					
	palette.setColor(QPalette::Foreground, QColor ("red"));
	ui.rgbPixelLabel->setPalette(palette);
				
	int j = 0;
	for(int i = 0; i < width; i++)
	{
		for(j = 0; j < height; j++)
		{
			qrgb = image.pixel(i, j);
			
			// Check the pixel
			if(((level.red = qRed(qrgb)) <= (lwRedValue + lwRedSeverity)) && 
					((level.green = qGreen(qrgb)) <= (lwGreenValue + lwGreenSeverity)) && 
					((level.blue = qBlue(qrgb)) <= (lwBlueValue + lwBlueSeverity)) && 
					((level.red = qRed(qrgb)) >= (lwRedValue - lwRedSeverity)) && 
					((level.green = qGreen(qrgb)) >= (lwGreenValue - lwGreenSeverity)) && 
					((level.blue = qBlue(qrgb)) >= (lwBlueValue - lwBlueSeverity)))
			{
				if((i < 319) &&  (image.pixel((i + 1), j) != qRgb(0, 0, 255)))
					crImage.setPixel((i + 1), j, qRgb(0, 0, 128));
				if((i > 0) &&  (image.pixel((i - 1), j) != qRgb(0, 0, 255)))
					crImage.setPixel((i - 1), j, qRgb(0, 0, 128));
				crImage.setPixel(i, j, qRgb(0,0,255));

				// Color the label showing RGB codes in green when it correspond.
				if ((ui.mainViewWidget->curPosX() == i) && (ui.mainViewWidget->curPosY() == j))
				{
					palette.setColor(QPalette::Foreground, QColor ("green"));
					ui.rgbPixelLabel->setPalette(palette);
				}
			}
		}
	}
	ui.crFrameLabel->setPixmap(QPixmap::fromImage(crImage));
*/
}

void KaptureWin::keepZoomerTimeOut()
{
	//showZoom();
	//if(crIsActivated)
	//	showColorReplaced();
}

/*
void KaptureWin::satChanged()
{
	camera->changeCtrl(0, ui.satManualValueBox->value());
}

void KaptureWin::brightChanged()
{
	camera->changeCtrl(2, ui.brightManualValueBox->value());
}

void KaptureWin::contChanged()
{
	camera->changeCtrl(3, ui.contManualValueBox->value());
}

void KaptureWin::sharpChanged()
{
	camera->changeCtrl(4, ui.sharpManualValueBox->value());
}
*/

void KaptureWin::freqChanged()
{
	camera->changeCtrl(1, ui.freqBox->isChecked() ? 1 : 2);
}

void KaptureWin::closeEvent(QCloseEvent*)
{
	printf("\n * Exiting...\n");
	if (camera->isStreaming())
		startStopVideo();
	camera->close();
	//((QApplication*) this->parentWidget())->quit();
}
