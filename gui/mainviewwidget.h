/*
 *      mainvideowidget.h -- Kapture
 *
 *      Copyright (C) 2006-2007
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#ifndef MAINVIEWWIDGET_H
#define MAINVIEWWIDGET_H

#include <QMainWindow>
#include <QTimer>
#include <unistd.h>

#include "ui_mainviewwidget.h"
#include "webcam.h"

class MainViewWidget : public QWidget
{
	Q_OBJECT
public:
	MainViewWidget();
	~MainViewWidget();

signals:
	void turnRightEvent();
	void turnLeftEvent();
	void turnUpEvent();
	void turnDownEvent();

private:
	Ui::mainViewWidget ui;
	int posCurX;
	int posCurY;
};

#endif // MAINVIEWWIDGET_H

