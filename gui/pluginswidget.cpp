#if QT_VERSION >= 0x050000
#include <QtWidgets>
#else
#include <QHBoxLayout>
#include <QCheckBox>
#include <QLabel>
#endif

#include "pluginswidget.h"
#include "pluginmanager.h"
#include "imageinterface.h"

PluginsWidget::PluginsWidget(QWidget *parent)
 : QGroupBox(parent)
{

}


PluginsWidget::~PluginsWidget()
{

}

void PluginsWidget::setPluginManager(PluginManager* pmanager)
{
	m_pluginManager = pmanager;

	QHBoxLayout *hLayout = new QHBoxLayout(this);

	if (!m_pluginManager || m_pluginManager->plugins().count() == 0)
	{
		hLayout->addWidget(new QLabel("No plugins found", this));
		hLayout->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

		return;
	}

	foreach(ImageInterface *ii, m_pluginManager->plugins())
	{
		QCheckBox *cb = new QCheckBox(ii->name(), this);
		cb->setCheckState(ii->active() ? Qt::Checked : Qt::Unchecked);
		connect(cb, SIGNAL(stateChanged(int)), this, SLOT(updatePlugins()));

		hLayout->addWidget(cb);
	}
	
	hLayout->addItem(new QSpacerItem(20, 40, QSizePolicy::Expanding, QSizePolicy::Minimum));
}

void PluginsWidget::updatePlugins()
{
	QCheckBox *cb = dynamic_cast<QCheckBox*>(sender());
	if (!cb)
		return;

	if (cb->checkState() == Qt::Unchecked)
		m_pluginManager->stopPlugin(cb->text());
	else
		m_pluginManager->startPlugin(cb->text());
}
