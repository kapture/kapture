#ifndef PLUGINS_MANAGER_H
#define PLUGINS_MANAGER_H

#include <QGroupBox>

class PluginManager;

class PluginsWidget : public QGroupBox
{
	Q_OBJECT
public:
	PluginsWidget(QWidget *parent = 0);
	~PluginsWidget();

	void setPluginManager(PluginManager*);

private slots:
	void updatePlugins();

private:
	PluginManager *m_pluginManager;
};


#endif
