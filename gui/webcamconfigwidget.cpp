#include "webcamconfigwidget.h"
#include "webcampool.h"
#include "webcam.h"

#include <stdio.h>

WebcamConfigWidget::WebcamConfigWidget(QWidget *parent)
 : QWidget(parent), camera(0)
{
	ui.setupUi(parent);
	ui.pictureLabel->setAlignment(Qt::AlignCenter);
	ui.pictureLabel->setText("Initializing the webcam...");
	ui.pictureLabel->resize(320, 280);
	
	m_isStreaming = false;

	pool = WebcamPool::self();
	if (pool->count() <= 0)
	{
		ui.pictureLabel->setText("No camera found...");
		return;
	}
	
	pool->fillDeviceComboBox(ui.comboBoxDevs);
	camera = pool->device(0);
	start();
}

WebcamConfigWidget::~WebcamConfigWidget()
{
	if (camera)
		camera->close();
}

bool WebcamConfigWidget::streaming() const
{
	return m_isStreaming;
}

void WebcamConfigWidget::start()
{
	printf("Starting camera\n");
	
	if (camera->open() != EXIT_SUCCESS)
	{
		printf("Error opening video device\n");
		return;
	}

	QList<QString> formatName;
	QList<int> formatList = camera->getFormatList(formatName);
	ui.comboBoxFormat->clear();
	for (int i = 0; i < formatList.size(); i++)
		ui.comboBoxFormat->addItem(formatName.at(i));
	
	camera->setPixelFormat(formatList.at(0));

	QList<QSize> sizeList = camera->getSizesList();
	for (int i = 0; i < sizeList.size(); i++)
		ui.comboBoxSize->addItem(QString("%1x%2").arg(sizeList.at(i).width()).arg(sizeList.at(i).height()));
	
	camera->setSize(sizeList.at(0));

	connect(camera, SIGNAL(imageReady()), SLOT(slotImageReady()));

	if (camera->startStreaming())
		m_isStreaming = true;
	else
		m_isStreaming = false;

}

void WebcamConfigWidget::slotImageReady()
{
    //QImage image = camera->getFrame();
    ui.pictureLabel->setPixmap(camera->getFrame());
}

void WebcamConfigWidget::stop()
{
	if (!camera)
		return;
	
	camera->stopStreaming();
}
