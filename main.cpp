/*
 *      main.cpp -- Kapture
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova <detlev.casanova@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */
#include <QApplication>
#include <QMainWindow>
#include <QFrame>

#include "stunclient.h"

#include "kapturewin.h"
#include "xmppwin.h"

int main(int argc, char **argv)
{
/*
	QString pwd = "DavidEtDetlev";
	StunMessage m(StunMessage::BindingRequest);
	m.addAttribute(Attribute::Priority, (uint32_t) 0x89abcdef);
	m.addAttribute(Attribute::UseCandidate, QByteArray());
	m.authenticate(pwd);

	QByteArray data = m.toNetworkBytes();

	qDebug() << data.toBase64();

	StunMessage m2(data, pwd);
	qDebug() << m2.getInt(Attribute::Priority);
	qDebug() << m2.hasAttribute(Attribute::UseCandidate);
*/
	QApplication app(argc, argv);
	int ret = 0;

	KaptureWin *kw = NULL;
	XmppWin *xw = NULL;

	if (argc > 1 && strcmp(argv[1], "--xmpp") == 0)
	{
		xw = new XmppWin();
		xw->show();
	}
	else
	{
		kw = new KaptureWin();
		kw->show();
	}

	ret = app.exec();

	if (kw == NULL)
		delete xw;
	else
		delete kw;

	return ret;
}
