#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QString>
#include <QList>

class Emoticons : public QObject
{
public:
	Emoticons(QObject *parent = 0);
	QString changeEmoticons(const QString&);
private:
	struct Emoticon
	{
		QString binette; // :-)
		QString link; // smile.png
		Emoticon(QString b, QString l)
		{
			binette = b;
			link = l;
		}
	};
	QList<Emoticon> emoticons;
	
	~Emoticons() {}
};

#endif
