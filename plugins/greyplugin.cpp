#include <QtGui>

#include "greyplugin.h"

bool GreyPlugin::applyChanges(const QList<QImage*> &images)
{
	foreach(QImage* img, images)
	{
		img->invertPixels();
	}

	return true;
}

//Q_EXPORT_PLUGIN2(greyplugin, GreyPlugin);
