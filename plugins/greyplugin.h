#ifndef GREY_PLUGIN_H
#define GREY_PLUGIN_H

#include <QObject>

#include "imageinterface.h"

class GreyPlugin : public QObject, ImageInterface
{
	Q_OBJECT
	Q_INTERFACES(ImageInterface)

public:	
	GreyPlugin() {}

	virtual QString name() const {return "Grey Plugin";}
	virtual QString version() const {return "1.0";}
	virtual QString description() const {return "Image greying plugin";}
	//virtual QStringList parameters() const {return QStringList();}
	virtual bool applyChanges(const QList<QImage*>&);
	//virtual int imageInputCount() const {return 1;}
	//virtual int imageOutputCount() const {return 1;}

};

#endif
