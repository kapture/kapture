#ifndef IMAGE_INTERFACE_H
#define IMAGE_INTERFACE_H

#include <QStringList>
#include <QImage>

class ImageInterface
{
public:
	ImageInterface() : m_active(true) {}
	virtual ~ImageInterface() {}

	virtual QString name() const = 0;
	virtual QString version() const = 0;
	virtual QString description() const = 0;
	virtual QStringList parameters() const {return QStringList();}
	virtual bool applyChanges(const QList<QImage*>&) = 0;
	virtual int imageInputCount() const {return 1;}
	virtual int imageOutputCount() const {return 1;}
	void setActive(bool a) {m_active = a;}
	bool active() const {return m_active;}

private:
	bool m_active;

};

Q_DECLARE_INTERFACE(ImageInterface, "org.Kapture.Plugin.ImageInterface/1.0");

#endif
