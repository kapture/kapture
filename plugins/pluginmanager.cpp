#include <QApplication>
#include <QPluginLoader>
#include <QDir>
#include <QDebug>

#include "pluginmanager.h"
#include "imageinterface.h"

PluginManager::PluginManager()
{
//FIXME:only linux compatible

	QDir pluginsDir(qApp->applicationDirPath());
	foreach (QString fileName, pluginsDir.entryList(QDir::Files))
	{
		QPluginLoader pluginLoader(pluginsDir.absoluteFilePath(fileName));
		QObject *plugin = pluginLoader.instance();
		if (plugin)
		{
			ImageInterface *imageInterface = qobject_cast<ImageInterface*>(plugin);
			if (imageInterface)
			{
				m_plugins << imageInterface;
			}
		}
	//	else
	//		qDebug() << pluginLoader.errorString();
	}

	qDebug() << "Found" << m_plugins.count() << "suitable plugins :";
	foreach(ImageInterface* ii, m_plugins)
	{
		qDebug() << ii->name() << "version" << ii->version();
	}

}

PluginManager::~PluginManager()
{
}


void PluginManager::init()
{

}

QList<ImageInterface*> PluginManager::plugins()
{
	return m_plugins;
}

void PluginManager::applyPlugins(QList<QImage*> &imgs)
{
	foreach(ImageInterface *plugin, m_plugins)
	{
		if (plugin->active())
			plugin->applyChanges(imgs);
	}
}

void PluginManager::stopPlugin(int pos)
{
	m_plugins[pos]->setActive(false);
}

void PluginManager::stopPlugin(const QString& name)
{
	foreach(ImageInterface *plugin, m_plugins)
	{
		if (plugin->name() == name)
			plugin->setActive(false);
	}
}

void PluginManager::startPlugin(int pos)
{
	m_plugins[pos]->setActive(true);
}

void PluginManager::startPlugin(const QString& name)
{
	foreach(ImageInterface *plugin, m_plugins)
	{
		if (plugin->name() == name)
			plugin->setActive(true);
	}
}
