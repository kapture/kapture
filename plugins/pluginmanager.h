#ifndef PLUGIN_MANAGER_H
#define PLUGIN_MANAGER_H

#include <QObject>
#include <QList>

class ImageInterface;
class QImage;

class PluginManager : public QObject
{
	Q_OBJECT
public:
	PluginManager();
	~PluginManager();

	void init();
	QList<ImageInterface*> plugins();

	void applyPlugins(QList<QImage*>&);
	void stopPlugin(int);
	void stopPlugin(const QString&);
	void startPlugin(int);
	void startPlugin(const QString&);

private:
	QList<ImageInterface*> m_plugins;

};

#endif
