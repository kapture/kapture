/*
 *      webcam.cpp -- Kapture
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/select.h>

#include <linux/videodev2.h>

#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QImage>
#include <QPixmap>
#include <QLabel>
#include <QSize>
#include <QSocketNotifier>

#include "webcam.h"
#include "imageconvert.h"
#include "merror.h"


Webcam::Webcam(const QString& devName)
 : dev(0),
   allocated(false),
   streaming(false),
   opened(false),
   imageNotifier(0),
   m_currentPixelFmt(-1),
   m_currentBufLength(0),
   m_jpegBuf1(0)

{
	m_currentSize = QSize(0, 0);
	m_name = devName;
}

Webcam::~Webcam()
{
	delete [] m_jpegBuf1;
	close();
}

void Webcam::close()
{
	stopStreaming();
	::close(dev);

	delete imageNotifier;
	imageNotifier = 0;
	opened = false;
	allocated = false;
}

int Webcam::open()
{
	struct v4l2_capability cap;
	int ret;
	char str[256];

	if (opened)
		return EXIT_SUCCESS;

	dev = ::open(m_name.toLocal8Bit().constData(), O_RDWR);
	if (dev < 0) 
	{
		strcpy(str, "Error Opening ");
		//KError(strcat(str, devFile), errno);
		return EXIT_FAILURE;
	}

        memset(&cap, 0, sizeof cap);
        ret = ioctl(dev, VIDIOC_QUERYCAP, &cap);
        if (ret < 0) 
	{
		strcpy(str, "Error querying capabilities for ");
		//KError(strcat(str, devFile), errno);
                return EXIT_FAILURE;
	}

	if ((cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) == 0) 
	{
		strcpy(str, "Error checking caps for ");
		//KError(strcat(str, devFile), errno);
                return -EINVAL;
	}
	opened = true;

	imageNotifier = new QSocketNotifier(dev, QSocketNotifier::Read);
	imageNotifier->setEnabled(false);
	connect(imageNotifier, SIGNAL(activated(int)), this, SIGNAL(imageReady()));

	return EXIT_SUCCESS;
}

QList<int> Webcam::getFormatList(QList<QString> &description) const
{
	QList<int> formatList;
	int ret;
	struct v4l2_fmtdesc fmtList;
	memset(&fmtList, 0, sizeof fmtList);
	fmtList.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	int i = 0;

	do
	{
		fmtList.index = i;
		if ((ret = ioctl(dev, VIDIOC_ENUM_FMT, &fmtList)) < 0)
			break;
		else
		{
			formatList.append((int)fmtList.pixelformat);
			description.append((char*)fmtList.description);
		}
		i++;
	}
	while (ret != EINVAL);
	return formatList;
}

QList<QSize> Webcam::getSizesList() const
{
#ifdef VIDIOC_ENUM_FRAMESIZES
	int i = 0;
	QList<QSize> rSizes;
	QSize tmp;
	struct v4l2_frmsizeenum sizes;
	memset(&sizes, 0, sizeof sizes);
	sizes.pixel_format = currentPixelFormat();
	sizes.index = i;
	while(ioctl(dev, VIDIOC_ENUM_FRAMESIZES, &sizes) != -1)
	{
		tmp.setWidth((int)sizes.discrete.width);
		tmp.setHeight((int)sizes.discrete.height);
		rSizes.append(tmp);
		i++;
		sizes.index = i;
	}
	return rSizes;
#else
	QList<QSize> rSizes;
	QSize tmp;

	tmp.setWidth(320);
	tmp.setHeight(240);
	
	rSizes << tmp;

	return rSizes;
#endif
}

int Webcam::setPixelFormat(int pixelformat)
{
	if(streaming)
		return EXIT_FAILURE;
	
	m_currentPixelFmt = pixelformat;
	
	if (m_currentSize.isEmpty())
		return EXIT_FAILURE;
	
	v4l2_format fmt;
	memset(&fmt, 0, sizeof fmt);
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = m_currentSize.width();
	fmt.fmt.pix.height = m_currentSize.height();
	fmt.fmt.pix.field = V4L2_FIELD_ANY;
	fmt.fmt.pix.pixelformat = pixelformat;
	if (ioctl(dev, VIDIOC_S_FMT, &fmt) < 0)
	{
		KError("Error while setting pixel format", errno);
		m_currentPixelFmt = -1;
		
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int Webcam::setSize(const QSize &size)
{
	if(streaming)
	{
		printf("Still streaming.");
		return EXIT_FAILURE;
	}

	m_currentSize = size;

	if (m_currentPixelFmt <= 0)
	{
		printf("Invalid pixelfmt.\n");
		return EXIT_FAILURE;
	}

	v4l2_format fmt;
	memset(&fmt, 0, sizeof fmt);
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = m_currentSize.width();
	fmt.fmt.pix.height = m_currentSize.height();
	fmt.fmt.pix.field = V4L2_FIELD_ANY;
	fmt.fmt.pix.pixelformat = m_currentPixelFmt;
	if (ioctl(dev, VIDIOC_S_FMT, &fmt) < 0)
	{
		KError("Error while setting size", errno);
		m_currentSize.setWidth(-1);
		m_currentSize.setHeight(-1);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int Webcam::streamOff()
{
	int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	int ret;
	
	if(!streaming)
		return -1;
	
	ret = ioctl(dev, VIDIOC_STREAMOFF, &type);
	if (ret < 0) 
	{
		KError("Unable to stop capture", errno);
		return EXIT_FAILURE;
	}
	streaming = false;
	return EXIT_SUCCESS;
}

int Webcam::startStreaming()
{
	int i, ret;

	if (!opened)
		open();

	if (m_currentPixelFmt < 0 || m_currentSize.isEmpty())
	{
		printf("Format error, Invalid argument. Unable to start.\n");
		return EXIT_FAILURE;
	}

	//Allocate buffers 
	if (!allocated)
	{
		memset(&rb, 0, sizeof rb);
		rb.count = 2;
		rb.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		rb.memory = V4L2_MEMORY_MMAP;

		ret = ioctl(dev, VIDIOC_REQBUFS, &rb);
		if (ret < 0) 
		{
			KError("Unable to allocate buffers", errno);
			return EXIT_FAILURE;
		}
		allocated = true;
	}


	// Map the buffers. /
	memset(&buf, 0, sizeof buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	for (i = 0; i < 2; i++)
	{
		buf.index = i; 
		ret = ioctl(dev, VIDIOC_QUERYBUF, &buf);
		if (ret < 0) {
			KError("Unable to query buffer", errno);
			return EXIT_FAILURE;
		}
		
		mem[i] = (uchar *) mmap(0, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, dev, buf.m.offset);
		if (mem[i] == MAP_FAILED) {
			KError("Unable to map buffer", errno);
			return EXIT_FAILURE;
		}
		bufLength = buf.length;
		mmaped = true;
	}

	// Queue the buffers. /
	for (i = 0; i < 2; i++)
	{
		buf.index = i;
		ret = ioctl(dev, VIDIOC_QBUF, &buf);
		if (ret < 0) 
		{
			KError("Unable to queue buffer", errno);
			return EXIT_FAILURE;
		}
	}

	// Start streaming.
	ret = ioctl(dev, VIDIOC_STREAMON, &buf.type);
	if (ret < 0) 
	{
		KError("Unable to start capture", errno);
		return EXIT_FAILURE;
	}
	
	imageNotifier->setEnabled(true);
	streaming = true;
	return EXIT_SUCCESS;
}

int Webcam::stopStreaming()
{
	if(!streaming)
		return -1;
	
	imageNotifier->setEnabled(false);
	
	if(streamOff() == 0)
	{
		streaming = false;
		printf(" * Succesfuly Stopped\n");
	}
	else
		imageNotifier->setEnabled(true);
	
	if (munmap(mem[0], bufLength) == -1)
	{
		printf("Webcam::stopStreaming : munmap 0 failed. errno = %d\n", errno);
	}

	if (munmap(mem[1], bufLength) == -1)
	{
		printf("Webcam::stopStreaming : munmap 1 failed. errno = %d\n", errno);
	}
	else
		mmaped = false;

	// Deallocate buffers 
	if (allocated)
	{
		memset(&rb, 0, sizeof rb);
		rb.count = 0;
		rb.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		rb.memory = V4L2_MEMORY_MMAP;
	
		int ret = ioctl(dev, VIDIOC_REQBUFS, &rb);
		if (ret < 0) 
		{
			//Some drivers do not support setting 0 buffers, closing in that case.
			close();
		}
		allocated = false;
	}

	return EXIT_SUCCESS;
}

QPixmap Webcam::getFrame()
{
	int ret = 0;
    QPixmap pixmap(currentWidth(), currentHeight());

	// Dequeue a buffer.
	ret = ioctl(dev, VIDIOC_DQBUF, &buf);
	if (ret < 0) 
	{
		KError("Unable to dequeue buffer", errno);
        return QPixmap();
	}

	// Save the image.
	if (m_currentPixelFmt == V4L2_PIX_FMT_MJPEG)
	{
		if (m_currentBufLength != buf.bytesused + 420 || m_jpegBuf1 == 0)
		{
			delete [] m_jpegBuf1;
			m_jpegBuf1 = new uchar[buf.bytesused + 420];
		}

        if (mjpegToJpeg(mem[buf.index], m_jpegBuf1, (int) buf.bytesused) == EXIT_SUCCESS)
            pixmap.loadFromData(m_jpegBuf1, buf.bytesused + 420);
	}
    else if (m_currentPixelFmt == V4L2_PIX_FMT_YUYV)
        pixmap = yuvToJpeg(mem[buf.index], currentWidth(), currentHeight());
	
	// Requeue the buffer.
	ret = ioctl(dev, VIDIOC_QBUF, &buf);

    if (ret < 0)
	{
		KError("Unable to requeue buffer", errno);
        return QPixmap();
	}

	if(!imageNotifier->isEnabled())
		imageNotifier->setEnabled(true);
	
    return pixmap;
}

int Webcam::changeCtrl(int ctrl, int value) // an enum for formats and reorganisation would be great...
{
	struct v4l2_queryctrl queryctrl;
	struct v4l2_control control;
	
	if(!opened) // At the begining of the function.
	{
		return -1;
	}
/*
 * ctrl values :
 * 	0 : Saturation
 * 	1 : Power line Frequency (néons)
 * 	2 : Brightness
 * 	3 : Contrast
 * 	4 : Sharpness
 * 	5 : Reset Pan/Tilt
 */
	__u32 CTRL;
	switch(ctrl)
	{
		case Saturation: 
		{
			CTRL = V4L2_CID_SATURATION;
			break;
		}
		case Brightness: 
		{
			CTRL = V4L2_CID_BRIGHTNESS;
			break;
		}
		case Contrast: 
		{
			CTRL = V4L2_CID_CONTRAST;
			break;
		}
#ifdef V4L2_CID_POWER_LINE_FREQUENCY
		case PowerLineFreq: 
		{
			CTRL = V4L2_CID_POWER_LINE_FREQUENCY;
			break;
		}
#endif
#ifdef V4L2_CID_SHARPNESS
		case Sharpness:
		{
			CTRL = V4L2_CID_SHARPNESS;
			break;
		}
#endif
#ifdef V4L2_CID_PANTILT_RESET
		case PanTiltReset:
		{
			CTRL = V4L2_CID_PANTILT_RESET;
			value = 3;
			break;
		}
#endif
		default:
			CTRL = 0;
	}

	memset (&queryctrl, 0, sizeof queryctrl);
	memset (&control, 0, sizeof control);
	queryctrl.id = CTRL;
	if (-1 == ioctl (dev, VIDIOC_QUERYCTRL, &queryctrl)) 
	{
	        if (errno != EINVAL) 
		{
#ifdef DEBUG
			perror ("VIDIOC_QUERYCTRL");
#endif
			return EXIT_FAILURE;
		} 
	} else 
	{
		control.id = CTRL;
		control.value = value;
		if (-1 == ioctl (dev, VIDIOC_S_CTRL, &control)) {
#ifdef DEBUG
			perror("VIDIOC_S_CTRL");
			printf(" * Error while setting control\n");
#endif
			return EXIT_FAILURE;
        	}
	}
	return EXIT_SUCCESS;
}

QSize Webcam::currentSize() const
{
	return m_currentSize;
}

int Webcam::currentWidth() const
{
	return m_currentSize.width();
}

int Webcam::currentHeight() const
{
	return m_currentSize.height();
}

int Webcam::currentPixelFormat() const
{
	return m_currentPixelFmt;
}


int Webcam::defaultCtrlVal(unsigned int control, int &defaultValue)
{
	struct v4l2_queryctrl queryctrl;
	QString ctrl;
	
	if(!opened)
	{
		return false;
	}
	
	memset(&queryctrl, 0, sizeof queryctrl);
	switch(control)
	{
		case Saturation : 
		{
			ctrl = "Saturation";
			queryctrl.id = V4L2_CID_SATURATION;
			break;
		}
		case Brightness : 
		{
			ctrl = "Brightness";
			queryctrl.id = V4L2_CID_BRIGHTNESS;
			break;
		}
		case Contrast : 
		{
			ctrl = "Contrast";
			queryctrl.id = V4L2_CID_CONTRAST;
			break;
		}
#ifdef V4L2_CID_POWER_LINE_FREQUENCY
		case PowerLineFreq : 
		{
			ctrl = "Powerline Frequecy";
			queryctrl.id = V4L2_CID_POWER_LINE_FREQUENCY;
			break;
		}
#endif
#ifdef V4L2_CID_SHARPNESS
		case Sharpness : 
		{
			ctrl = "Sharpness";
			queryctrl.id = V4L2_CID_SHARPNESS;
			break;
		}
#endif
		default :
			ctrl = "ERROR";
	}

	QString str;
	if (-1 == ioctl(dev, VIDIOC_QUERYCTRL, &queryctrl))
	{
		str = "Unable to set control ";
		printf("ERROR\n");
		KError(str + ctrl, errno);
		return false;
	}

	defaultValue = (int)queryctrl.default_value;

	return true;
}

bool Webcam::panTiltSupported()
{
#ifdef V4L2_CID_TILT_RELATIVE
	struct v4l2_queryctrl queryctrl;

	if(!opened)
	{
		return false;
	}

	memset(&queryctrl, 0, sizeof queryctrl);
	queryctrl.id = V4L2_CID_TILT_RELATIVE; // Could be V4L2_CID_PAN_RELATIVE.

	if (-1 == ioctl(dev, VIDIOC_QUERYCTRL, &queryctrl))
	{
		KError("Unable to check wether Pan Tilt is supported.", errno);
		return false;
	}

	if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
	{
		printf ("Pan & Tilt not supported.\n");
		return false; //FLAG_NOT_SUPPORTED;
	}

	return true;
#else
	return false;
#endif

}

void Webcam::turnRight()
{
#ifdef V4L2_CID_PAN_RELATIVE
	struct v4l2_queryctrl queryctrl;
	struct v4l2_control control;

	memset (&queryctrl, 0, sizeof queryctrl);
	memset (&control, 0, sizeof control);
	queryctrl.id = V4L2_CID_PAN_RELATIVE;
	if (-1 == ioctl (dev, VIDIOC_QUERYCTRL, &queryctrl)) 
	{
	        if (errno != EINVAL) 
		{
			perror ("VIDIOC_QUERYCTRL");
			return;
		} 
	} else 
	{
		control.id = V4L2_CID_PAN_RELATIVE;
		control.value = -320;
		if (-1 == ioctl (dev, VIDIOC_S_CTRL, &control)) {
			perror("VIDIOC_S_CTRL");
			return;
        	}
	}
#endif
}

void Webcam::turnLeft()
{
#ifdef V4L2_CID_PAN_RELATIVE
	struct v4l2_queryctrl queryctrl;
	struct v4l2_control control;

	memset (&queryctrl, 0, sizeof queryctrl);
	memset (&control, 0, sizeof control);
	queryctrl.id = V4L2_CID_PAN_RELATIVE;
	if (-1 == ioctl (dev, VIDIOC_QUERYCTRL, &queryctrl)) 
	{
	        if (errno != EINVAL) 
		{
			perror ("VIDIOC_QUERYCTRL");
			return;
		} 
	} else 
	{
		control.id = V4L2_CID_PAN_RELATIVE;
		control.value = 320;
		if (-1 == ioctl (dev, VIDIOC_S_CTRL, &control)) {
			perror("VIDIOC_S_CTRL");
			return;
        	}
	}
#endif
}

void Webcam::turnUp()
{
#ifdef V4L2_CID_TILT_RELATIVE
	struct v4l2_queryctrl queryctrl;
	struct v4l2_control control;
	
	memset (&queryctrl, 0, sizeof queryctrl);
	memset (&control, 0, sizeof control);
	queryctrl.id = V4L2_CID_TILT_RELATIVE;
	if (-1 == ioctl (dev, VIDIOC_QUERYCTRL, &queryctrl)) 
	{
	        if (errno != EINVAL) 
		{
			perror ("VIDIOC_QUERYCTRL");
			return;
		} 
	} else 
	{
		control.id = V4L2_CID_TILT_RELATIVE;
		control.value = -320;
		if (-1 == ioctl (dev, VIDIOC_S_CTRL, &control)) {
			perror("VIDIOC_S_CTRL");
			return;
        	}
	}
#endif
}

void Webcam::turnDown()
{
#ifdef V4L2_CID_TILT_RELATIVE
	struct v4l2_queryctrl queryctrl;
	struct v4l2_control control;

	memset (&queryctrl, 0, sizeof queryctrl);
	memset (&control, 0, sizeof control);
	queryctrl.id = V4L2_CID_TILT_RELATIVE;
	if (-1 == ioctl (dev, VIDIOC_QUERYCTRL, &queryctrl)) 
	{
	        if (errno != EINVAL) 
		{
			perror ("VIDIOC_QUERYCTRL");
			return;
		} 
	} else 
	{
		control.id = V4L2_CID_TILT_RELATIVE;
		control.value = 320;
		if (-1 == ioctl (dev, VIDIOC_S_CTRL, &control)) {
			perror("VIDIOC_S_CTRL");
			return;
        	}
	}
#endif
}

