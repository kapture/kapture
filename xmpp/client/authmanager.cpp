#include <QDebug>
#include <QStringList>

#include <openssl/md5.h>

#include "authmanager.h"

AuthManager::AuthManager()
{
}

bool AuthManager::isSupported(const QString &mechanism)
{
	return mechanism == "PLAIN";
}

AuthMechanism* AuthManager::getAuthMechanism(const QString &mechanism, const QString &username, const QString &password)
{
	if (mechanism == "PLAIN")
		return new PlainAuthMechanism(username, password);

	if (mechanism == "DIGEST-MD5")
		return new DigestMD5AuthMechanism(username, password);

	return 0;
}

QString AuthManager::randomString(int size)
{
	char *c;
	c = new char[size+1];

	for (int i = 0; i < size; i++)
	{
		int r;
		r = (rand()%26)+97;
		c[i] = r;
	}

	c[size] = '\0';

	return QString(c);
}

/***************
 * PLAIN
 ***************/
PlainAuthMechanism::PlainAuthMechanism(const QString &username, const QString &password)
	: AuthMechanism(username, password), m_success(false), m_failure(false)
{
	qDebug() << "Create PLAIN auth mechanism";
	// Prepare initial packet to send
	QDomElement e = m_document.createElement("auth");
	m_document.appendChild(e);
	e.setAttribute(QString("xmlns"), QString("urn:ietf:params:xml:ns:xmpp-sasl"));
	e.setAttribute(QString("mechanism"), QString("PLAIN"));
	QString text = QString("%1%2%3%4").arg('\0').arg(m_username).arg('\0').arg(m_password);
	QDomText t = m_document.createTextNode(text.toLatin1().toBase64());
	e.appendChild(t);
}

void PlainAuthMechanism::write(const QDomElement &node)
{
	m_failure = node.tagName() == QString("failure");
	m_success = node.tagName() == QString("success");
}

QDomDocument PlainAuthMechanism::read() const
{
	return m_document;
}

/***************
 * DIGEST-MD5
 ***************/
DigestMD5AuthMechanism::DigestMD5AuthMechanism(const QString &username, const QString &password)
	: AuthMechanism(username, password), m_success(false), m_failure(false)
{
	qDebug() << "Create DIGEST-MD5 auth mechanism";
	// Prepare initial packet to send
	QDomElement e = m_document.createElement("auth");
	m_document.appendChild(e);
	e.setAttribute(QString("xmlns"), QString("urn:ietf:params:xml:ns:xmpp-sasl"));
	e.setAttribute(QString("mechanism"), QString("DIGEST-MD5"));

	m_qop = "auth";
	m_state = waitChallenge;
}

QString DigestMD5AuthMechanism::toMD5String(const QByteArray &val) const
{
	QString val_md5;
	unsigned char *md5 = new unsigned char[MD5_DIGEST_LENGTH];

	MD5((unsigned char*)val.constData(), val.length(), md5);

	for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
		val_md5 += QString::number(md5[i], 16);

	delete [] md5;

	return val_md5;
}

QByteArray DigestMD5AuthMechanism::toMD5(const QByteArray &val) const
{
	QByteArray val_md5;
	unsigned char *md5 = new unsigned char[MD5_DIGEST_LENGTH];

	MD5((unsigned char*)val.constData(), val.length(), md5);

	val_md5 = QByteArray::fromRawData((char*)md5, MD5_DIGEST_LENGTH);

	delete [] md5;

	return val_md5;
}

QString DigestMD5AuthMechanism::computeResponse()
{
	m_realm = "elwood.innosoft.com";
	m_nonce = "OA9BSXrbuRhWay";
	m_cnonce = "OA9BSuZWMSpW8m";
	m_password = "secret";
	m_username ="chris";

	/*
	A1 = { H( { username-value, ":", realm-value, ":", passwd } ),
		   ":", nonce-value, ":", cnonce-value }
	A1 = toMD5(username-value + ":" + realm-value + ":" + passwd) + ":" + nonce-value + ":" + cnonce-value
	*/
	//qDebug() <<m_username.toUtf8() + ":" +m_realm.toUtf8() + ":" + m_password.toUtf8() << toMD5String(m_username.toUtf8() + ":" +m_realm.toUtf8() + ":" + m_password.toUtf8());
	QString A1 = toMD5String(m_username.toUtf8() + ":" + m_realm.toUtf8() + ":" + m_password.toUtf8());

	/*
	A2       = { "AUTHENTICATE:", digest-uri-value }
	*/

	//QByteArray A2 = "AUTHENTICATE:xmpp/jabber.org";
	QByteArray A2 = "AUTHENTICATE:acap/elwood.innosoft.com";
	/*
	response = HEX( HEX(A1) + ":" + nonce-value + ":" + nc-value + ":" + cnonce-value + ":" + qop-value + ":" + HEX((A2))
	*/

	qDebug() << A1.toUtf8() + ":" + m_nonce.toUtf8() + ":" + m_nc.toUtf8() + ":" + m_cnonce.toUtf8() + ":" + m_qop.toUtf8() + ":" + toMD5String(A2).toUtf8();

	QString response = toMD5String(A1.toUtf8() + ":" + m_nonce.toUtf8() + ":" + m_nc.toUtf8() + ":" + m_cnonce.toUtf8() + ":" + m_qop.toUtf8() + ":" + toMD5String(A2).toUtf8());

	return response;
}

void DigestMD5AuthMechanism::write(const QDomElement& node)
{
	QString challenge, text, pass = "";
	QDomElement e;

	m_document.clear();

	switch(m_state)
	{
	case waitChallenge:
		if (node.tagName() != "challenge")
		{
			m_failure = true;
			break;
		}

		challenge = QByteArray::fromBase64(node.firstChildElement().toText().data().toUtf8());

		//qDebug() << challenge;

		foreach(QString val, challenge.split(','))
		{
			QStringList couple = val.split('=');
			if (couple[0] == "realm")
				m_realm = couple.value(1).remove("\"");
			if (couple[0] == "nonce")
				m_nonce = couple.value(1).remove("\"");
			if (couple[0] == "qop")
				m_qop = couple.value(1).remove("\"");
		}

		m_cnonce = AuthManager::randomString(m_nonce.size());
		m_nc = "00000001";

		pass = computeResponse();

				qDebug() << "**************";
				qDebug() << pass;
				qDebug() << "**************";

		e = m_document.createElement("response");
		m_document.appendChild(e);
		e.setAttribute(QString("xmlns"), QString("urn:ietf:params:xml:ns:xmpp-sasl"));
		text = QString("username=\"%1\",realm=\"%2\","
							"nonce=\"%3\",cnonce=\"%4\","
							"nc=%5,qop=%6,digest-uri=\"xmpp/jabber.org\","
							"response=%7,charset=utf-8").arg(m_username).arg(m_realm).arg(m_nonce).arg(m_cnonce).arg(m_nc).arg(m_qop).arg(pass);

		e.appendChild(m_document.createTextNode(text.toLatin1().toBase64()));

		m_state = waitChallenge2;

	default:
		if (node.tagName() == "failure")
			m_failure = true;
		break;
	}
}

QDomDocument DigestMD5AuthMechanism::read() const
{
	return m_document;
}
