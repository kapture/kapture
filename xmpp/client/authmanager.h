#ifndef AUTHMANAGER_H
#define AUTHMANAGER_H

#include <QString>
#include <QDomDocument>

#include "xmliodevice.h"

class AuthMechanism;

class AuthManager
{
public:
	static bool isSupported(const QString& mechanism);
	static AuthMechanism* getAuthMechanism(const QString& mechanism, const QString &username, const QString &password);
	static QString randomString(int size);

private:
	explicit AuthManager();
	
};

class AuthMechanism
{
public:
	AuthMechanism(const QString& username, const QString& password)
		: m_username(username), m_password(password) {}
	virtual ~AuthMechanism() {}

	virtual void write(const QDomElement& node) = 0;
	virtual QDomDocument read() const = 0;

	virtual bool success() const = 0;
	virtual bool failure() const = 0;

protected:
	QString m_username;
	QString m_password;
};

class PlainAuthMechanism : public AuthMechanism
{
public:
	PlainAuthMechanism(const QString& username, const QString& password);
	virtual ~PlainAuthMechanism() {}

	virtual bool success() const {return m_success;}
	virtual bool failure() const {return m_failure;}

	virtual void write(const QDomElement& node);
	virtual QDomDocument read() const;

private:
	bool m_success;
	bool m_failure;

	QDomDocument m_document;
};

class DigestMD5AuthMechanism : public AuthMechanism
{
public:
	DigestMD5AuthMechanism(const QString& username, const QString& password);
	virtual ~DigestMD5AuthMechanism() {}

	virtual bool success() const {return m_success;}
	virtual bool failure() const {return m_failure;}

	virtual void write(const QDomElement &);
	virtual QDomDocument read() const;

private:
	bool m_success;
	bool m_failure;

	QDomDocument m_document;

	enum State {waitChallenge, waitChallenge2} m_state;

	QString m_realm;
	QString m_nonce;
	QString m_cnonce;
	QString m_nc;
	QString m_qop;

	QString computeResponse();
	QString toMD5String(const QByteArray&) const;
	QByteArray toMD5(const QByteArray&val) const;
};

#endif // AUTHMANAGER_H
