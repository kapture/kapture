/*
 *      Kapture
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */
#include <QFileDialog>
#include <QFile>

#include "client.h"
#include "presence.h"
#include "message.h"
#include "profile.h"
#include "filetransferhandler.h"
#include "jingle/jinglesession.h"
#include "jid.h"

Client::Client(const Jid &jid, QString server, QString port)
	: m_xmpp(0), m_taskManager(0)
{
	m_jid = jid;
	m_personnalServer = server;
	m_port = port.isEmpty() ? 5222 : port.toInt();
	m_roster = Roster(this);
	m_authenticated = false;
}

Client::Client()
	: m_xmpp(0), m_taskManager(0)
{
	m_roster = Roster(this);
	m_authenticated = false;
}

Client::~Client()
{
	delete m_xmpp;
	delete m_taskManager;
}

void Client::authenticate()
{
	if (m_jid.isValid() && !m_xmpp)
		m_xmpp = new Xmpp(m_jid, m_personnalServer, m_port);

	connect(m_xmpp, SIGNAL(error(Xmpp::ErrorType)), this, SLOT(connectionError(Xmpp::ErrorType)));

	if (m_resource.isEmpty())
		m_resource = "Kapture";

	if (!m_password.isEmpty())
		m_xmpp->auth(m_password, m_resource);

	if (!m_taskManager)
		m_taskManager = new TaskManager();

	ppTask = new PullPresenceTask(m_taskManager);
	connect(ppTask, SIGNAL(presenceFinished()), this, SLOT(presenceFinished()));
	pmTask = new PullMessageTask(m_taskManager);
	connect(pmTask, SIGNAL(messageFinished()), this, SLOT(messageFinished()));
	pjTask = new PullJingleTask(m_taskManager, this);
	connect(pjTask, SIGNAL(sessionReady(JingleSession*)), this, SLOT(jingleSessionReady(JingleSession*)));

//	psTask = new PullStreamTask(task, xmpp);
//	connect(psTask, SIGNAL(fileTransferIncoming()), this, SLOT(fileTransferIncoming()));
//	connect(psTask, SIGNAL(receiveFileReady()), this, SLOT(receiveFileReady()));
	//pjTask = new PullJingleTask(task, xmpp);
	//connect(pjTask, SIGNAL(newSession()), SLOT(newJingleSession()));
	
	connect(m_xmpp, SIGNAL(connected()), SLOT(authFinished()));
	connect(m_xmpp, SIGNAL(readyRead()), SLOT(read()));
}

void Client::connectionError(Xmpp::ErrorType e)
{
	delete m_xmpp;
	m_xmpp = 0;

	delete m_taskManager;
	   m_taskManager = 0;

	emit error(e);
}
/*
void Client::receiveFileReady()
{
	QString f = psTask->from().full();
	rfTask = new FileTransferTask(task, f, xmpp);
	connect(rfTask, SIGNAL(prcentChanged(Jid&, QString&, int)), this, SIGNAL(prcentChanged(Jid&, QString&, int)));
	rfTask->setFileInfo(psTask->fileName(), psTask->fileSize());
	rfTask->connectToHosts(psTask->streamHosts(), psTask->sid(), psTask->lastId(), psTask->saveFileName());
}

void Client::fileTransferIncoming()
{
//*
 //* FIXME: That shouldn't be there.
 //*
	ifd = new IncomingFileDialog();
	ifd->show();

	ifd->setFrom(psTask->from());
	ifd->setFileName(psTask->fileName());
	ifd->setFileSize(psTask->fileSize());
	ifd->setDesc(psTask->fileDesc());
	connect(ifd, SIGNAL(agree()), this, SLOT(ftAgree()));
	connect(ifd, SIGNAL(decline()), this, SLOT(ftDecline()));
}

void Client::ftAgree()
{
	psTask->ftAgree(ifd->fileName(), ifd->from(), ifd->saveFileName());
	ifd->close();
	delete ifd;
	//psTask->reset();
}

void Client::ftDecline()
{
	psTask->ftDecline(ifd->fileName(), ifd->from());
	ifd->close();
	delete ifd;
}*/

void Client::authFinished()
{
	qDebug() << "Client::authFinished()";
	m_jid.setResource(m_xmpp->getResource());
	m_authenticated = true;

	emit connected();

	if (m_presenceTmp.valid())
	{
		setPresence(m_presenceTmp.show(), m_presenceTmp.status(), m_presenceTmp.type());
		m_presenceTmp = Presence();
	}
}


void Client::setResource(const QString& resource)
{
	m_resource = resource;
}

void Client::setJid(const Jid& jid)
{
	m_jid = jid;
}

void Client::setPassword(const QString& password)
{
	m_password = password;
}
bool Client::isSecured() const
{
	return m_xmpp->isSecured();
}

void Client::sendMessage(const QString &to, const QString &message)
{
	qDebug() << "Client::sendMessage(" << to << "," << message << ")";
	// FIXME:Xml tree should be created here, the method sendMessage() should not exist ix Xmpp class.
	/*Message(const Jid& from,
		const Jid& to,
		const QString& message,
		const QString& type,
		const QString& subject,
		const QString& thread);*/
	
	mTask = new MessageTask(m_taskManager);
	QString type = "chat";
	QString subject = "";
	QString thread = "";
	Message m(m_jid, Jid(to), message, type, subject, thread);
	connect(mTask, SIGNAL(finished()), SLOT(messageDone()));
	mTask->sendMessage(m_xmpp, m);
}

void Client::messageDone()
{
	delete mTask;
}

bool Client::noStanza() const
{
	return !m_xmpp->stanzaAvailable();
}

Stanza *Client::getFirstStanza()
{
	return m_xmpp->getFirstStanza();
}

void Client::getRoster()
{
	rTask = new RosterTask(m_xmpp, m_taskManager, this);
	connect(rTask, SIGNAL(finished()), this, SLOT(rosterFinished()));
	rTask->getRoster(m_jid);
}

void Client::rosterFinished()
{
	m_roster = rTask->roster();

	emit rosterReady(rTask->roster());

	delete rTask;

	//FIXME:why ?
	//rTask = new RosterTask(m_xmpp, m_taskManager, this);
	//connect(rTask, SIGNAL(finished()), this, SLOT(slotUpdateItem()));
}

void Client::slotUpdateItem()
{
	printf("[CLIENT] Should add an item.\n");
	emit signalUpdateItem(rTask->roster().contactList().at(0));
}

void Client::setPresence(const QString& show, const QString& status, const QString& type)
{
	qDebug() << "[CLIENT] setPresence()";
	if (!m_authenticated)
	{
		m_presenceTmp = Presence(show, status, type, m_jid);
		return;
	}

	if (!m_xmpp)
		return;

	pTask = new PresenceTask(m_xmpp, m_taskManager);
	connect(pTask, SIGNAL(finished()), this, SLOT(setPresenceFinished()));
	pTask->setPresence(show, status, type);
}

/*
 * setInitialPresence finished SIGNAL is connected to this SLOT.
 * Everytime the presence of this resource changes, this SLOT should
 * be called.
 */
void Client::setPresenceFinished()
{
	// Add some feedback...
	delete pTask;
}

void Client::presenceFinished()
{
	/*
	 * TODO
	 * Better resource management.
	 * All XmppContact should have a list of currently available resources.
	 * Messages should be sent to the hight priority resource.
	 * A presence will add, remove or update a contact resource.
	 */
	//qDebug() << "Presence Finished";
	Presence p = ppTask->getPresence();

	for (int i = 0; i < m_roster.contactList().size(); i++)
	{
		//if (m_roster.contactList()[i]->jid()->full() == p.from().full())
		if (m_roster.contactList()[i]->jid()->equals(p.from()))
			m_roster.contactList()[i]->addPresence(p);
	}
}

// Sends authorization to "to" so "to" can see me.
void Client::addAuthFor(const QString& to)
{
	subTask = new PresenceTask(m_xmpp, m_taskManager);
	subTask->setSubscription(to, "subscribed");
	delete subTask;
}

// Removes authorization to "to" so "to" cannot see me.
void Client::removeAuthFor(const QString& to)
{
	subTask = new PresenceTask(m_xmpp, m_taskManager);
	subTask->setSubscription(to, "unsubscribed");
	delete subTask;
}

void Client::requestAuthFor(const QString& to)
{
	subTask = new PresenceTask(m_xmpp, m_taskManager);
	subTask->setSubscription(to, "subscribe");
	connect(subTask, SIGNAL(subApproved()), SLOT(subApproved()));
	connect(subTask, SIGNAL(subRefused()), SLOT(subRefused()));
}

void Client::subApproved()
{
	printf("Client::subApproved()\n");
}

void Client::subRefused()
{
	printf("Client::subRefused()\n");
}

void Client::messageFinished()
{
	Message m = pmTask->getMessage();

	qDebug() << "Message from" << m.from().full() << ":" << m.message();

	for (int i = 0; i < m_roster.contactList().size(); i++)
	{
		//if (m_roster.contactList()[i]->jid()->full() == m.from().full())
		if (m_roster.contactList()[i]->jid()->equals(m.from()))
			m_roster.contactList()[i]->addMessage(m);
	}
}

void Client::jingleSessionReady(JingleSession *session)
{
	foreach (XmppContact* c, m_roster.contactList())
	{
		if (session->to().equals(*(c->jid())))
		{
			//emit c->jingleSessionReady(session);
			return;
		}
	}
}

void Client::read()
{
	while (m_xmpp->stanzaAvailable())
	{
		//printf("Read next Stanza\n");
		Stanza *s = m_xmpp->getFirstStanza();
		//printf("Client:: : %s\n", s->from().full().toLatin1().constData());
		      m_taskManager->processStanza(*s);
		delete s;
	}
}

FileTransferHandler* Client::createFileTransferHandler(QString& to)
{
	FileTransferHandler *ftHandler = new FileTransferHandler(m_taskManager, m_xmpp, to);

	return ftHandler;
}

JingleSession *Client::createJingleRtpSession(const Jid &to, JingleRtpContent::JingleMediaType type)
{
	JingleSession *session = new JingleSession(this);
	session->setId(TaskManager::randomString(10));
	session->setResponder(to);
	session->addContent(new JingleRtpContent(type, this, session));

	pjTask->addSession(session);

	return session;
}

void Client::setJingleSupportedPayloads(QList<JinglePayloadType *> types, JingleRtpContent::JingleMediaType type)
{
	m_payloads.insert(type, types);
}

void Client::addItem(const Jid& item, const QString& name, const QString& /*group*/)
{
	rTask->addItem(item, name);
}

void Client::delItem(const Jid& item)
{
	rTask->delItem(item);
}
