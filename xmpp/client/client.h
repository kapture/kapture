#ifndef CLIENT_H
#define CLIENT_H
#include <QDomDocument>

#include "xmpp.h"
#include "taskmanager.h"
#include "tasks.h"
#include "roster.h"

#include "incomingfiledialog.h"

#include "jingle/jinglecontent.h"

class FileTransferHandler;

class Client : public QObject
{
	Q_OBJECT
public:
	enum streamErrorType {
			Unknown = 1,
			Declined
	};

	/**
	 * @brief Client Create a new client with parameters
	 * @param jid The jid to connect with
	 * @param server the personnal server to use
	 * @param port the personnal port to use.
	 */
	Client(const Jid &jid, QString server, QString port);

	/**
	 * @brief Client Create an empty client
	 */
	Client();
	~Client();


	void authenticate();

	/**
	 * @brief setResource set the resource for this connection.
	 */
	void setResource(const QString&);

	/**
	 * @brief setPassword Set the password to connect with.
	 */
	void setPassword(const QString&);

	/**
	 * @brief setJid Set the Jid to connect with.
	 */
	void setJid(const Jid&);

	/**
	 * @brief sendMessage Send a message to a contact
	 * @param to The destination of the contact
	 * @param message The message to send
	 */
	void sendMessage(const QString& to, const QString& message);

	/**
	 * @brief getRoster get the Roster from the server
	 */
	void getRoster();

	FileTransferHandler* createFileTransferHandler(QString& to);
	JingleSession* createJingleRtpSession(const Jid& to, JingleRtpContent::JingleMediaType type);
	void setJingleSupportedPayloads(QList<JinglePayloadType*>, JingleRtpContent::JingleMediaType type);
	QList<JinglePayloadType*> supportedPayloads(JingleRtpContent::JingleMediaType type) const {return m_payloads[type];}

	Stanza *getFirstStanza();

	void setPresence(const QString& show, const QString& status, const QString &type = QString());

	void addAuthFor(const QString& to);

	void removeAuthFor(const QString& to);

	void requestAuthFor(const QString& to);

	void addItem(const Jid&, const QString&, const QString&);

	void delItem(const Jid&);

	TaskManager *taskManager() const {return m_taskManager;}
	Xmpp *xmpp() const {return m_xmpp;}

	bool isSecured() const;
	bool noStanza() const;
	
private slots:
	void read();
	void rosterFinished();
	void setPresenceFinished();
	void presenceFinished();
	void messageFinished();
	void authFinished();
	//void fileTransferIncoming();
	//void ftAgree();
	//void ftDecline();
	//void receiveFileReady();
	void connectionError(Xmpp::ErrorType);
	void subApproved();
	void subRefused();
	void slotUpdateItem();
	//void newJingleSession();
	//void jingleSessionDeclined();
	void messageDone();
	void jingleSessionReady(JingleSession*);

signals:
	/**
	 * This signal is emitted when the stream is authenticated.
	 */
	void connected();
	void rosterReady(Roster);
	void signalUpdateItem(XmppContact*);
	//void presenceReady(const Presence&);
	//void messageReady(const Message&);
	void error(Xmpp::ErrorType);
	void streamError(streamErrorType);
	void registrationFinished();
	//void newJingleSessionReady();

private:
	IncomingFileDialog *ifd;

	Jid m_jid;
	QString m_resource;		// Resource
	QString m_personnalServer; 	// Personnal Server
	int m_port;				// Port
	QString m_password;		// Password
	Xmpp *m_xmpp;			//XMPP Protocol layer
	TaskManager *m_taskManager;				//rootTask
	RosterTask *rTask;			//rosterTask
	PresenceTask *pTask;		//presenceTask
	PresenceTask *subTask;		//subscriptionTask
	MessageTask *mTask;		//messageTask
	PullPresenceTask *ppTask;	//pullPresenceTask
	PullMessageTask *pmTask;	//pullMessageTask
	PullStreamTask *psTask;		//pullStreamTask
	PullJingleTask *pjTask;
	StreamTask *sTask;			//streamTask
	FileTransferTask *sfTask;		//sendFileTask
	FileTransferTask *rfTask;		//receiveFileTask
	Roster m_roster;
	bool m_authenticated;
	Presence m_presenceTmp;
	QMap<JingleRtpContent::JingleMediaType,
		QList<JinglePayloadType*>
	    > m_payloads;
	//JingleTask *svTask; 		//sendVideoTask
	//PullJingleTask *pjTask;
	//QList<JingleTask*> sessionList;
};
#endif
