#include <QFileDialog>

#include "filetransferhandler.h"
#include "taskmanager.h"
#include "tasks.h"
#include "xmpp.h"

FileTransferHandler::FileTransferHandler(TaskManager *taskManager, Xmpp *xmpp, const QString &to)
	: m_taskManager(taskManager), m_xmpp(xmpp),  m_to(to), sfTask(0)
{
	sTask = new StreamTask(m_taskManager, m_xmpp, to);

	connect(sTask, SIGNAL(error(int, const QString&)), SLOT(streamTaskError(int, const QString&)));

	sTask->discoInfo();

	connect(sTask, SIGNAL(infoDone()), this, SLOT(slotInfoDone()));
}

FileTransferHandler::~FileTransferHandler()
{
	if (sfTask)
	{
		m_taskManager->removeChild(sfTask);
		delete sfTask;
	}

	if (sTask)
	{
		m_taskManager->removeChild(sTask);
		delete sTask;
	}
}

void FileTransferHandler::streamTaskError(int errCode, const QString&)
{
	switch (errCode)
	{
	case 1:
		//emit streamError(Unknown);
		printf("Unknown error\n");
		break;
	case 403:
		//emit streamError(Declined);
		printf("User Declined sending invitation.\n");
		break;
	}

	emit transferFailed();
}

void FileTransferHandler::slotInfoDone()
{
	if (sTask->supports(XMLNS_SI) && sTask->supports(XMLNS_FILETRANSFER))
	{
		//Ask file here (show file open dialog).
		//QFileDialog *fileDialog = new QFileDialog(this);
		//TODO:do it somewhere else.
		m_fileName = QFileDialog::getOpenFileName(0,
			 tr("Send File"), QDir::homePath(), tr("All Files (*.*)"));
		QFile *f = new QFile(m_fileName);
		//TODO:manage QFile's deletion
		if (f->exists())
		{
			sTask->initStream(*f);
			connect(sTask, SIGNAL(finished()), this, SLOT(transferFile()));
		}
	}

	emit transferFailed();
}

void FileTransferHandler::transferFile()
{
	// TODO:Should manage more than 1 transfer at a time.
	sfTask = new FileTransferTask(m_taskManager, sTask->toJid(), m_xmpp);
	sfTask->start(sTask->negProfile(), sTask->sid(), m_fileName, sTask->proxies(), sTask->ips(), sTask->ports());
	connect(sfTask, SIGNAL(prcentChanged(Jid&, QString&, int)), SIGNAL(prcentChanged(Jid&, QString&, int)));
	connect(sfTask, SIGNAL(finished()), SIGNAL(transferDone()));
	connect(sfTask, SIGNAL(notConnected()), SLOT(notConnected()));

	m_taskManager->removeChild(sTask);
	delete sTask;
	sTask = 0;
}

void FileTransferHandler::notConnected()
{
	printf("Unable to connect to the target.\n");

	emit transferFailed();
}
