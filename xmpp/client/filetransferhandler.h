#include <QObject>
#include <QString>

#include "jid.h"

class TaskManager;
class Xmpp;
class FileTransferTask;
class StreamTask;

class FileTransferHandler : public QObject
{
	Q_OBJECT
public:
	FileTransferHandler(TaskManager *taskManager, Xmpp *xmpp, const QString& to);
	~FileTransferHandler();

private slots:
	void slotInfoDone();
	void transferFile();
	void streamTaskError(int, const QString&);
	void notConnected();

signals:
	void prcentChanged(Jid&, QString&, int);
	void transferFailed();
	void transferDone();

private:
	TaskManager *m_taskManager;
	Xmpp *m_xmpp;
	QString m_to;
	StreamTask *sTask;
	FileTransferTask *sfTask;

	QString m_fileName;
};
