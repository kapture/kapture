#ifndef JID_H
#define JID_H

#include <QString>
#include <QStringList>

class Jid
{
public:
	/**
	 * @brief Jid Constructs an empty Jid
	 */
	Jid();

	/**
	 * @brief Jid Construct a Jid from a string
	 * @param jid The string containing the Jid
	 */
	Jid(const char *jid);

	/**
	 * @brief Jid Construct a Jid from a string
	 * @param jid The string containing the Jid
	 */
	Jid(const QString &jid);

	~Jid();

	/**
	 * @brief isValid returns whether the Jid is valid
	 * @return true if the Jid is valid
	 */
	bool isValid() const;

	/**
	 * @brief node return the node of the Jid
	 * @return
	 */
	QString node() const;

	/**
	 * @brief resource return the resource of the Jid
	 * @return
	 */
	QString resource() const;

	/**
	 * @brief domain return the domain of the Jid
	 * @return
	 */
	QString domain() const;

	/**
	 * @brief bare return the bare Jid
	 * @return
	 */
	QString bare() const;

	/**
	 * @brief full return the full Jid
	 * @return
	 */
	QString full() const;

	/**
	 * @brief setNode set the node value of the Jid
	 * @param node
	 */
	void setNode(const QString &node);

	/**
	 * @brief setDomain set the domain value of the Jid
	 * @param domain
	 */
	void setDomain(const QString &domain);

	/**
	 * @brief setResource set the optionnal resource of the Jid
	 * @param resource
	 */
	void setResource(const QString &resource);

	bool equals(const Jid& other, bool withResource = false);
	bool operator==(Jid other) const;

	Jid &operator=(Jid other);

private:
	QString n; // node
	QString r; // resource
	QString d; // domain
	bool valid;
};

#endif //JID_H
