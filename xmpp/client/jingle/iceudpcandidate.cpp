#include "iceudpcandidate.h"
#include "jingleportsmanager.h"

#include <QDebug>

IceUdpCandidate::IceUdpCandidate(const QDomElement& elem, CandidateType t, QObject *parent)
	: QObject(parent), m_socket(0), m_established(false)
{
	m_type = t;
	bool ok;
	valid = true;

	m_component = elem.attribute("component").toInt(&ok);
	if (!ok)
	{
		valid = false;
		return;
	}

	foundation = elem.attribute("foundation").toInt(&ok);
	if (!ok)
	{
		valid = false;
		return;
	}

	generation = elem.attribute("generation").toInt(&ok);
	if (!ok)
	{
		valid = false;
		return;
	}

	network = elem.attribute("network").toInt(&ok);
	if (!ok)
	{
		valid = false;
		return;
	}

	m_port = elem.attribute("port").toInt(&ok);
	if (!ok)
	{
		valid = false;
		return;
	}

	m_priority = elem.attribute("priority").toUInt(&ok);
	if (!ok)
	{
		valid = false;
		return;
	}

	id = elem.attribute("id");
	m_address = QHostAddress(elem.attribute("ip", "0.0.0.0"));
	qDebug() << m_address;
	type = elem.attribute("type");
	protocol = elem.attribute("protocol") == "udp" ? UDP : TCP;
}

IceUdpCandidate::~IceUdpCandidate()
{
	JinglePortsManager::instance()->releasePort(m_port);
}
