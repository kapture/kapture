#ifndef ICEUDPCANDIDATE_H
#define ICEUDPCANDIDATE_H

#include <QObject>
#include <QHostAddress>
#include <QUdpSocket>
#include <QTimer>
#include <QDomElement>

class IceUdpCandidate : public QObject
{
public:
	enum CandidateType {LocalType, PeerType};
	IceUdpCandidate() {}
	IceUdpCandidate(const QDomElement& elem, CandidateType t, QObject *parent = 0);

	~IceUdpCandidate();

	unsigned int priority() const {return m_priority;}
	unsigned int component() const {return m_component;}
	QHostAddress address() const {return m_address;}
	int port() const {return m_port;}

	bool valid;

private:
	int m_component;
	int foundation;
	int generation;
	int network;
	int m_port;
	unsigned int m_priority;
	QString id;
	QHostAddress m_address;
	enum Protocol {UDP, TCP} protocol;
	QString type;

	CandidateType m_type;
	QUdpSocket *m_socket;
	QTimer m_timer;
	bool m_established;
};

#endif // ICEUDPCANDIDATE_H
