#include "iceudpconnectivitycheck.h"
#include "iceudpcandidate.h"
#include "jingletransport.h"
#include "jinglesession.h"
#include "stunclient.h"

#include <openssl/sha.h>
#include <openssl/hmac.h>

#include <QDebug>

IceUdpConnectivityCheck::IceUdpConnectivityCheck(IceUdpCandidate *local, IceUdpCandidate *remote, QObject *parent) :
	QObject(parent), m_local(local), m_remote(remote), m_state(FrozenState), m_receiving(false), m_sending(false)
{
	m_stunMessage = 0;
	m_session = dynamic_cast<JingleSession*>(parent->parent()->parent());
	int g, d;

	if (m_session->isInitiator())
	{
		g = local->priority();
		d = remote->priority();
	}
	else
	{
		d = local->priority();
		g = remote->priority();
	}

	m_priority = ((unsigned long int)qMin(g,d) << 32) + (2 * qMax(g,d)) + (g > d ? 1 : 0);

	start();
}

IceUdpConnectivityCheck::~IceUdpConnectivityCheck()
{
	delete m_stunMessage;
}

void IceUdpConnectivityCheck::start()
{
	qDebug() << "Starting connectivity checks";
	m_count = 0;
	m_state = PerformingState;

	m_udpSocket = new QUdpSocket(this);
	m_udpSocket->bind(m_local->address(), m_local->port());
	connect(m_udpSocket, SIGNAL(readyRead()), SLOT(readListener()));

	JingleIceUdpTransport *transport = dynamic_cast<JingleIceUdpTransport*>(parent());

	m_stunMessage = new StunMessage(StunMessage::BindingRequest);
	m_stunMessage->addAttribute(Attribute::Priority, (uint32_t)(m_local->priority() - (16 << 24)));
	if (m_session->isInitiator())
		m_stunMessage->addAttribute(Attribute::UseCandidate, QByteArray());
	m_stunMessage->addAttribute(
				m_session->isInitiator() ?
					Attribute::IceControlling :
					Attribute::IceControlled,
				(uint64_t) 0
				);
	m_stunMessage->addAttribute(
				Attribute::Username,
				transport->peerUfrag() +
				":" +
				transport->ufrag()
				);
	m_stunMessage->authenticate(transport->peerPwd());
	m_timer.setInterval(200);
	connect(&m_timer, SIGNAL(timeout()), SLOT(sendRequest()));
	m_timer.start();
}

void IceUdpConnectivityCheck::readListener()
{
	qDebug() << "IceUdpConnectivityCheck::readListener()";
	unsigned long int size = m_udpSocket->pendingDatagramSize();
	char * c_data = new char[size];
	QHostAddress fromAddr;
	uint16_t fromPort;
	m_udpSocket->readDatagram(c_data, size, &fromAddr, &fromPort);
	QByteArray data(c_data, size);
	delete [] c_data;

	JingleIceUdpTransport *transport = dynamic_cast<JingleIceUdpTransport*>(parent());
	StunMessage message(data, transport->pwd());

	if (message.type() == StunMessage::BindingResponse && message.transactionId() == m_stunMessage->transactionId())
	{
		//Todo: check mapped address to add peer reflexive candidate
		if (m_session->isInitiator())
			m_receiving = true;
		else
			m_sending = true;
		qDebug() << "Received answer !";
		m_timer.stop();
		m_valid = true;
		return;
	}
	else if (message.type() == StunMessage::BindingResponse)
	{
		qCritical() << "Response message ID is not valid";
		return;
	}

	StunMessage response(StunMessage::BindingResponse);
	response.setTransactionId(message.transactionId());

	QByteArray map;

	uint32_t addr = fromAddr.toIPv4Address();
	map.append(addr & 0xff);
	map.append(addr >> 8);
	map.append(addr >> 16);
	map.append(addr >> 24);

	map.append(fromPort & 0xff);
	map.append(fromPort >> 8);

	map.append(fromAddr.protocol() == QAbstractSocket::IPv4Protocol ? (char)1 : (char)2);
	map.append('\0');

	qDebug() << fromAddr << fromPort << *((uint64_t*)(map.data()));

	response.addAttribute(Attribute::MappedAddress, *((uint64_t*)(map.data())));
	response.addAttribute(Attribute::Priority, (uint32_t)(m_local->priority() - (16 << 24)));
	//response.addAttribute(Attribute::UseCandidate, QByteArray());
	response.addAttribute(m_session->isInitiator() ? Attribute::IceControlling : Attribute::IceControlled, (uint64_t) 0);
	m_stunMessage->addAttribute(
				Attribute::Username,
				transport->peerUfrag() +
				":" +
				transport->ufrag()
				);
	m_stunMessage->authenticate(transport->peerPwd());

	m_udpSocket->writeDatagram(response.toNetworkBytes(), m_remote->address(), m_remote->port());

	if (m_session->isInitiator())
		m_sending = true;
	else
		m_receiving = true;

	if (m_receiving && m_sending)
	{
		m_valid = true;
		m_timer.stop();
		emit success(this);
	}
}

void IceUdpConnectivityCheck::sendRequest()
{
	if (!m_receiving && !m_session->isInitiator())
		return;

	if (m_count++ == 10)
	{
		m_timer.stop();
		emit failure(this);
	}

	m_udpSocket->writeDatagram(m_stunMessage->toNetworkBytes(), m_remote->address(), m_remote->port());
}
