#ifndef ICEUDPCONNECTIVITYCHECK_H
#define ICEUDPCONNECTIVITYCHECK_H

#include <QObject>

#include "jinglesession.h"

class IceUdpCandidate;
class StunMessage;
class QUdpSocket;

class IceUdpConnectivityCheck : public QObject
{
	Q_OBJECT
public:
	enum State {FrozenState, WaitingState, PerformingState, FailureState, SuccessState};
	explicit IceUdpConnectivityCheck(IceUdpCandidate* local, IceUdpCandidate* remote, QObject *parent = 0);
	~IceUdpConnectivityCheck();

	void start();

	IceUdpCandidate* local() const {return m_local;}
	IceUdpCandidate* remote() const {return m_remote;}

signals:
	void success(IceUdpConnectivityCheck*);
	void failure(IceUdpConnectivityCheck*);
	
private slots:
	void readListener();
	void sendRequest();

private:
	IceUdpCandidate *m_local;
	IceUdpCandidate *m_remote;

	unsigned long int m_priority;
	unsigned short int m_count;

	State m_state;
	bool m_valid;
	bool m_receiving;
	bool m_sending;

	JingleSession *m_session;
	QUdpSocket *m_udpSocket;
	QTimer m_timer;
	StunMessage *m_stunMessage;
};

#endif // ICEUDPCONNECTIVITYCHECK_H
