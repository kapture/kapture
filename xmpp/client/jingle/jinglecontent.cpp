#include "jinglecontent.h"
#include "jingletransport.h"

#include "client.h"

JingleContent::JingleContent(QObject *parent)
	: XmlIODevice(parent)
{
}

JingleContent *JingleContent::createContent(const QDomElement& contentElement, Client *client, QObject *parent)
{
	QDomElement element = contentElement.firstChildElement();

	while(!element.isNull())
	{
		QDomDocument doc("");
		doc.appendChild(element);
		qDebug() << doc.toString();
		if (element.tagName() != "description")
		{
			element = element.nextSiblingElement();
			continue;
		}

		QString ns = element.namespaceURI();

		if (ns.startsWith("urn:xmpp:jingle:apps:rtp"))
			return new JingleRtpContent(contentElement, client, parent);

		element = element.nextSiblingElement();
	}

	return 0;
}

JingleRtpContent::JingleRtpContent(const QDomElement &contentNode, Client *client, QObject *parent)
	: JingleContent(parent), m_media(Audio), m_client(client)
{
	m_creator = contentNode.attribute("creator") == "initiator" ? Initiator : Responder;
	m_name = contentNode.attribute("name");
	m_senders = contentNode.attribute("senders", "both");
	m_ssrc = contentNode.attribute("ssrc").toInt();

	mediaName[0] = "audio";
	mediaName[1] = "video";
	mediaName[2] = "application";
	mediaName[3] = "example";
	mediaName[4] = "image";
	mediaName[5] = "message";
	mediaName[6] = "model";
	mediaName[7] = "multipart";
	mediaName[8] = "text";

	QDomElement node = contentNode.firstChildElement();

	while(!node.isNull())
	{
		if (node.tagName() == "description")
			loadDescription(node);
		else if (node.tagName() == "transport")
			m_transport = JingleTransport::createTransport(node, 2, this);

		node = node.nextSiblingElement();
	}

	connect(m_transport, SIGNAL(readyRead()), SLOT(readTransport()));
}

JingleRtpContent::JingleRtpContent(JingleMediaType type, Client *client, QObject *parent)
	: JingleContent(parent), m_media(type), m_client(client)
{
	mediaName[0] = "audio";
	mediaName[1] = "video";
	mediaName[2] = "application";
	mediaName[3] = "example";
	mediaName[4] = "image";
	mediaName[5] = "message";
	mediaName[6] = "model";
	mediaName[7] = "multipart";
	mediaName[8] = "text";

	m_transport = new JingleIceUdpTransport(2, this);
	m_name = mediaName[m_media];
	m_creator = Initiator;
}

void JingleRtpContent::readTransport()
{
	QDomDocument doc("");
	QDomElement content = doc.createElement("content");
	content.setAttribute("creator", m_creator == Initiator ? "initiator" : "responder");
	content.setAttribute("name", m_name);

	content.appendChild(m_transport->read());

	m_contents << content;

	emit readyRead(JingleStanza::TransportInfo, this);
}

void JingleRtpContent::loadDescription(const QDomElement &node)
{
	QString ns = node.namespaceURI();
	bool ok[3];

	m_version = ns.split(":").last().toInt(&(ok[0]));

	if (!ok[0])
		m_version = 1;

	QDomElement cNode = node.firstChildElement();
	while (!cNode.isNull())
	{
		m_peerPayloads <<
			      new JinglePayloadType(
				cNode.attribute("id").toInt(&(ok[0])),
				cNode.attribute("name"),
				cNode.attribute("clockrate", "0").toInt(&(ok[1])),
				cNode.attribute("channels", "0").toInt(&(ok[2])));

		if (!ok[0] || !ok[1] || ! ok[2])
		{
			qDebug() << "Ignoring payload" << m_peerPayloads.last()->name() << "because it is not valid.";
			delete m_peerPayloads.last();
			m_peerPayloads.removeLast();
		}

		if (cNode.hasChildNodes())
		{
			QDomElement pNode = cNode.firstChildElement();

			while (!pNode.isNull())
			{
				m_peerPayloads.last()->addParameter(pNode.attribute("name"),
								pNode.attribute("value"));

				pNode = pNode.nextSiblingElement();
			}
		}

		cNode = cNode.nextSiblingElement();
	}
}

bool JingleRtpContent::isPeerPayload(JinglePayloadType *payload) const
{
	if (m_peerPayloads.empty())
		return true;

	foreach (JinglePayloadType *cur, m_peerPayloads)
		if (payload->equals(cur))
			return true;

	return false;
}

QDomElement JingleRtpContent::initialContent()
{
	QDomDocument doc("");
	QDomElement content = doc.createElement("content");
	content.setAttribute("creator", m_creator == Initiator ? "initiator" : "responder");
	content.setAttribute("name", m_name);

	QDomElement description = doc.createElement("description");
	description.setAttribute("xmlns", "urn:xmpp:jingle:apps:rtp:1");
	description.setAttribute("media", mediaName[m_media]); //FIXME: set proper value (audio/video)

	foreach (JinglePayloadType *payload, m_client->supportedPayloads(m_media))
	{
		if (!isPeerPayload(payload))
			continue;

		description.appendChild(payload->toElement());
	}

	content.appendChild(description);
	content.appendChild(m_transport->initialCandidates());

	return content;
}

void JingleRtpContent::write(const QDomElement &)
{

}

QDomElement JingleRtpContent::read()
{
	return m_contents.takeFirst();
}


JingleRtpContent::CreatorType JingleRtpContent::creator() const
{
	return m_creator;
}

void JingleRtpContent::setCreator(const CreatorType &creator)
{
	m_creator = creator;
}

QString JingleRtpContent::name() const
{
	return m_name;
}

void JingleRtpContent::setName(const QString &name)
{
	m_name = name;
}

unsigned int JinglePayloadType::channels() const
{
	return m_channels;
}

unsigned int JinglePayloadType::clockrate() const
{
	return m_clockrate;
}

unsigned int JinglePayloadType::id() const
{
	return m_id;
}

QString JinglePayloadType::name() const
{
	return m_name;
}

void JinglePayloadType::addParameter(const QString &name, const QString &value)
{
	m_parameters.insert(name, value);
}

QString JinglePayloadType::parameter(const QString &name) const
{
	return m_parameters.value(name);
}

QMap<QString, QString> JinglePayloadType::parameters() const
{
	return m_parameters;
}

QDomElement JinglePayloadType::toElement() const
{
	QDomDocument doc("");
	QDomElement pt = doc.createElement("payload-type");

	pt.setAttribute("name", m_name);
	pt.setAttribute("id", m_id);

	if (m_channels)
		pt.setAttribute("channels", m_channels);
	if (m_clockrate)
		pt.setAttribute("clockrate", m_clockrate);

	foreach (QString param, parameters())
	{
		QDomElement paramElem = doc.createElement("parameter");
		paramElem.setAttribute("name", param);
		paramElem.setAttribute("value", parameter(param));

		pt.appendChild(paramElem);
	}

	return pt;
}

bool JinglePayloadType::equals(JinglePayloadType *other) const
{
	return name() == other->name() && id() == other->id();
}
