#ifndef JINGLECONTENT_H
#define JINGLECONTENT_H

#include "jinglestanza.h"
#include "xmliodevice.h"
#include "jingletransport.h"

class JingleTransport;
class Client;

class JingleContent : public XmlIODevice
{
	Q_OBJECT
public:
	JingleContent(QObject *parent = 0);
	virtual ~JingleContent() {}

	static JingleContent *createContent(const QDomElement &contentElement, Client *client, QObject *parent);
	virtual QDomElement initialContent() = 0;

	virtual QString name() const = 0;
	virtual JingleTransport* transport() const = 0;

signals:
	void readyRead(JingleStanza::JingleStreamAction, JingleContent*);
};

class JinglePayloadType;

class JingleRtpContent : public JingleContent
{
	Q_OBJECT
public:

	enum JingleMediaType
	{
		Audio = 0,
		Video,
		Application,
		Example,
		Image,
		Message,
		Model,
		Multipart,
		Text
	};
	enum CreatorType
	{
		Initiator,
		Responder
	};

	JingleRtpContent(const QDomElement& contentNode, Client *client, QObject *parent = 0);
	JingleRtpContent(JingleMediaType type, Client *client, QObject *parent = 0);
	virtual ~JingleRtpContent() {}

	virtual void write(const QDomElement &);
	virtual QDomElement read();

	virtual QDomElement initialContent();

	JingleMediaType mediaType() const {return m_media;}

	virtual QString name() const;
	void setName(const QString &name);

	CreatorType creator() const;
	void setCreator(const CreatorType &creator);

	virtual JingleTransport* transport() const {return m_transport;}

private slots:
	void readTransport();

private:
	JingleTransport *m_transport;
	QList<JinglePayloadType*> m_peerPayloads;

	QList<QDomElement> m_contents;

	QDomDocument m_document;

	unsigned int m_version;
	CreatorType m_creator;
	QString m_name;
	QString m_senders;
	unsigned int m_ssrc;
	JingleMediaType m_media;

	void loadDescription(const QDomElement &node);
	bool isPeerPayload(JinglePayloadType*) const;
	QString mediaName[9];

	Client *m_client;
};

class JinglePayloadType
{
public:
	JinglePayloadType(unsigned int id, const QString& name, unsigned int clockrate, unsigned int channels)
		: m_name(name), m_id(id), m_clockrate(clockrate), m_channels(channels)
	{}

	QString name() const;

	unsigned int id() const;

	unsigned int clockrate() const;

	unsigned int channels() const;

	void addParameter(const QString&, const QString&);
	QString parameter(const QString&) const;
	QMap<QString, QString> parameters() const;

	bool equals(JinglePayloadType *other) const;

	QDomElement toElement() const;

private:
	QString m_name;
	unsigned int m_id;
	unsigned int m_clockrate;
	unsigned int m_channels;
	QMap<QString, QString> m_parameters;
};

#endif // JINGLECONTENT_H
