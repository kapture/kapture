#include "jingleportsmanager.h"
#include "config.h"

JinglePortsManager *m_instance = 0;

JinglePortsManager::JinglePortsManager(int startPort)
	: m_startPort(startPort)
{
}

JinglePortsManager *JinglePortsManager::instance(unsigned int startPort)
{
	Config c;
	if (!m_instance)
		m_instance = new JinglePortsManager(startPort ? startPort : c.jinglePort());

	return m_instance;
}

int JinglePortsManager::getNextAvailablePort()
{
	unsigned int start = m_startPort;
	unsigned int idx = 0;

	while (m_ports.contains(start))
	{
		start++;
		idx++;
	}

	m_ports.insert(idx, start);

	return start;
}

void JinglePortsManager::releasePort(unsigned int port)
{
	if (!m_ports.contains(port))
		return;

	m_ports.removeOne(port);
}
