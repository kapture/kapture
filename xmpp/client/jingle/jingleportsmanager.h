#ifndef JINGLEPORTSMANAGER_H
#define JINGLEPORTSMANAGER_H

#include <QList>

class JinglePortsManager
{
public:
	static JinglePortsManager *instance(unsigned int startPort = 0);

	int getNextAvailablePort();
	void releasePort(unsigned int port);

private:
	JinglePortsManager(int startPort);
	QList<int> m_ports;
	unsigned int m_startPort;
};

#endif // JINGLEPORTSMANAGER_H
