#include "jinglesession.h"

#include "jingletransport.h"
#include "jinglecontent.h"

#include "client.h"
#include "task.h"
#include "tasks.h"

JingleSession::JingleSession(Client *parent) :
	QObject(parent), m_client(parent)
{
	m_sessionAccepted = false;
	m_initiator = m_client->xmpp()->node();
}

QString JingleSession::id() const
{
	return m_id;
}

void JingleSession::setId(const QString &id)
{
	m_id = id;
}

void JingleSession::write(const JingleStanza &s)
{
	QDomElement node;
	switch (s.action())
	{
	case JingleStanza::SessionInitiate:
		node = s.firstContentElement();
		m_peer = s.from();
		m_initiator = s.from();
		m_responder = m_client->xmpp()->node();
		m_id = s.sid();

		while(!node.isNull())
		{
			m_contents << JingleContent::createContent(node, m_client, this);
			connect(m_contents.last(), SIGNAL(readyRead(JingleStanza::JingleStreamAction, JingleContent*)),
						   SLOT(readContentAction(JingleStanza::JingleStreamAction, JingleContent*)));
			node = node.nextSiblingElement();
		}

		break;
	case JingleStanza::SessionAccept:
		m_sessionAccepted = true;
		/* fall-through */
	case JingleStanza::TransportInfo:
	{
		node = s.firstContentElement();
		JingleContent *content = findContentFromName(node.attribute("name"));
		content->transport()->addTransportInfo(node.firstChildElement("transport"));
		break;
	}
	case JingleStanza::SessionTerminate:
		node = s.jingleElement().firstChildElement("reason");

		emit sessionTerminated(stringToReason(node.firstChildElement().tagName()), this);
		break;
	default:
		break;
	}
}

void JingleSession::readContentAction(JingleStanza::JingleStreamAction action, JingleContent *content)
{
	JingleStanza s(Stanza::IQ, "set", TaskManager::randomString(10), m_peer.full(), action, m_id);
	s.jingleElement().appendChild(content->read());

	JingleTask *jTask = new JingleTask(m_client->taskManager(), m_client->xmpp());
	jTask->sendStanza(s);
	connect(jTask, SIGNAL(finished()), jTask, SLOT(deleteLater()));
}

JingleContent* JingleSession::findContentFromName(const QString& name)
{
	foreach(JingleContent *content, m_contents)
		if (content->name() == name)
			return content;

	return 0;
}

JingleSession::Reason JingleSession::stringToReason(const QString& reason)
{
	/**
	  FIXME:implement the rest.
	  */
	if (reason == "alternative-session")
		return ReasonAlternative;
	if (reason == "decline")
		return ReasonDecline;
	if (reason == "gone")
		return ReasonGone;

	return ReasonGeneralError;
}

QDomDocument JingleSession::read() const
{
	return m_document;
}

void JingleSession::accept()
{
	m_sessionAccepted = true;
	JingleStanza stanza(Stanza::IQ, "set", TaskManager::randomString(10), m_peer.full(), JingleStanza::SessionAccept, m_id);

	QDomElement jingle = stanza.jingleElement();

	foreach (JingleContent *content, m_contents)
		jingle.appendChild(content->initialContent());

	JingleTask *jTask = new JingleTask(m_client->taskManager(), m_client->xmpp());
	jTask->sendStanza(stanza);
	connect(jTask, SIGNAL(finished()), jTask, SLOT(deleteLater()));
}

void JingleSession::decline()
{
	terminate(ReasonDecline);
}

void JingleSession::terminate(Reason r)
{
	QDomDocument doc("");
	JingleStanza stanza(Stanza::IQ, "set", TaskManager::randomString(10), m_peer.full(), JingleStanza::SessionTerminate, m_id);

	QDomElement jingle = stanza.jingleElement();

	QDomElement reason = doc.createElement("reason");

	QDomElement condition;

	if (r == ReasonDecline)
	{
		condition = doc.createElement("condition");
		QDomElement condValue = doc.createElement("decline");
		condition.appendChild(condValue);
	}
	else
		condition = doc.createElement("success");


	jingle.appendChild(reason);
	reason.appendChild(condition);

	JingleTask *jTask = new JingleTask(m_client->taskManager(), m_client->xmpp());
	jTask->sendStanza(stanza);
	connect(jTask, SIGNAL(finished()), jTask, SLOT(deleteLater()));
	emit sessionTerminated(r, this);
}

void JingleSession::setResponder(const Jid& res)
{
	m_responder = res;
}

void JingleSession::initiate()
{
	m_peer = m_responder;
	JingleStanza stanza(Stanza::IQ, "set", TaskManager::randomString(10), m_peer.full(), JingleStanza::SessionInitiate, m_id);
	stanza.setInitiator(m_client->xmpp()->node().full());
	stanza.setFrom(m_client->xmpp()->node());

	QDomElement jingle = stanza.jingleElement();

	foreach (JingleContent *content, m_contents)
		jingle.appendChild(content->initialContent());

	JingleTask *jTask = new JingleTask(m_client->taskManager(), m_client->xmpp());
	jTask->sendStanza(stanza);
	connect(jTask, SIGNAL(finished()), jTask, SLOT(deleteLater()));
}

void JingleSession::addContent(JingleContent *content)
{
	m_contents << content;
}
