#ifndef JINGLESESSION_H
#define JINGLESESSION_H

#include <QObject>
#include <QDomDocument>

#include "jinglestanza.h"

class JingleContent;
class Client;

class JingleSession : public QObject
{
	Q_OBJECT
public:
	enum Reason
	{
		ReasonAlternative,
		ReasonBusy,
		ReasonCancel,
		ReasonConnectivity,
		ReasonDecline,
		ReasonExpired,
		ReasonFailedApplication,
		ReasonFailedTransport,
		ReasonGeneralError,
		ReasonGone,
		ReasonParameters,
		ReasonMediaError,
		ReasonSecurityError,
		ReasonSuccess,
		ReasonTimeout,
		ReasonUnsupportedApplication,
		ReasonUnsupportedTransport
	};
	explicit JingleSession(Client *parent = 0);
	
	QString id() const;
	void setId(const QString &id);

	void write(const JingleStanza&);
	QDomDocument read() const;

	enum JingleState {
		waitInitiate,
		/*...*/
		active
	};

	void setResponder(const Jid&);

	void accept();
	void initiate();

	void decline();
	void terminate(Reason);

	void addContent(JingleContent*);

	static Reason stringToReason(const QString& reason);
	JingleContent* findContentFromName(const QString&);

	bool isInitiator() const {return m_peer == m_responder;}
	Jid to() const {return m_peer;}

signals:
	void readyRead();
	void sessionTerminated(JingleSession::Reason, JingleSession*);

private slots:
	void readContentAction(JingleStanza::JingleStreamAction, JingleContent*);

private:
	QString m_id;
	Jid m_peer;
	Jid m_initiator;
	Jid m_responder;
	bool m_sessionAccepted;
	QList<JingleContent*> m_contents;

	QDomDocument m_document;
	Client *m_client;
	
};

#endif // JINGLESESSION_H
