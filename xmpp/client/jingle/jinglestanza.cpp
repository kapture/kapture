#include "jinglestanza.h"

JingleStanza::JingleStanza()
{
	valid = false;
}

JingleStanza::JingleStanza(const Stanza& s)
	: Stanza(s.element())
{
	valid = n.firstChildElement().localName() == "jingle";
}

JingleStanza::JingleStanza(Kind kind, const QString &type, const QString &id, const QString &to, JingleStreamAction action, const QString &sid) :
	Stanza(kind, type, id, to), valid(true)
{
	QDomDocument doc("");
	QDomElement jingle = doc.createElement("jingle");

	jingle.setAttribute("xmlns", "urn:xmpp:jingle:1");
	jingle.setAttribute("action", actionString(action));
	jingle.setAttribute("sid", sid);

	n.appendChild(jingle);
}

JingleStanza::~JingleStanza()
{

}

QString JingleStanza::actionString(JingleStreamAction action) const
{
	if (action == SessionInitiate)
		return "session-initiate";
	if (action == SessionTerminate)
		return "session-terminate";
	if (action == SessionAccept)
		return "session-accept";
	if (action == SessionInfo)
		return "session-info";
	if (action == ContentAdd)
		return "content-add";
	if (action == ContentRemove)
		return "content-remove";
	if (action == ContentModify)
		return "content-modify";
	if (action == ContentReplace)
		return "content-replace";
	if (action == ContentAccept)
		return "content-accept";
	if (action == TransportInfo)
		return "transport-info";

	return "";
}

QDomElement JingleStanza::jingleElement() const
{
	return n.firstChildElement("jingle");
}

void JingleStanza::setInitiator(const QString& jid)
{
	jingleElement().setAttribute("initiator", jid);
}

void JingleStanza::setResponder(const QString& jid)
{
	jingleElement().setAttribute("responder", jid);
}

QString JingleStanza::sid() const
{
	return jingleElement().attribute("sid");
}

JingleStanza::JingleStreamAction JingleStanza::action() const
{
	if (jingleElement().attribute("action") == "session-initiate")
		return SessionInitiate;
	if (jingleElement().attribute("action") == "session-terminate")
		return SessionTerminate;
	if (jingleElement().attribute("action") == "session-accept")
		return SessionAccept;
	if (jingleElement().attribute("action") == "session-info")
		return SessionInfo;
	if (jingleElement().attribute("action") == "content-add")
		return ContentAdd;
	if (jingleElement().attribute("action") == "content-remove")
		return ContentRemove;
	if (jingleElement().attribute("action") == "content-modify")
		return ContentModify;
	if (jingleElement().attribute("action") == "content-replace")
		return ContentReplace;
	if (jingleElement().attribute("action") == "content-accept")
		return ContentAccept;
	if (jingleElement().attribute("action") == "transport-info")
		return TransportInfo;

	return NoAction;
}

QDomElement JingleStanza::firstContentElement() const
{
	return n.firstChild().firstChildElement();
}

JingleStanza JingleStanza::operator=(const JingleStanza& s)
{
	valid = s.valid;
	return *this;
}
