#ifndef JINGLESTANZA_H
#define JINGLESTANZA_H

#include "stanza.h"

class JingleStanza : public Stanza
{
public:
	enum JingleStreamAction
	{
		SessionInitiate = 0,
		SessionTerminate,
		SessionAccept,
		SessionInfo,
		ContentAdd,
		ContentRemove,
		ContentModify,
		ContentReplace,
		ContentAccept,
		TransportInfo,
		NoAction
	};

	JingleStanza();
	JingleStanza(const Stanza&);
	JingleStanza(Kind kind, const QString& type, const QString& id, const QString& to, JingleStreamAction action, const QString& sid);

	~JingleStanza();

	QString sid() const;
	bool isValid() const {return valid;}

	JingleStreamAction action() const;

	QDomElement firstContentElement() const;
	QDomElement jingleElement() const;

	QString actionString(JingleStreamAction) const;

	void setInitiator(const QString& jid);
	void setResponder(const QString& jid);

	JingleStanza operator=(const JingleStanza&);
private:
	bool valid;
};

#endif
