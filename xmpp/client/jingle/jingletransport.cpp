#include "jingletransport.h"
#include "jingleportsmanager.h"
#include "iceudpcandidate.h"
#include "iceudpconnectivitycheck.h"
#include "task.h"
#include "config.h"

#include <QStringList>
#include <QNetworkInterface>
#include <QDebug>

JingleTransport::JingleTransport(QObject *parent) :
	XmlIODevice(parent)
{
}

JingleTransport *JingleTransport::createTransport(const QDomElement& node, unsigned int componentCount, QObject* parent)
{
	if (node.namespaceURI().startsWith("urn:xmpp:jingle:transports:ice-udp"))
		return new JingleIceUdpTransport(node, componentCount, parent);

	return 0;
}


/**
 * JingleIceUdpTransport class implementation.
 */

JingleIceUdpTransport::JingleIceUdpTransport(const QDomElement& node, unsigned int componentCount, QObject *parent) :
	JingleTransport(parent), m_componentCount(componentCount)
{
	m_stunServer = "stun.stunprotocol.org";
	QString ns = node.namespaceURI();
	bool ok;

	m_version = ns.split(":").last().toInt(&ok);

	if (!ok)
		m_version = 1;

	m_peerPwd = node.attribute("pwd");
	m_pwd = TaskManager::randomString(m_peerPwd.size());
	m_peerUfrag = node.attribute("ufrag");
	m_ufrag = TaskManager::randomString(m_peerUfrag.size());

	QDomElement curNode = node.firstChildElement();
	while(!curNode.isNull())
	{
		IceUdpCandidate *c = new IceUdpCandidate(curNode, IceUdpCandidate::PeerType, this);
		if (!c->valid)
		{
			delete c;
		}
		else
		{
			if (c->component() > m_componentCount)
				m_componentCount = c->component();

			addRemoteCandidate(c);
		}

		curNode = curNode.nextSiblingElement();
	}
}

JingleIceUdpTransport::JingleIceUdpTransport(unsigned int componentCount, QObject *parent):
	JingleTransport(parent), m_componentCount(componentCount)
{
	m_stunServer = "stun.stunprotocol.org";
	m_pwd = TaskManager::randomString(12);
	m_ufrag = TaskManager::randomString(4);
}

void JingleIceUdpTransport::write(const QDomElement &)
{

}

QDomElement JingleIceUdpTransport::read()
{
	return m_transportInfos.takeFirst();
}

QDomElement JingleIceUdpTransport::initialCandidates()
{
	/*
	 * Note on priority computation :
	 * The type preference values are as follows : 0 for relayed, 63 for reflexive and 126 for local
	 * The local preference values are as follows : n for address n (0,1,2,3,...)
	 */
	QList<QHostAddress> addresses = QNetworkInterface::allAddresses();
	QDomDocument doc("");

	QDomElement transport = doc.createElement("transport");
	transport.setAttribute("xmlns", "urn:xmpp:jingle:transports:ice-udp:1");
	transport.setAttribute("ufrag", m_ufrag);
	transport.setAttribute("pwd", m_pwd);

	foreach (QHostAddress addr, addresses)
	{
		if (addr.protocol() == QAbstractSocket::IPv6Protocol)
			continue;

		for (unsigned int i = 0; i < m_componentCount; i++)
		{
			QDomElement candidate = doc.createElement("candidate");
			candidate.setAttribute("component", i + 1);
			candidate.setAttribute("foundation", 1);
			candidate.setAttribute("generation", 0);
			candidate.setAttribute("id", TaskManager::randomString(10));
			candidate.setAttribute("ip", addr.toString());
			candidate.setAttribute("network", 1);
			candidate.setAttribute("port", JinglePortsManager::instance()->getNextAvailablePort());
			candidate.setAttribute("protocol", "udp");
			candidate.setAttribute("type", "host");

			unsigned long int priority = (126 << 24) + ((65535 - m_candidates.size()/m_componentCount) << 8) + 256 - (i + 1);
			candidate.setAttribute("priority", QString::number(priority));

			addLocalCandidate(new IceUdpCandidate(candidate.toElement(), IceUdpCandidate::LocalType, this));
			transport.appendChild(candidate);
		}
	}

	m_stunClient = new StunClient(m_stunServer, "", this);
	connect(m_stunClient, SIGNAL(gotResponse(StunResponse)), SLOT(stunResponse(StunResponse)));
	m_stunClient->getServerReflexive();

	return transport;
}

void JingleIceUdpTransport::addLocalCandidate(IceUdpCandidate * c)
{
	m_candidates << c;
	bool mustStart = m_checkList.isEmpty();

	foreach (IceUdpCandidate *remote, m_remoteCandidates)
	{
		if (c->component() == remote->component() && c->address().protocol() == remote->address().protocol())
		{
			IceUdpConnectivityCheck *checker = new IceUdpConnectivityCheck(c, remote, this);
			m_checkList << checker;
			connect(checker, SIGNAL(failure(IceUdpConnectivityCheck*)), SLOT(connectivityFailure(IceUdpConnectivityCheck*)));
			connect(checker, SIGNAL(success(IceUdpConnectivityCheck*)), SLOT(connectivitySuccess(IceUdpConnectivityCheck*)));
		}
	}

	//if (mustStart && !m_checkList.isEmpty())
	//	m_checkList[0]->start();
}

void JingleIceUdpTransport::addRemoteCandidate(IceUdpCandidate *c)
{
	m_remoteCandidates << c;
	bool mustStart = m_checkList.isEmpty();

	foreach (IceUdpCandidate *local, m_candidates)
	{
		if (c->component() == local->component() && c->address().protocol() == local->address().protocol())
		{
			IceUdpConnectivityCheck *checker = new IceUdpConnectivityCheck(local, c, this);
			m_checkList << checker;
			connect(checker, SIGNAL(failure(IceUdpConnectivityCheck*)), SLOT(connectivityFailure(IceUdpConnectivityCheck*)));
			connect(checker, SIGNAL(success(IceUdpConnectivityCheck*)), SLOT(connectivitySuccess(IceUdpConnectivityCheck*)));
		}
	}

	//if (mustStart && !m_checkList.isEmpty())
	//	m_checkList[0]->start();
}

void JingleIceUdpTransport::stunResponse(StunResponse response)
{
	/*
	 * fixme: check port values, should the stun request use the same port as the candidate ?
	 */
	delete m_stunClient;

	return;

	if (!response.usable())
		return;

	QDomDocument doc("");

	QDomElement transport = doc.createElement("transport");
	transport.setAttribute("xmlns", "urn:xmpp:jingle:transports:ice-udp:1");
	transport.setAttribute("ufrag", m_ufrag);
	transport.setAttribute("pwd", m_pwd);

	for (unsigned int i = 0; i < m_componentCount; i++)
	{
		QDomElement candidate = doc.createElement("candidate");
		candidate.setAttribute("component", i + 1);
		candidate.setAttribute("foundation", 1);
		candidate.setAttribute("generation", 0);
		candidate.setAttribute("id", TaskManager::randomString(10));
		candidate.setAttribute("ip", response.address().toString());
		candidate.setAttribute("network", 1);
		candidate.setAttribute("port", JinglePortsManager::instance()->getNextAvailablePort());
		candidate.setAttribute("protocol", "udp");
		candidate.setAttribute("type", "srflx");

		unsigned long int priority = (126 << 24) + ((65535 - m_candidates.size()/m_componentCount) << 8) + 256 - (i + 1);
		candidate.setAttribute("priority", QString::number(priority));

		addLocalCandidate(new IceUdpCandidate(candidate.toElement(), IceUdpCandidate::LocalType, this));
		transport.appendChild(candidate);
	}

	m_transportInfos << transport;

	emit readyRead();
}

void JingleIceUdpTransport::addTransportInfo(const QDomElement& elem)
{
	// FIXME:should an error be sent ?
	if (!elem.namespaceURI().startsWith("urn:xmpp:jingle:transports:ice-udp:") || (!m_remoteCandidates.isEmpty() && (elem.attribute("pwd") != m_peerPwd || elem.attribute("ufrag") != m_peerUfrag)))
		return;

	if (m_peerPwd.isEmpty() && m_peerUfrag.isEmpty())
	{
		m_peerPwd = elem.attribute("pwd");
		m_peerUfrag = elem.attribute("ufrag");
	}

	QDomElement candidate = elem.firstChildElement("candidate");
	while(!candidate.isNull())
	{
		IceUdpCandidate *c = new IceUdpCandidate(candidate, IceUdpCandidate::PeerType, this);
		if (!c->valid)
		{
			delete c;
		}
		else
			addRemoteCandidate(c);

		candidate = candidate.nextSiblingElement();
	}
}

void JingleIceUdpTransport::connectivitySuccess(IceUdpConnectivityCheck *checker)
{
	qDebug() << "Connectivity success:" << checker->local()->address() << "<=>" << checker->remote()->address();
}

void JingleIceUdpTransport::connectivityFailure(IceUdpConnectivityCheck *checker)
{
	qDebug() << "Connectivity failure:" << checker->local()->address() << "<=>" << checker->remote()->address();
}
