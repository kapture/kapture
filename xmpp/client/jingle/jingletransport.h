#ifndef JINGLETRANSPORT_H
#define JINGLETRANSPORT_H

#include <QObject>
#include <QDomElement>

#include "xmliodevice.h"
#include "stunclient.h"
#include "jingleportsmanager.h"

class JingleTransport : public XmlIODevice
{
	Q_OBJECT
public:
	JingleTransport(QObject *parent = 0);
	virtual ~JingleTransport() {}

	static JingleTransport *createTransport(const QDomElement& node, unsigned int componentCount = 2, QObject *parent = 0);

	virtual QDomElement initialCandidates() = 0;
	virtual void addTransportInfo(const QDomElement&) = 0;
};

class IceUdpCandidate;
class IceUdpConnectivityCheck;

class JingleIceUdpTransport : public JingleTransport
{
	Q_OBJECT
public:
	JingleIceUdpTransport(const QDomElement& node, unsigned int componentCount, QObject *parent = 0);
	JingleIceUdpTransport(unsigned int componentCount, QObject *parent = 0);
	virtual ~JingleIceUdpTransport() {}

	virtual void write(const QDomElement&);
	virtual QDomElement read();

	virtual QDomElement initialCandidates();
	virtual void addTransportInfo(const QDomElement&);

	QString peerUfrag() const {return m_peerUfrag;}
	QString ufrag() const {return m_ufrag;}
	QString peerPwd() const {return m_peerPwd;}
	QString pwd() const {return m_pwd;}

private slots:
	void stunResponse(StunResponse);

	void connectivitySuccess(IceUdpConnectivityCheck*);
	void connectivityFailure(IceUdpConnectivityCheck*);

private:
	QDomDocument m_document;
	unsigned int m_version;

	QList<IceUdpCandidate*> m_remoteCandidates;
	QList<IceUdpCandidate*> m_candidates;

	QList<QDomElement> m_transportInfos;

	QString m_peerPwd;
	QString m_peerUfrag;
	QString m_pwd;
	QString m_ufrag;
	QString m_stunServer;

	unsigned int m_componentCount;

	StunClient *m_stunClient;

	QList<IceUdpConnectivityCheck*> m_checkList;

	void addLocalCandidate(IceUdpCandidate*);
	void addRemoteCandidate(IceUdpCandidate*);
};


#endif // JINGLETRANSPORT_H
