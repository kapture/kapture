/*
 *      Kapture
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include <QString>

#include "jid.h"
#include "presence.h"

Presence::Presence(const QString& type, const QString& status, const QString& show, const Jid& from, int priority)
//Presence::Presence(const QString& type, const QString& status, const QString& show)
{
	j = from;
	t = type;
	stat = status;
	s = show;
	m_priority = priority;
	//printf("j = %s, t = %s, stat = %s, s = %s\n",j.full().toLatin1().constData(), t.toLatin1().constData(), stat.toLatin1().constData(), s.toLatin1().constData());
}

Presence::~Presence()
{

}

void Presence::setType(const QString& type)
{
	t = type;
}

void Presence::setShow(const QString& show)
{
	s = show;
}

void Presence::setStatus(const QString& status)
{
	stat = status;
}

void Presence::setFrom(const Jid& from)
{
	j = from;
}

bool Presence::operator>=(const Presence& other)
{
	if (t == other.type() && t == "available")
	{
		if (s == "chat")
			return true;

		if (s == "" && other.show() != "chat")
			return true;
		else
			return false;

		return true;
	}

	return t == "available";
}

bool Presence::operator==(const Presence& p)
{
	return t == p.t && s == p.s && j.equals(p.from()) && stat == p.status() && m_priority == p.m_priority;
}

Presence &Presence::operator=(Presence o)
{
	t = o.t;
	s = o.s;
	stat = o.stat;
	j = o.j;
	m_priority = o.m_priority;

	return *this;
}
