#ifndef PRESENCE_H
#define PRESENCE_H

#include "jid.h"

class Presence
{
public:
	Presence(const QString& type, const QString& status, const QString& show, const Jid& from = Jid(), int priority = -1);
	//Presence(const QString& type, const QString& status, const QString& show);
	Presence() {}
	~Presence();

	Jid from() const {return j;}
	QString type() const {return t;}
	QString status() const {return stat;}
	QString show() const {return s;}
	int priority() const {return m_priority;}

	void setType(const QString&);
	void setShow(const QString&);
	void setStatus(const QString&);
	void setFrom(const Jid&);

	bool valid() const {return j.isValid();}

	bool operator>=(const Presence&);
	bool operator==(const Presence&);
	Presence &operator=(Presence);
private:
	Jid j; // Jid
	QString t; // Type
	QString stat; // Status
	QString s; // Show
	int m_priority;
};

#endif //PRESENCE_H
