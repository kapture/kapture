#ifndef ROSTER_H
#define ROSTER_H

#include <QList>
#include <QString>

#include "jid.h"
#include "xmppcontact.h"

class Client;

class Roster
{
public:
	Roster(Client *client = 0);
	~Roster();

	void addContact(const QString& jid, const QString& nickname, const QString& subscription);
	QList<XmppContact*> contactList() const;
	void clear();

	Roster &operator=(const Roster o)
	{
		c = o.c;
		m_client = o.m_client;
		return *this;
	}

private:
	QList<XmppContact*> c;
	Client *m_client;
};
#endif
