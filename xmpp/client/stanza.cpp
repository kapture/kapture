/*
 *      Kapture
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include "stanza.h"
#include <QString>


Stanza::Stanza()
{

}

Stanza::Stanza(Kind kind, const QString& type, const QString& id, const QString& to, const QString& namespaceURI)
{
	doc = QDomDocument("");
	n = doc.createElement(kindToTagName(kind));
	
	if (!type.isEmpty())
		n.setAttribute("type", type);
	if (!id.isEmpty())
		n.setAttribute("id", id);
	if (!to.isEmpty())
		n.setAttribute("to", to);
	if (!namespaceURI.isEmpty())
		n.setAttribute("xmlns", namespaceURI);
}

Stanza::Stanza(const QDomElement &elem)
{
	n = elem;
}

Stanza::~Stanza()
{

}

Stanza::Kind Stanza::tagNameToKind(QString tagName) const
{
	if (tagName == "iq")
		return IQ;
	if (tagName == "presence")
		return Presence;
	if (tagName == "message")
		return Message;
	return BadStanza;
}

QString Stanza::kindToTagName(Kind kind) const
{
	if (kind == IQ)
		return "iq";
	if (kind == Presence)
		return "presence";
	if (kind == Message)
		return "message";
	return "";
}

void Stanza::setFrom(const Jid& from)
{
	if (from.isValid())
		n.setAttribute("from", from.full());
}

void Stanza::setTo(const Jid& to)
{
	if (to.isValid())
		n.setAttribute("to", to.full());
}

void Stanza::setId(const QString& id)
{
	if (!id.isEmpty())
		n.setAttribute("id", id);
}

void Stanza::setType(const QString& type)
{
	if (!type.isEmpty())
		n.setAttribute("type", type);
}

void Stanza::setKind(Kind kind)
{
	 n.setTagName(kindToTagName(kind));
}

Jid Stanza::from() const
{
	Jid f = Jid(n.attribute("from"));
	return f;
}

Jid Stanza::to() const
{
	Jid t = Jid(n.attribute("to"));
	return t;
}

QString Stanza::id() const
{
	return n.attribute("id");
}

QString Stanza::type() const
{
	return n.attribute("type");
}

Stanza::Kind Stanza::kind() const
{
	return tagNameToKind(n.localName());
}

QString Stanza::namespaceURI() const
{
	return n.namespaceURI();
}

QDomElement Stanza::element() const
{
	return n;
}
