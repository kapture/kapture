#include <stdlib.h>
#include <time.h>

#include <arpa/inet.h>

#include <QList>
#include <QHostAddress>
#include <QCoreApplication>
#include <QTimer>

#include <openssl/hmac.h>
#include <openssl/sha.h>

#include "task.h"

#include "stunclient.h"

#define STUN_PORT 3478

struct StunHeader
{
	StunHeader() : type(0), length(0) {}
	uint16_t type;
	uint16_t length;
	uint16_t cookie[2];
	uint16_t id[6];
};

// define types for a stun message 
uint16_t BindRequestMsg               = 0x0001;
uint16_t BindResponseMsg              = 0x0101;
uint16_t BindErrorResponseMsg         = 0x0111;
uint16_t SharedSecretRequestMsg       = 0x0002;
uint16_t SharedSecretResponseMsg      = 0x0102;
uint16_t SharedSecretErrorResponseMsg = 0x0112;

Attribute::Attribute(AttributeType type, const QByteArray& value, uint16_t size)
	: m_type(type), m_value(value), m_size(size), m_useCandidate(false)
{
	switch(type)
	{
	case 1:
		m_type = MappedAddress;
		loadAddress();
		break;
	case 4:
		m_type = SourceAddress;
		loadAddress();
		break;
	case 5:
		m_type = ChangedAddress;
		loadAddress();
		break;
	case 6:
		m_type = Username;
		loadUsername();
		break;
	case 8:
		m_type = MessageIntegrity;
		loadMessageIntegrity();
		break;
	case 0x24:
		m_type = Priority;
		loadPriority();
		break;
	case 0x25:
		m_type = UseCandidate;
		loadUseCandidate();
		break;
	case 0x8028:
		m_type = FingerPrint;
		loadFingerprint();
		break;
	case 0x802A:
		m_type = IceControlling;
		break;
	case 0x8029:
		m_type = IceControlled;
		break;
	default:
		m_type = UnknownType;
		return;
	}
}

Attribute::~Attribute()
{}

void Attribute::loadAddress()
{
	m_family = m_value.at(1) == 1 ? QAbstractSocket::IPv4Protocol : QAbstractSocket::IPv6Protocol;
	m_port = ((uint32_t) (m_value.at(2)) << 8) + (uint32_t) m_value.at(3);
	m_address = ((((uint32_t)m_value.at(4)) & 0xff) << 24) +
		    ((((uint32_t)m_value.at(5)) & 0xff) << 16) +
		    ((((uint32_t)m_value.at(6)) & 0xff) << 8) +
		    (((uint32_t)m_value.at(7)) & 0xff);
}

void Attribute::loadPriority()
{
	m_priority = ((((uint32_t)m_value.at(0)) & 0xff) << 24) +
		     ((((uint32_t)m_value.at(1)) & 0xff) << 16) +
		     ((((uint32_t)m_value.at(2)) & 0xff) << 8) +
		     (((uint32_t)m_value.at(3)) & 0xff);
}

void Attribute::loadUseCandidate()
{
	m_useCandidate = true;
}

void Attribute::loadUsername()
{
	m_username = QString::fromUtf8(m_value);
}

void Attribute::loadMessageIntegrity()
{
	m_integrity = m_value;
}

void Attribute::loadFingerprint()
{
	m_fingerprint = m_value;
}

StunClient::StunClient(const QString& server, const QString& server2, QObject *parent)
// : QTcpSocket(parent), m_server(server)
 : QUdpSocket(parent), m_server(server), m_server2(server2), changeIP(false), changePort(false), m_savedIP(0), m_mappedAddress(0)
{
	state = OtherState;
}

StunClient::~StunClient()
{
}


void StunClient::getServerReflexive()
{
	//Close connection if opened.
	if (QUdpSocket::state() != QAbstractSocket::UnconnectedState)
		QUdpSocket::close();
	QHostInfo::lookupHost(m_server, this, SLOT(dnsResponse(QHostInfo)));

	connect(&timer, SIGNAL(timeout()), SIGNAL(dnsTimeOut()));
	timer.start(15000);
}

void StunClient::dnsResponse(QHostInfo info)
{
	timer.stop();
	timer.disconnect(this, SLOT(dnsTimeOut()));
	if (info.error() != QHostInfo::NoError)
	{
		//emit errorResponse(info);
		qDebug() << "Error Response" << info.errorString();
		return;
	}

	//m_state = WaitResponse;

	connect(this, SIGNAL(connected()), SLOT(connectedSlot()));
	connect(this, SIGNAL(readyRead()), SLOT(dataReady()));
	connectToHost(info.addresses().at(0), STUN_PORT);
}

void StunClient::connectedSlot()
{
	StunHeader hdr;
	hdr.type = htons(BindRequestMsg);
	if (changeIP || changePort)
		hdr.length = 8;

	// Transaction ID
	srand(time(0)*rand());
	printf("Send with id = ");
	for (int i = 0; i < 8; i++)
	{
		hdr.id[i] = htons((uint16_t)(rand()));
		printf("%4x ", ntohs(hdr.id[i]));
	}
	printf("\n");

	QByteArray data((const char*) &hdr, sizeof(hdr));

	if (changePort || changeIP)
	{
		unsigned int attrHdr = htonl((3 << 16) + 8);
		for (int i = 0; i < 4; i++)
			data.append((int8_t) (attrHdr >> (3-i) * 8));
		unsigned int value = htonl(1 << (changeIP ? 2 : 1));
		for (int i = 0; i < 4; i++)
			data.append((int8_t) (value >> (3-i) * 8));
	}

	connect(&timer, SIGNAL(timeout()), SLOT(requestTimeOut()));
	timer.start(5000);

	write(data);
}

void StunClient::requestTimeOut()
{
	//qDebug() << "Request timeout";
	timer.stop();
	timer.disconnect(this, SLOT(requestTimeOut()));
	if (!changeIP && !changePort && state != CheckSymmetricNat)
		emit gotResponse(StunResponse(StunResponse::UDPBlocked));
	else if (changeIP && changePort && state == CheckFirewall)
		emit gotResponse(StunResponse(StunResponse::SymmetricFirewall));
	else if (changeIP && changePort && state == CheckSymmetricNat)
	{
		changeIP = false;
		changePort = false;

		qDebug() << "Setting changeIP =" << changeIP << "and changePort =" << changePort;
		qDebug() << "state =" << stateToString(state);

		if (!m_server2.isEmpty())
		{
			m_server = m_server2;
			disconnect(this, SLOT(connectedSlot()));
			disconnect(this, SLOT(dataReady()));
			getServerReflexive(); //FIXME:must be to a second server !
		}
		else
			emit gotResponse(StunResponse(m_mappedAddress, StunResponse::Unknown));
	}
	else if (!changeIP && changePort && state == CheckSymmetricNat)
		emit gotResponse(StunResponse(m_mappedAddress, StunResponse::RestrictedPortNAT));
}

void StunClient::dataReady()
{
	//FIXME:Use StunMessage
	//qDebug() << "Received response";
	QByteArray int8_tData = readAll();

	timer.stop();
	timer.disconnect(this, SLOT(requestTimeOut()));

	uint16_t *data = reinterpret_cast<uint16_t*>(int8_tData.data());

	StunHeader hdr;

	hdr.type = ntohs(*(data++));
	//qDebug() << "Type =" << hdr.type;

	if (hdr.type != BindResponseMsg)
		return;
	
	hdr.length = ntohs(*(data++));
	//qDebug() << "Length =" << hdr.length;
	
	//printf("Receive with id = ");
	for (int i = 0; i < 8; i++)
	{
		hdr.id[i] = ntohs(*(data++));
		//printf("%4x ", hdr.id[i]);
	}
	//printf("\n");

	uint16_t *end = data + hdr.length/2;
	while (data < end)
	{
		uint16_t attrType = ntohs(*(data++));
		uint16_t attrSize = ntohs(*(data++));
		//qDebug() << "Attribute (" << attrSize << ")";

		QByteArray attrData;
		uint16_t val;
		for (int i = 0; i < attrSize/2; i++)
		{
			val = ntohs(*(data++));
			attrData.append((int8_t) (val >> 8));
			attrData.append((int8_t) (val & 0x00ff));
			//printf("0x%2x 0x%2x ", (val >> 8) & 0x00ff, val & 0x00ff);
		}
		//printf("\n");

		Attribute attr((Attribute::AttributeType)attrType, attrData, attrSize);
		switch (attr.type())
		{
		case Attribute::MappedAddress:
			if (!m_savedIP)
				m_savedIP = attr.address();
			m_mappedAddress = attr.address();

			m_mappedPort = attr.port();
			//m_mappedProtocol = attr.family();
			break;
		case Attribute::SourceAddress:
		default:
			continue;
		}
	}
	
	qDebug() << QHostAddress(m_mappedAddress);
	if (m_mappedAddress == 0)
	{
		//FIXME:should never arrive but make sure we handle it
		return;
	}

	emit gotResponse(StunResponse(m_mappedAddress, StunResponse::Unknown));
	return;

/*
	if (!changeIP && !changePort && state != CheckSymmetricNat && m_mappedAddress == localAddress().toIPv4Address())
	{
		// Run test 2 and got to CheckSymmetricFirewall state
		state = CheckFirewall;
		changeIP = true;
		changePort = true;

		qDebug() << "Setting changeIP =" << changeIP << "and changePort =" << changePort;
		qDebug() << "state =" << stateToString(state);

		connectedSlot();
		return;
	}
	else if (!changeIP && !changePort && state != CheckSymmetricNat) // Different Ip's
	{
		state = CheckSymmetricNat;
		changeIP = true;
		changePort = true;

		qDebug() << "Setting changeIP =" << changeIP << "and changePort =" << changePort;
		qDebug() << "state =" << stateToString(state);

		connectedSlot();
		return;
	}
	else if (!changeIP && !changePort && state == CheckSymmetricNat && m_savedIP != m_mappedAddress)
	{
		emit gotResponse(StunResponse(m_mappedAddress, StunResponse::SymmetricNat));
		return;
	}
	else if (!changeIP && !changePort && state == CheckSymmetricNat)
	{
		changeIP = false;
		changePort = true;

		qDebug() << "Setting changeIP =" << changeIP << "and changePort =" << changePort;
		qDebug() << "state =" << stateToString(state);

		connectedSlot();
		return;
	}

	if (changeIP && changePort && state == CheckFirewall)
	{
		emit gotResponse(StunResponse(m_mappedAddress, StunResponse::NoNAT));
		return;
	}
	else if (changeIP && changePort && state == CheckSymmetricNat)
	{
		emit gotResponse(StunResponse(m_mappedAddress, StunResponse::FullConeNAT));
		return;
	}

	if (!changeIP && changePort)
	{
		emit gotResponse(StunResponse(m_mappedAddress, StunResponse::RestrictedConeNAT));
		return;
	}
*/
}

QString StunClient::stateToString(State s)
{
	switch(s)
	{
	case DNS:
		return "DNS";
		break;
	case CheckFirewall:
		return "CheckFirewall";
		break;
	case CheckSymmetricNat:
		return "CheckSymmetricNat";
		break;
	}

	return "Unknown";
}

/*
 * Implement StunResponse
 */

StunResponse::StunResponse(uint32_t address, NATType type)
 : m_natType(type)
{
	m_address = QHostAddress(address);
	qDebug() << "Address :" << m_address << "NAT type :" << type;
}

StunResponse::~StunResponse()
{
}

StunMessage::StunMessage(QByteArray allData, const QString& pwd)
{
	uint16_t *data = reinterpret_cast<uint16_t*>(allData.data());

	m_hdr = new StunHeader();

	m_hdr->type = ntohs(*(data++));

	m_hdr->length = ntohs(*(data++));

	// Get cookie
	for (int i = 0; i < 2; i++)
	{
		m_hdr->cookie[i] = ntohs(*(data++));
	}

	// Get id
	for (int i = 0; i < 6; i++)
	{
		m_hdr->id[i] = ntohs(*(data++));
	}

	m_transactionId.append(reinterpret_cast<char*>(m_hdr->id), 12);

	uint16_t *end = data + m_hdr->length/2;
	while (data < end)
	{
		uint16_t attrType = ntohs(*(data++));
		uint16_t attrSize = ntohs(*(data++));
		//qDebug() << "Attribute (" << attrSize << ")";

		QByteArray attrData;
		uint16_t val;
		for (int i = 0; i < attrSize/2; i++)
		{
			val = ntohs(*(data++));
			attrData.append((int8_t) (val >> 8));
			attrData.append((int8_t) (val & 0x00ff));
			//printf("0x%2x 0x%2x ", (val >> 8) & 0x00ff, val & 0x00ff);
		}
		if (attrSize % 2 == 1)
		{
			uint16_t tmp = *(data++);
			attrData.append(ntohs(tmp) >> 8);
			attrData.append(ntohs(tmp) & 0xff);
		}

		//Align on byte size if needed.
		if (attrSize % 4 >= 1)
		{
			uint16_t tmp = *(data++);
			attrData.append(ntohs(tmp) >> 8);
			attrData.append(ntohs(tmp) & 0xff);
		}
		//printf("\n");
		if ((Attribute::AttributeType)attrType == Attribute::MessageIntegrity)
		{
			unsigned char *md = new unsigned char[SHA_DIGEST_LENGTH];
			unsigned int md_len = SHA_DIGEST_LENGTH;
			//qDebug() << toNetworkBytes(true).toBase64();
			QByteArray b(
				(char*)HMAC(
					EVP_sha1(),
					pwd.toUtf8().constData(),
					pwd.toUtf8().length(),
					(const unsigned char*)toNetworkBytes(true).constData(),
					toNetworkBytes(true).length(),
					md,
					&md_len),
				md_len);
			//qDebug() << "Given :" << attrData.toBase64();
			//qDebug() << "Computed :" << b.toBase64();
		}

		m_attributes << new Attribute((Attribute::AttributeType)attrType, attrData, attrSize);
	}
}

StunMessage::StunMessage(MessageType t)
{
	m_hdr = new StunHeader;

	m_hdr->type = (uint16_t) t;

	m_hdr->cookie[0] = 0x2112;
	m_hdr->cookie[1] = 0xA442;

	m_transactionId = TaskManager::randomString(12).toUtf8();
	for (int i = 0; i < 6; i++)
	{
		m_hdr->id[i] = htons(*((uint16_t*)m_transactionId.data() + i));
	}
}

void StunMessage::setTransactionId(QByteArray id)
{
	m_transactionId = id;
}

StunMessage::~StunMessage()
{
	foreach (Attribute *attr, m_attributes)
	{
		delete attr;
	}

	delete m_hdr;
}

void StunMessage::addAttribute(Attribute::AttributeType t, uint32_t val)
{
	QByteArray ba;
	ba.append((val & 0xff000000) >> 24);
	ba.append((val & 0xff0000) >> 16);
	ba.append((val & 0xff00) >> 8);
	ba.append((val & 0xff));
	addAttribute(t, ba);
}

void StunMessage::addAttribute(Attribute::AttributeType t, uint64_t val)
{
	QByteArray ba;
	ba.append((val & 0xff00000000000000) >> 56);
	ba.append((val & 0xff000000000000) >> 48);
	ba.append((val & 0xff0000000000) >> 40);
	ba.append((val & 0xff00000000) >> 32);
	ba.append((val & 0xff000000) >> 24);
	ba.append((val & 0xff0000) >> 16);
	ba.append((val & 0xff00) >> 8);
	ba.append((val & 0xff));
	addAttribute(t, ba);
}

void StunMessage::addAttribute(Attribute::AttributeType t, const QString& val)
{
	addAttribute(t, val.toUtf8());
}

void StunMessage::addAttribute(Attribute::AttributeType t, const QByteArray& val)
{
	m_attributes << new Attribute(t, val, val.length());
}

bool StunMessage::hasAttribute(Attribute::AttributeType t)
{
	foreach(Attribute *attr, m_attributes)
	{
		if (attr->type() == t)
			return true;
	}

	return false;
}

QByteArray StunMessage::getBytes(Attribute::AttributeType t)
{
	foreach(Attribute *attr, m_attributes)
	{
		if (attr->type() == t)
			return attr->value();
	}

	return QByteArray();
}

QHostAddress StunMessage::getAddress(Attribute::AttributeType t, uint16_t *port)
{
	QByteArray ba = getBytes(t);
	*port = ((uint32_t) (ba.at(2)) << 8) + (uint32_t) ba.at(3);
	uint32_t address = ((((uint32_t)ba.at(4)) & 0xff) << 24) +
		    ((((uint32_t)ba.at(5)) & 0xff) << 16) +
		    ((((uint32_t)ba.at(6)) & 0xff) << 8) +
		    (((uint32_t)ba.at(7)) & 0xff);

	return QHostAddress(address);
}

QString StunMessage::getString(Attribute::AttributeType t)
{
	return QString::fromUtf8(getBytes(t));
}

uint32_t StunMessage::getInt(Attribute::AttributeType t)
{
	QByteArray ba = getBytes(t);
	return ((((uint32_t)ba.at(0)) & 0xff) << 24) +
		((((uint32_t)ba.at(1)) & 0xff) << 16) +
		((((uint32_t)ba.at(2)) & 0xff) << 8) +
		(((uint32_t)ba.at(3)) & 0xff);
}

uint64_t StunMessage::getLongInt(Attribute::AttributeType t)
{
	QByteArray ba = getBytes(t);
	return ((((uint64_t)ba.at(0)) & 0xff) << 56) +
			((((uint64_t)ba.at(1)) & 0xff) << 48) +
			((((uint64_t)ba.at(2)) & 0xff) << 40) +
			((((uint64_t)ba.at(3)) & 0xff) << 32) +
			((((uint64_t)ba.at(4)) & 0xff) << 24) +
			((((uint64_t)ba.at(5)) & 0xff) << 16) +
			((((uint64_t)ba.at(6)) & 0xff) << 8) +
			(((uint64_t)ba.at(7)) & 0xff);
}

void StunMessage::authenticate(QString password)
{
	QByteArray message = toNetworkBytes(true);
	unsigned char *md = new unsigned char[SHA_DIGEST_LENGTH];
	unsigned int md_len = SHA_DIGEST_LENGTH;

	qDebug() << toNetworkBytes(true).toBase64();

	QByteArray b(
		(char*)HMAC(
			EVP_sha1(),
			password.toUtf8().constData(),
			password.toUtf8().length(),
			(const unsigned char*)message.constData(),
			message.length(),
			md,
			&md_len),
		md_len);

	m_attributes << new Attribute(Attribute::MessageIntegrity, b, b.length());
}

QByteArray StunMessage::toNetworkBytes(bool forIntegrity) const
{
	QByteArray ret;
	m_hdr->length = 20;

	foreach (Attribute *m_attr, m_attributes)
	{
		m_hdr->length += m_attr->size() + 4;
		if (m_attr->size() % 4 != 0)
			m_hdr->length += 4 - (m_attr->size() % 4);
	}

	if (forIntegrity)
		m_hdr->length += 4;

	//qDebug() << "Length =" << m_hdr->length;

	ret.append(htons(m_hdr->type) & 0xff);
	ret.append(htons(m_hdr->type) >> 8);
	ret.append(htons(m_hdr->length) & 0xff);
	ret.append(htons(m_hdr->length) >> 8);

	for (unsigned int i = 0; i < 2; i++)
	{
		ret.append(m_hdr->cookie[i] >> 8);
		ret.append(m_hdr->cookie[i] & 0xff);
	}

	for (unsigned int i = 0; i < 6; i++)
	{
		ret.append(m_hdr->id[i] >> 8);
		ret.append(m_hdr->id[i] & 0xff);
	}

	foreach(Attribute *attr, m_attributes)
	{
		ret.append(htons(attr->type()) & 0xff);
		ret.append(htons(attr->type()) >> 8);
		ret.append(htons(attr->size()) & 0xff);
		ret.append(htons(attr->size()) >> 8);
		//qDebug() << attr->value().size();
		if (forIntegrity && attr->type() == Attribute::MessageIntegrity)
			continue;
		for (int i = 0; i < attr->value().size()/2; i++)
		{
			ret.append(attr->value()[i*2]);
			ret.append(attr->value()[i*2+1]);
		}
	}

	return ret;
}

StunMessage::MessageType StunMessage::type() const
{
	return (MessageType)m_hdr->type;
}
