#ifndef STUN_CLIENT_H
#define STUN_CLIENT_H

#include <stdint.h>

#include <QObject>
//#include <QTcpSocket>
#include <QUdpSocket>
#include <QHostInfo>
#include <QTimer>

/**
 * TODO:
 * 	- Determine best stuns server through SRV records (possible with Qt ?)
 * 	- Secure STUN
 */

struct StunHeader;

class Attribute
{
public:
	enum AttributeType
	{
		MappedAddress = 0x1,
		SourceAddress = 0x4,
		ChangedAddress = 0x5,
		Username = 0x6,
		MessageIntegrity = 0x8,
		Priority = 0x24,
		UseCandidate = 0x25,
		FingerPrint = 0x8028,
		IceControlled = 0x8029,
		IceControlling = 0x802A,
		UnknownType = 0xffff
	};

	Attribute(AttributeType type, const QByteArray& value, uint16_t size);
	~Attribute();


	QByteArray value() const {return m_value;}
	uint16_t type() const {return m_type;}
	uint16_t size() const {return m_size;}
	uint32_t address() const {return m_address;}
	uint16_t port() const {return m_port;}

	uint32_t priority() const {return m_priority;}
	bool useCandidate() const {return m_useCandidate;}

private:
	AttributeType m_type;
	QByteArray m_value;
	uint16_t m_size;
	QAbstractSocket::NetworkLayerProtocol m_family;
	uint16_t m_port;
	uint32_t m_address;

	uint32_t m_priority;

	bool m_useCandidate;

	QString m_username;

	QByteArray m_integrity;
	QByteArray m_fingerprint;

	void loadAddress();
	void loadPriority();
	void loadUseCandidate();
	void loadUsername();
	void loadMessageIntegrity();
	void loadFingerprint();
};

class StunResponse
{
public:
	enum NATType {UDPBlocked = 0, SymmetricNat, SymmetricFirewall, NoNAT, FullConeNAT, RestrictedConeNAT, RestrictedPortNAT, Unknown};
	StunResponse(uint32_t address, NATType type);
	StunResponse(NATType type) : m_natType(type), m_error(type < 3) {qDebug() << "type = " << type;}
	~StunResponse();

	QHostAddress address() const {return m_address;}
	bool error() const {return m_error;}
	NATType natType() const {return m_natType;}
	bool usable() const {return !m_address.isNull();}

private:
	QHostAddress m_address;
	NATType m_natType;
	bool m_error;
};


class StunMessage
{
public:
	enum MessageType
	{
		BindingRequest = 0x0001,
		BindingResponse = 0x0101
	};

	StunMessage(QByteArray, const QString &pwd = QString());
	StunMessage(MessageType);
	~StunMessage();

	QList<Attribute*> attributes() const {return m_attributes;}

	void addAttribute(Attribute::AttributeType, uint32_t);
	void addAttribute(Attribute::AttributeType, uint64_t);
	void addAttribute(Attribute::AttributeType, const QString&);
	void addAttribute(Attribute::AttributeType, const QByteArray&);
	void setTransactionId(QByteArray id);
	//void addAttribute(Attribute::AttributeType, );

	QByteArray getBytes(Attribute::AttributeType);
	QHostAddress getAddress(Attribute::AttributeType, uint16_t *port);
	QString getString(Attribute::AttributeType);
	uint32_t getInt(Attribute::AttributeType);
	uint64_t getLongInt(Attribute::AttributeType);
	QByteArray transactionId() const {return m_transactionId;}
	MessageType type() const;

	bool hasAttribute(Attribute::AttributeType);

	QByteArray toNetworkBytes(bool forIntegrity = false) const;

	void authenticate(QString password);

private:
	StunHeader *m_hdr;
	QList<Attribute*> m_attributes;
	QByteArray m_transactionId;
};

//class SecureStunClient : public QSslSocket
//class StunClient : public QTcpSocket
class StunClient : public QUdpSocket
{
	Q_OBJECT
public:
	StunClient(const QString& server, const QString& server2, QObject *parent = 0);
	~StunClient();

	void getServerReflexive();

signals:
	void gotResponse(const StunResponse&);
	void dnsTimeOut();

private slots:
	void dnsResponse(QHostInfo);
	void connectedSlot();
	void dataReady();
	void requestTimeOut();

private:
	enum State {DNS, CheckFirewall, CheckSymmetricNat, OtherState};
	QString m_server;
	QString m_server2;
	bool changeIP;
	bool changePort;
	uint32_t m_savedIP;
	uint32_t m_mappedAddress;
	uint16_t m_mappedPort;
	State state;
	QTimer timer;

	QString stateToString(State s);
};


#endif
