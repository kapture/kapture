#ifndef TASK_H
#define TASK_H

#include <QObject>
#include <QDomDocument>
#include <QList>
#include "xmpp.h"
#include "jid.h"
#include "taskmanager.h"

class Task : public QObject
{
	Q_OBJECT
public:
	Task(TaskManager *parent) : QObject(parent), m_taskManager(parent) {}
	virtual ~Task() {}
	virtual bool canProcess(const Stanza&) const = 0;
	virtual void processStanza(const Stanza&) = 0;
	struct StreamHost
	{
		Jid jid;
		QString host;
		int port;
	};

signals:
	void finished();

private:
	bool processed;
	TaskManager *m_taskManager;
};

#endif
