/*
 *      Kapture
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include "task.h"
#include "taskmanager.h"
#include <cstdlib>
#include <ctime>


TaskManager::TaskManager(QObject *parent):
	QObject(parent)
{
	srand((unsigned)time(0));
}

TaskManager::~TaskManager()
{
	//FIXME:some tasks are still in taskList but are destroyed here...
	for (int i = 0; i < taskList.count(); i++)
		delete taskList[i];
	//taskList.clear();
}

void TaskManager::appendTask(Task* t)
{
	connect(t, SIGNAL(destroyed(QObject*)), SLOT(removeChildSlot(QObject*)));
	taskList << t;
	qDebug() << taskList;
}

void TaskManager::processStanza(const Stanza& s)
{
	bool processed = false;

	for (int i = 0; i < taskList.count(); i++)
	{
		if (taskList[i]->canProcess(s))
		{
			//printf("[TASK] Ok, processing...\n");
			taskList[i]->processStanza(s);
			processed = true;
			break;
		}
	}

	if (!processed)
		printf("OO Must process that !\n");
}

void TaskManager::removeChildSlot(QObject* childTask)
{
	/*
	 * Note : reinterpret cast is needed because the destroyed signal is
	 * emitted after the Task instance has been destroyed.
	 */
	removeChild(reinterpret_cast<Task*>(childTask));
}

void TaskManager::removeChild(Task* childTask)
{
	for (int i = 0; i < taskList.count(); i++)
	{
		if (taskList[i] == childTask)
		{
			taskList.removeAt(i);
			break;
		}
	}
}

QString TaskManager::randomString(int size)
{
	QString ret(size, '1');
	for (int i = 0; i < size; i++)
		ret[i] = (char)((rand()%26)+97);

	return ret;
}
