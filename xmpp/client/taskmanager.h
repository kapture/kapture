#ifndef TASK_MANAGER_H
#define TASK_MANAGER_H

#include <QObject>
#include <QDomDocument>
#include <QList>
#include "xmpp.h"
#include "jid.h"

class Task;

class TaskManager : public QObject
{
	Q_OBJECT
public:
	TaskManager(QObject *parent = 0);
	~TaskManager();
	void processStanza(const Stanza&);
	static QString randomString(int size);
	struct StreamHost
	{
		Jid jid;
		QString host;
		int port;
	};
	void removeChild(Task* childTask);

signals:
	void finished();

private slots:
	void removeChildSlot(QObject* childTask);

private:
	void appendTask(Task* t);
	QList<Task*> taskList;
};

#endif
