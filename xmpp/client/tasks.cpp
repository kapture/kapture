/*
 *      Kapture -- tasks.cpp
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include <QTcpServer>
#include <QSysInfo>
#include <QDebug>

#include "tasks.h"
#include "client.h"

#define TIMEOUT 			30000

QString osVersionString()
{
#ifdef Q_WS_WIN
	switch (QSysInfo::WindowsVersion)
	{
	case QSysInfo::WV_32s :
		return "Windows 3.1 with Win 32s";
	case QSysInfo::WV_95 :
		return "Windows 95";
	case QSysInfo::WV_98 :
		return "Windows 98";
	case QSysInfo::WV_Me :
		return "Windows ME";
	case QSysInfo::WV_NT :
		return "Windows NT";
	case QSysInfo::WV_XP :
		return "Windows XP";
	case QSysInfo::WV_2000 :
		return "Windows 2000";
	case QSysInfo::WV_VISTA :
		return "Windows Vista";
	case QSysInfo::WV_2003 :
		return "Windows 2003";
	case QSysInfo::WV_
	}
	return "Windows";

#endif
#ifdef Q_WS_MAC
	
	switch (QSysInfo::MacintoshVersion)
	{
	case QSysInfo::MV_PANTHER :
		return "Mac OS X (panther)";
	case QSysInfo::MV_TIGER :
		return "Mac OS X (tiger)";
	case QSysInfo::MV_LEOPARD :
		return "Mac OS X (leopard)";
	}
	return "Mac OS X";

#endif
#ifdef Q_WS_X11

	return "GNU/Linux"; //FIXME: more details.
#endif
	return "Unknown";
}

RosterTask::RosterTask(Xmpp* xmpp, TaskManager* parent, Client *client)
	:Task(parent), p(xmpp)
{
	m_roster = Roster(client);
}

RosterTask::~RosterTask()
{

}

void RosterTask::getRoster(const Jid& j)
{
	m_roster.clear();
	QString type = "get";
	id = TaskManager::randomString(6);
	
	Stanza stanza(Stanza::IQ, type, id);
	QDomDocument doc("");
	stanza.setFrom(j);
	QDomElement c = doc.createElement("query");
	c.setAttribute("xmlns", "jabber:iq:roster");
	stanza.element().appendChild(c);
	
	p->write(stanza);
}

void RosterTask::addItem(const Jid& jid, const QString& name)
{
/* 
 * TODO:
 * http://www.xmpp.org/rfcs/rfc3921.html#rfc.section.7.4
 */
	QString type = "set";
	id = TaskManager::randomString(8);

	Stanza stanza(Stanza::IQ, type, id, QString());
	QDomDocument doc("");
	QDomElement query = doc.createElement("query");
	query.setAttribute("xmlns", "jabber:iq:roster");
	QDomElement item = doc.createElement("item");
	item.setAttribute("jid", jid.bare());
	if (name != "")
		item.setAttribute("name", name);
	
	query.appendChild(item);
	stanza.element().appendChild(query);

	p->write(stanza);
}

void RosterTask::delItem(const Jid& jid)
{
	Q_UNUSED(jid)
}

bool RosterTask::canProcess(const Stanza& s) const
{
	qDebug() << "[RosterTask]";
	if (s.kind() != Stanza::IQ)
		return false;

	qDebug() << s.element().firstChildElement().namespaceURI() << s.namespaceURI();

	if (s.element().firstChildElement().namespaceURI() == "jabber:iq:roster" && s.namespaceURI() == "jabber:client")
		return true;

	return false;
}

void RosterTask::processStanza(const Stanza& s)
{
	m_roster.clear();
	QString j;
	QString n;
	QString subs;
	QDomElement query = s.element().firstChildElement("query");

	QDomNodeList items = query.childNodes();
	for (int i = 0; i < items.count(); i++)
	{
		j = items.at(i).toElement().attribute("jid");
		n = items.at(i).toElement().attribute("name");
		subs = items.at(i).toElement().attribute("subscription");
		qDebug() << "[RosterTask] Add " << j;
		m_roster.addContact(j, n, subs);
	}

	emit finished();
}

Roster RosterTask::roster() const
{
	return m_roster;
}

//----------------------------------
// PresenceTask
//----------------------------------

/*
 * Manage this resource presence and status.
 * Changes it with the setPresence() method.
 */

PresenceTask::PresenceTask(Xmpp *xmpp, TaskManager* parent)
	:Task(parent), p(xmpp)
{
	waitSub = false;
}

PresenceTask::~PresenceTask()
{

}

void PresenceTask::setPresence(const QString& show, const QString& status, const QString& type)
{
	Stanza stanza(Stanza::Presence, type, TaskManager::randomString(8), QString());
	QDomNode node = stanza.element();
	QDomDocument doc("");

	//QDomElement e = doc.createElement("presence");
	
	if (show != "")
	{
		QDomElement s = doc.createElement("show");
		QDomText val = doc.createTextNode(show);
		s.appendChild(val);
		node.appendChild(s);
	}
	
	if (status != "")
	{
		QDomElement s = doc.createElement("status");
		QDomText val = doc.createTextNode(status);
		s.appendChild(val);
		node.appendChild(s);
	}

	if (p)
		p->write(stanza);

	emit finished();
}

void PresenceTask::setSubscription(const Jid& to, const QString&  type)
{
	if (type == "subscribe")
		waitSub = true;
	Stanza stanza(Stanza::Presence, type, QString(), to.bare());
	stanza.setTo(to);
	p->write(stanza);
}

bool PresenceTask::canProcess(const Stanza& s) const
{
	qDebug() << "[PresenceTask]";
	if (s.kind() == Stanza::Presence && (s.type() == "subscribed" || s.type() == "unsubscribed") && waitSub)
		return true;
	return false;
}

void PresenceTask::processStanza(const Stanza& s)
{
	qDebug() << "[PresenceTask] processStanza : Not implemented yet !";
	waitSub = false;
	if (s.type() == "subscribed")
		emit subApproved();
	else if (s.type() == "unsubscribed")
		emit subRefused();
}

//----------------------------------
// PullPresenceTask
//----------------------------------

/*
 * Received and process new presences from contacts.
 */

PullPresenceTask::PullPresenceTask(TaskManager* parent)
	:Task(parent)
{

}

PullPresenceTask::~PullPresenceTask()
{

}

bool PullPresenceTask::canProcess(const Stanza& s) const
{
	qDebug() << "[PullPresenceTask]";
	if (s.kind() == Stanza::Presence)
		return true;
	return false;
}

void PullPresenceTask::processStanza(const Stanza& stanza)
{
	from = stanza.from();
	type = stanza.type().isEmpty() ? "available" : stanza.type();
	QDomElement s = stanza.element();

	if (!s.hasChildNodes())
	{
		show = "";
		status = "";
		emit presenceFinished();
		return;
	}
	s = s.firstChildElement();
	while(!s.isNull())
	{
		if (s.localName() == "show")
		{
			if (s.firstChild().isText())
				show = s.firstChild().toText().data();
			else
				show = "";
		}
		if (s.localName() == "status")
		{
			if (s.firstChild().isText())
				status = s.firstChild().toText().data();
			else
				status = "";
		}
		/*
		 * No priority managed yet, 
		 * no VCard managed yet.
		 */
		s = s.nextSibling().toElement();
	}

	emit presenceFinished();
}

Presence PullPresenceTask::getPresence()
{
	
	Presence pr(type, status, show);
	pr.setFrom(from);
	type = QString("");
	status = QString("");
	show = QString("");
	return pr;
}

//-----------------------------
// PullMessageTask
//-----------------------------

/*
 * Receives and process new messages from contacts.
 */

PullMessageTask::PullMessageTask(TaskManager* parent)
	:Task(parent)
{

}

PullMessageTask::~PullMessageTask()
{

}

bool PullMessageTask::canProcess(const Stanza& s) const
{
	qDebug() << "[PullMessageTask]";
	if (s.kind() == Stanza::Message)
		return true;
	return false;
}

void PullMessageTask::processStanza(const Stanza& stanza)
{
/* TODO: should manage that kind of error :
 * <message xmlns='jabber:client' from='ffgfg@localhost' to='cazou88@localhost/Kapture' id='mjzodi' type='error'>
 *  <error type='cancel' code='503'>
 *   <service-unavailable xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
 *  </error>
 *  <body>dddddddddddddd</body>
 * </message>
 * Currently, The message will just be shown to the user af if he sent it to himself.
 * That really is not good.
 */

	ty = stanza.type();
	f = stanza.from();
	t = stanza.to();
	QDomElement s = stanza.element().toElement();
	qDebug() << QString(stanza.element().toDocument().toByteArray());
	m = "";
	sub = "";
	thr = "";
	
	if (!s.hasChildNodes())
	{
		emit messageFinished();
		return;
	}
	
	s = s.firstChildElement();
	while (!s.isNull())
	{
		if (s.localName() == "body")
		{
			/*
			 * FIXME:
			 * There can be more than one body child,
			 * giving traductions of the message in other languages
			 */
			if (s.firstChild().isText())
				m = s.firstChild().toText().data();
		}

		if (s.localName() == "subject") // Not used yet.
		{
			if (s.firstChild().isText())
				sub = s.firstChild().toText().data();
		}
		
		if (s.localName() == "thread") // Not used yet.
		{
			if (s.firstChild().isText())
				thr = s.firstChild().toText().data();
		}
		s = s.nextSibling().toElement();
	}

	qDebug() << "PullMessageTask::processStanza() : debug1";

	emit messageFinished();
}

Message PullMessageTask::getMessage()
{
	return Message(f, t, m, ty, sub, thr);
}

//-------------------------------------
// MessageTask
//-------------------------------------

MessageTask::MessageTask(TaskManager* parent)
	:Task(parent)
{}

MessageTask::~MessageTask()
{}

void MessageTask::sendMessage(Xmpp* p, const Message& message)
{
	QString id = TaskManager::randomString(6); 
	Jid to = message.to();
	Stanza stanza(Stanza::Message, message.type(), id, to.full());
	QDomDocument doc("");
	QDomElement s = doc.createElement("body");
	QDomText val = doc.createTextNode(message.message());
	s.appendChild(val);
	stanza.element().appendChild(s);

	// TODO:Thread, subjects... later.
	
	p->write(stanza);
	emit finished();
}

//-------------------------------------
// StreamTask
//-------------------------------------
/*
 *
 */


StreamTask::StreamTask(TaskManager* parent, Xmpp *xmpp, const Jid& t)
	:Task(parent)
{
	p = xmpp;
	to = t;
}

StreamTask::~StreamTask()
{
	
}

bool StreamTask::canProcess(const Stanza& s) const
{
	qDebug() << "[StreamTask]";
	if (s.kind() != Stanza::IQ)
		return false;
	
	QString ns = s.element().firstChildElement().namespaceURI();
	
	//qDebug() << "Type = %s, Id = %s (expected %s), namespace =" << s.type().toLatin1().constData(),
	//		s.id().toLatin1().constData(),
	//		id.toLatin1().constData(),
	//		s.element().toElement().namespaceURI().toLatin1().constData());

	if (s.id() == id /*&&
	    s.element().toElement().namespaceURI() == "jabber:client"*/)
	    	return true;
	/*
	 * Other namespaces should be supported here.
	 * http://jabber.org/protocol/disco#info is only for features discovering.
	 */
	return false;
}

void StreamTask::processStanza(const Stanza& s)
{
/*
 * FIXME:
 * 	Working with proxies does not seem to work correctly.
 * TODO:
 * 	Some correction have been done, check if proxies is repaired.
 */
	switch (state)
	{
	case WaitDiscoInfo:
		if (s.type() == "result")
		{
			QDomNode node = s.element().firstChild();
			if (node.localName() != "query" || node.namespaceURI() != XMLNS_DISCO_INFO)
			{
				qDebug() << "[STREAMTASK] Bad stanza. Stoppping here.";
				return;
			}
			node = node.firstChild();
			while (!node.isNull())
			{
				if (node.localName() == "feature")
					featureList << node.toElement().attribute("var");
				node = node.nextSibling();
			}
			emit infoDone();
		}
		if (s.type() == "error")
		{
			//emit error(DiscoInfoError);
		}
		break;
	case WaitAcceptFileTransfer:
		if (s.type() == "result")
		{
			QDomNode node = s.element().firstChild();
			if (node.localName() != "si")
			{
				qDebug() << "[STREAMTASK] Not SI tag, stop.";
				//emit error();
				return;
			}
			if (node.localName() == "file")
				node = node.nextSibling();
			node = node.firstChild();
			if (node.localName() != "feature")
			{
				qDebug() << "[STREAMTASK] Not FEATURE tag, stop.";
				//emit error();
				return;
			}
			node = node.firstChild();
			if (node.localName() != "x")
			{
				qDebug() << "[STREAMTASK] Not X tag, stop.";
				//emit error();
				return;
			}
			node = node.firstChild();
			if (node.localName() != "field")
			{
				qDebug() << "[STREAMTASK] Not FIELD tag, stop.";
				//emit error();
				return;
			}
			node = node.firstChild();
			if (node.localName() != "value")
			{
				qDebug() << "[STREAMTASK] Not VALUE tag, stop.";
				//emit error();
				return;
			}
			profileToUse = node.firstChild().toText().data();
			qDebug() << "[STREAMTASK] Ok, Using %s profile to transfer file" << profileToUse;
			getProxies();
		}
		if (s.type() == "error")
		{
			QDomNode node = s.element().firstChild();
			if (node.localName() == "error")
			{
				int errCode = node.toElement().attribute("code", "1").toInt();
				QString errorString = node.firstChild().toText().data();
				emit error(errCode, errorString);
			}
		}
		break;
	case WaitProxies :
		if (s.type() == "result" && s.element().firstChildElement().namespaceURI() == XMLNS_DISCO_ITEMS)
		{
			QDomNode node = s.element().firstChild();
			node = node.firstChild();
			while (!node.isNull())
			{
				if (node.localName() == "item")
					itemList << node.toElement().attribute("jid");
				node = node.nextSibling();
			}
			//emit infoDone();
		}
		qDebug() << "[STREAMTASK] Proxies are : ";
		for (int i = 0; i < itemList.count(); i++)
			qDebug() << "[STREAMTASK]  * " << itemList[i];
		if (itemList.count() <= 0)
		{
			emit finished();
			return;
		}
		state = WaitIsProxy;
		isAProxy(itemList.takeFirst());
		break;
	case WaitIsProxy :
		if (s.type() == "result" && s.element().firstChildElement().namespaceURI() == XMLNS_DISCO_INFO)
		{
			QDomNode node = s.element().firstChild();
			node = node.firstChild();
			qDebug() << "[STREAMTASK] node =" <<  node.localName();
			while (!node.isNull())
			{
				/*qDebug() << "[STREAMTASK] * Indentity = %s, Category = %s, type = %s\n",
				        node.localName().toLatin1().constData(),
					node.toElement().attribute("category").toLatin1().constData(), 
					node.toElement().attribute("type").toLatin1().constData());
*/
				if ((node.localName() == "identity") &&
				    (node.toElement().attribute("category") == "proxy") &&
				    (node.toElement().attribute("type") == "bytestreams"))
				{
					proxyList << s.element().toElement().attribute("from");
					//proxyList2 << s.element().toElement().attribute("from");
				}
				node = node.nextSibling();
			}
			qDebug() << "[STREAMTASK] itemList.count() =" <<  itemList.count();
			if (itemList.empty())
			{
				qDebug() << "[STREAMTASK]  ************* Number of Proxies =" << proxyList.count();
				if (proxyList.count() <= 0)
				{
					emit finished();
					return;
				}
				else
				{
					state = WaitProxyIp;
					getProxyIp(proxyList.takeFirst());
				}
			}
			else
				isAProxy(itemList.takeFirst());
		}
		break;
	case WaitProxyIp :
		if (s.type() == "result" && s.element().firstChildElement().namespaceURI() == XMLNS_BYTESTREAMS)
		{
			QDomNode node = s.element().firstChild();
			node = node.firstChild();
			if (node.localName() == "streamhost")
			{
				ipList << node.toElement().attribute("host", "UNKNOWN");
				portList << node.toElement().attribute("port", "UNKNOWN");
				//zeroconfList << node.toElement().attribute("zeroconf", "UNKNOWN");
			}
			if (proxyList.empty())
				emit finished();
			else
				getProxyIp(proxyList.takeFirst());
		}
	}
}

QStringList StreamTask::proxies() const
{
	return proxyList;
}

QStringList StreamTask::ips() const
{
	return ipList;
}

QStringList StreamTask::ports() const
{
	return portList;
}

void StreamTask::discoInfo()
{
/*
 * Stream Initiation
 *  - Discovers if Receiver implements the desired profile.
 *  - Offers a stream initiation.
 *  - Receiver accepts stream initiation.
 *  - Sender and receiver prepare for using negotiated profile and stream.
 *  See XEP 0095 : http://www.xmpp.org/extensions/xep-0095.html
 */
	id = TaskManager::randomString(8);
	Stanza stanza(Stanza::IQ, "get", id, to.full());
	QDomNode node = stanza.element();
	QDomDocument d("");
	QDomElement query = d.createElement("query");
	query.setAttribute("xmlns", XMLNS_DISCO_INFO);
	node.appendChild(query);

	state = WaitDiscoInfo;
	p->write(stanza);
}

bool StreamTask::supports(const QString& profile)
{
	for (int i = 0; i < featureList.count(); i++)
		if (featureList.at(i) == profile)
			return true;
	return false;
}

QString StreamTask::negProfile() const
{
	return profileToUse;
}

void StreamTask::getProxies()
{
	id = TaskManager::randomString(8);
	/* FIXME:Should not work with "UsePersonnalServer"
	 * Maybe in that case, one's has to use a personnal proxy.
	 */
	Stanza stanza(Stanza::IQ, "get", id, p->node().domain());
	QDomNode node = stanza.element();
	QDomDocument doc("");

	QDomElement query = doc.createElement("query");
	query.setAttribute("xmlns", XMLNS_DISCO_ITEMS);
	
	node.appendChild(query);
	state = WaitProxies;
	p->write(stanza);
}

void StreamTask::isAProxy(QString host)
{
	id = TaskManager::randomString(8);
	Stanza stanza(Stanza::IQ, "get", id, host);
	QDomNode node = stanza.element();
	QDomDocument doc("");

	QDomElement query = doc.createElement("query");
	query.setAttribute("xmlns", XMLNS_DISCO_INFO);
	
	node.appendChild(query);
	p->write(stanza);
}

void StreamTask::getProxyIp(QString proxy)
{
	id = TaskManager::randomString(8);
	Stanza stanza(Stanza::IQ, "get", id, proxy);
	QDomNode node = stanza.element();
	QDomDocument doc("");

	QDomElement query = doc.createElement("query");
	query.setAttribute("xmlns", XMLNS_BYTESTREAMS);
	
	node.appendChild(query);
	p->write(stanza);
}

void StreamTask::initStream(const QFile& f)
{
	id = TaskManager::randomString(6);
	Stanza stanza(Stanza::IQ, "set", id, to.full());
	QDomNode node = stanza.element();
	QDomDocument doc("");
	
	QDomElement si = doc.createElement("si");
	si.setAttribute("xmlns", XMLNS_SI);
	si.setAttribute("id", SID = TaskManager::randomString(2));
	si.setAttribute("profile", XMLNS_FILETRANSFER);
	
	QDomElement file = doc.createElement("file");
	file.setAttribute("xmlns", XMLNS_FILETRANSFER);
	QFileInfo fi(f.fileName());
	file.setAttribute("name", fi.fileName());
	file.setAttribute("size", QString("%1").arg((int)f.size()));

	QDomElement feature = doc.createElement("feature");
	feature.setAttribute("xmlns", XMLNS_FEATURENEG);

	QDomElement x = doc.createElement("x");
	x.setAttribute("xmlns", "jabber:x:data");
	x.setAttribute("type", "form");

	QDomElement field = doc.createElement("field");
	field.setAttribute("var", "stream-method");
	field.setAttribute("type", "list-single");

	QDomElement option1 = doc.createElement("option");

	QDomElement value1 = doc.createElement("value");

	QDomText bytestream = doc.createTextNode("http://jabber.org/protocol/bytestreams"); //Fully supported
	//FIXME:Use Defines

	node.appendChild(si);
	si.appendChild(file);
	si.appendChild(feature);
	feature.appendChild(x);
	x.appendChild(field);
	field.appendChild(option1);
	option1.appendChild(value1);
	value1.appendChild(bytestream);
	
	//qDebug() << "Node =" << stanza.element().toDocument().toString().toLatin1().constData());
	//rintf("Node tag =" << node.localName().toLatin1().constData());
	state = WaitAcceptFileTransfer;
	p->write(stanza);
}

Jid StreamTask::toJid() const
{
	return to;
}

QString StreamTask::sid() const
{
	return SID;
}

//QFile StreamTask::file() const
//{
//	return f;
//}

//----------------------------------------
// FileTransferTask
//----------------------------------------

FileTransferTask::FileTransferTask(TaskManager* parent, const Jid& t, Xmpp *xmpp)
	:Task(parent)
{
	to = t;
	p = xmpp;
	writtenData = 0;
	prc = 0;
	prc2 = 0;
	connectToProxy = true;
	isRecept = false;
}

FileTransferTask::~FileTransferTask()
{
	delete socks5Socket;
	delete socks5;
	// Remove all server sockets
	for (int i = 0; i < serverList.count(); i++)
	{
		delete serverList.at(i);
		serverList.removeAt(i);
	}
}

bool FileTransferTask::canProcess(const Stanza& s) const
{
	qDebug() << "[FileTransferTask]";
	qDebug() << "Type = %s, Id = %s (expected %s), namespace =" << s.type() <<
			s.id() <<
			id <<
			s.element().toElement().namespaceURI();
	if (s.kind() == Stanza::IQ && s.id() == id) //Check more....
		return true;
	return false;
}

#define STEP 512

void FileTransferTask::processStanza(const Stanza& s)
{
	if (s.type() != "result")
	{
		;
		//emit error();
	}

	QDomNode node = s.element().firstChild();
	
	if (!connectToProxy)
	{
		f->open(QIODevice::ReadOnly);
		QFileInfo fi(f->fileName());
		QString str = fi.fileName();
		emit prcentChanged(to, str, 0); // Tell chatWin that the transfer begins
		writtenData = 0;
		socks5Socket->write(f->readAll());
		connect(socks5Socket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWrittenSlot(qint64)));
		prc = 0;
		prc2 = 0;
	}
	else
	{
		if (node.localName() != "streamhost-used")
		{
			//emit error();
			return;
		}
		timeOut->stop(); // Connection received, no need to wait anymore.
		delete timeOut; // Unused now.
		
		qDebug() << "Must be using a proxy.";
		// Search the used proxy.
		int i;
		for (i = 0; i < proxies.count(); i++)
		{
			if (proxies.at(i) == node.toElement().attribute("jid"))
			{
				usedProxy = proxies.at(i);
				usedIP	  = ips.at(i);
				usedPort  = ports.at(i);
				break;
			}
		}
		socks5Socket = new QTcpSocket();
		socks5Socket->connectToHost(usedIP, usedPort.toLatin1().toInt());
		connect(socks5Socket, SIGNAL(connected()), this, SLOT(connectedToProxy()));
	}
}

void FileTransferTask::connectedToProxy()
{
	socks5 = new Socks5(s, p->node(), to);
	connect(socks5, SIGNAL(readyRead()), this, SLOT(readS5()));
	connect(socks5, SIGNAL(established()), this, SLOT(notifyStart()));
	connect(socks5Socket, SIGNAL(readyRead()), this, SLOT(dataAvailable()));
	socks5->connect();
}

void FileTransferTask::bytesWrittenSlot(qint64 sizeWritten)
{
	writtenData += sizeWritten;

	prc = (int)(((float)writtenData/(float)f->size())*100);
	if (prc2 != prc)
	{
		QFileInfo fi(f->fileName());
		QString str = fi.fileName();
		emit prcentChanged(to, str, prc);
	}
	prc2 = (int)(((float)writtenData/(float)f->size())*100);
	
	if (writtenData == f->size())
	{
		socks5Socket->disconnect();
		socks5Socket->disconnectFromHost();
		emit finished();
	}
}

void FileTransferTask::start(const QString& profile, const QString& SID, const QString& file,
			     const QStringList prox, const QStringList ip, const QStringList p)
{
	qDebug() << prox.count() << "proxies";
	proxies = prox;
	ips = ip;
	ports = p;
	s = SID;
	qDebug() << "File name =" << file;
	f = new QFile(file);
	fileName = file;
	
	//FIXME:Use Defines
	if (profile == "http://jabber.org/protocol/bytestreams")
		startByteStream();
}

void FileTransferTask::startByteStream()
{
	// Get network address.
	id = TaskManager::randomString(6);
	Stanza stanza(Stanza::IQ, "set", id, to.full());
	stanza.setFrom(p->node());
	QDomDocument doc("");
	QDomElement query = doc.createElement("query");
	//FIXME:Use Defines
	query.setAttribute("xmlns", "http://jabber.org/protocol/bytestreams");
	query.setAttribute("sid", s);
	query.setAttribute("mode", "tcp");
	
	timeOut = new QTimer();
	QNetworkInterface *interface = new QNetworkInterface();
	/* TODO:
	 * 	Should also add the external IP.
	 * 	For example, download it from
	 * 		http://www.swlink.net/~styma/REMOTE_ADDR.shtml
	 * 		or
	 * 		http://www.whatismyip.com/automation/n09230945.asp
	 * 	or use a webservice.
	 * 	This is not prioritary.
	 */
	for (int i = 0; i < interface->allAddresses().count(); i++)
	{
		qDebug() << "IP :" << interface->allAddresses().at(i).toString();
		QDomElement streamHost = doc.createElement("streamhost");
		streamHost.setAttribute("jid", p->node().full()); //FIXME:Should I set the full JID ?
		streamHost.setAttribute("host", interface->allAddresses().at(i).toString());
		streamHost.setAttribute("port", "8015"); //TODO: get it from config !!
		query.appendChild(streamHost);
		
		QTcpServer *tcpServer = new QTcpServer();
		tcpServer->listen(QHostAddress(interface->allAddresses().at(i).toString()), 8015);
		connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newConnection()));
		serverList << tcpServer;
	}
	for (int i = 0; i < proxies.count(); i++)
	{
		qDebug() << "add a Proxy in the list";
		qDebug() << "IP :" << ips.at(i);
		QDomElement streamHost = doc.createElement("streamhost");
		streamHost.setAttribute("jid", proxies[i]);
		streamHost.setAttribute("host", ips[i]);
		streamHost.setAttribute("port", ports[i]);
		query.appendChild(streamHost);
	}
	timeOut->setInterval(TIMEOUT);
	connect(timeOut, SIGNAL(timeout()), this, SLOT(noConnection()));
	timeOut->start();

	stanza.element().appendChild(query);
	p->write(stanza);
}

void FileTransferTask::noConnection()
{
	qDebug() << "Unable to connect to the target.";
	emit notConnected();
}

void FileTransferTask::newConnection()
{
	qDebug() << "New Connection Received.";
	
	timeOut->stop(); // Connection received, no need to wait anymore.
	delete timeOut; // Unused now.

	for (int i = 0; i < serverList.count(); i++)
	{
		if (serverList.at(i)->hasPendingConnections())
		{
			socks5Socket = serverList.at(i)->nextPendingConnection();
			// As this connection is a child of the server,
			// the QTcpServer cannot be destroied before we
			// are finished with the QTcpSocket.
			break;
		}
	}

	socks5 = new Socks5(s, p->node(), to);
	connect(socks5, SIGNAL(readyRead()), this, SLOT(readS5()));
	for (int i = 0; i < serverList.count(); i++)
	{
		serverList.at(i)->close();
	}
	connect(socks5Socket, SIGNAL(readyRead()), this, SLOT(dataAvailable()));
	connectToProxy = false;
}

void FileTransferTask::dataAvailable()
{
	QByteArray data = socks5Socket->readAll();
	if (!isRecept)
	{
		qDebug() << "[SOCKS5] Received :" << data.toHex();
		qDebug() << "Data (" << data.size() << "bytes)";
		socks5->write(data);
	}
	else
	{
		if (!fileOpened)
		{
			fileOut = new QFile(st);
			fileOut->open(QIODevice::WriteOnly | QIODevice::Append);
			fileOpened = true;
		}
		fileOut->write(data);
		writtenData += data.size();
		qDebug() << "[FileTransferTask] prc = " << (int)((float)writtenData/(float)filesize*100) << ", to = " << to.full();
		emit prcentChanged(to, filename, (int)((float)writtenData/(float)filesize*100));
	}
}

void FileTransferTask::readS5()
{
	QByteArray data = socks5->read();
	qDebug() << "[SOCKS5] Sent :" <<  data.toHex() << data.count();
	socks5Socket->write(data);
}

void FileTransferTask::notifyStart()
{
	id = TaskManager::randomString(8);
	Stanza stanza(Stanza::IQ, "set", id, usedProxy);
	QDomNode node = stanza.element();
	stanza.setFrom(p->node());
	QDomDocument doc("");

	QDomElement query = doc.createElement("query");
	query.setAttribute("sid", s);
	query.setAttribute("xmlns", XMLNS_BYTESTREAMS);

	QDomElement activate = doc.createElement("activate");

	QDomText toText = doc.createTextNode(to.full());

	activate.appendChild(toText);
	query.appendChild(activate);
	node.appendChild(query);
	
	connectToProxy = false;
	p->write(stanza);
}

void FileTransferTask::setFileInfo(const QString& fileName, int fileSize)
{
	filename = fileName;
	filesize = fileSize;
}

void FileTransferTask::connectToHosts(QList<PullStreamTask::StreamHost> hostList, const QString& sid, const QString& i, const QString& saveTo)
{
	h = hostList;
	st = saveTo;
	id = i;
	s = sid;
	//PullStreamTask::StreamHost tytutuyty = h.takeFirst();
	if (h.count() > 0)
		tryToConnect(h.takeFirst());
	else
		cancel();
}

void FileTransferTask::cancel()
{

}

void FileTransferTask::tryToConnect(PullStreamTask::StreamHost hostData)
{
	socks5Socket = new QTcpSocket();
	QString host = hostData.host;
	usedJid = hostData.jid;
	qDebug() << "Connecting to" << host.toLatin1().constData();
	int port = hostData.port;
	/*
	 * On the target side, we don't care if streamhost is a proxy or the initiator.
	 */
	if (port != 0)
	{
		connect(socks5Socket, SIGNAL(connected()), this, SLOT(s5Connected()));
		connect(socks5Socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(s5Error(QAbstractSocket::SocketError)));
		socks5Socket->connectToHost(host, port);
		//qDebug() << "[FILETRANSFERTASK] host = %s, port = %d, jid =" << host.toLatin1().constData(), port, usedJid.full().toLatin1().constData());
	}
	else
	{
		//qDebug() << "[FILETRANSFERTASK] Use Zeroconf policy, not supported at all. Please restart Kapture.";
		// FIXME: Should emit an error here.
		// TODO: zeroconf connection.
	}
}

void FileTransferTask::s5Connected()
{
	fileOpened = false;
	/* Start connection to hostData.host with SOCKS5 */
	socks5 = new Socks5(s, to, p->node());
	isRecept = false;
	connect(socks5, SIGNAL(readyRead()), this, SLOT(readS5()));
	connect(socks5, SIGNAL(established()), this, SLOT(receptionNotify()));
	connect(socks5Socket, SIGNAL(readyRead()), this, SLOT(dataAvailable()));
	disconnect(socks5Socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(s5Error()));
	socks5->connect();
}

void FileTransferTask::s5Error(QAbstractSocket::SocketError e)
{
	if (e == QAbstractSocket::RemoteHostClosedError)
	{
		//FIXME:Should check if the whole file has been transfered.
		//qDebug() << "Connection Closed.";
		socks5Socket->disconnect();
		socks5Socket->disconnectFromHost();
		fileOut->close();
		emit finished();
	}
	else
	{
		//qDebug() << "Unable to transfer the file, ");
		delete socks5Socket;
		if (h.count() > 0)
		{
			//qDebug() << "trying next streamhost.";
			tryToConnect(h.takeFirst());
		}
		else
		{
			//qDebug() << "Cancelling [Not Implmented yet]";
			//TODO: send iq to tell Initiator that the connection couldn't be established.
		}
	}
}

void FileTransferTask::receptionNotify()
{
	isRecept = true;
	Stanza stanza(Stanza::IQ, "result", id, to.full());
	QDomNode node = stanza.element();
	stanza.setFrom(p->node());
	QDomDocument doc("");

	QDomElement query = doc.createElement("query");
	query.setAttribute("xmlns", XMLNS_BYTESTREAMS);

	QDomElement streamhostused = doc.createElement("streamhost-used");
	streamhostused.setAttribute("jid", usedJid.full());

	node.appendChild(query);
	query.appendChild(streamhostused);

	p->write(stanza);
}

//-------------------------------
// PullStreamTask
//-------------------------------

PullStreamTask::PullStreamTask(TaskManager* parent, Xmpp *xmpp)
	:Task(parent)
{
	p = xmpp;
	id = "";
}

bool PullStreamTask::canProcess(const Stanza& s) const
{
	qDebug() << "[PullStreamTask]";
	if (s.kind() != Stanza::IQ)
		return false;

	if (JingleStanza(s).isValid())
	{
		qDebug() << "[PullStreamTask] Jingle stanza, not processing.";
		return false;
	}

	if (s.type() == "get" &&
	    s.element().namespaceURI() == XMLNS_DISCO_INFO) 	//FIXME: Stanza has not alway a child element !!!
	    	return true;							//FIXED: Would return "" is no child exists
	
	if (s.type() == "get" &&
	    s.element().namespaceURI() == XMLNS_IQ_VERSION)
		return true;
	
	if (s.type() == "set" &&
	    s.element().namespaceURI() == XMLNS_SI)
		return true;

	if (s.type() == "set" && /* FIXME:Should check SID here */
	    s.element().namespaceURI() == XMLNS_BYTESTREAMS)
	    	return true;

	return false;
}

void PullStreamTask::processStanza(const Stanza& s)
{
	qDebug() << "[PullStreamTask] PROCESSING";
	id = s.id();
	if (s.element().namespaceURI() == XMLNS_IQ_VERSION)
	{
		Stanza stanza(Stanza::IQ, "result", id, s.from().full());
		QDomNode node = stanza.element();
		QDomDocument doc("");

		QDomElement query = doc.createElement("query");
		query.setAttribute("xmlns", XMLNS_IQ_VERSION);

		QDomElement name = doc.createElement("name");
		QDomText nameText = doc.createTextNode("Kapture");
		name.appendChild(nameText);

		QDomElement version = doc.createElement("version");
		QDomText versionText = doc.createTextNode(KAPTURE_VERSION);
		version.appendChild(versionText);
		
		QDomElement os = doc.createElement("os");
		QDomText osText = doc.createTextNode(osVersionString());
		os.appendChild(osText);

		query.appendChild(name);
		query.appendChild(version);
		query.appendChild(os);

		node.appendChild(query);

		p->write(stanza);
		return; // To be sure we do not enter another "if".
	}
	if (s.element().namespaceURI() == XMLNS_DISCO_INFO)
	{
		Stanza stanza(Stanza::IQ, "result", id, s.from().full());
		QDomNode node = stanza.element();
		QDomDocument doc("");

		QDomElement query = doc.createElement("query");
		query.setAttribute("xmlns", XMLNS_DISCO_INFO);

		QDomElement identity = doc.createElement("identity");
		identity.setAttribute("category", "client");
		identity.setAttribute("type", "pc");

		// Supported protocols
		// TODO:should add [Jingle], [Jingle video], [Raw-UDP],...
		QDomElement feature = doc.createElement("feature");
		feature.setAttribute("var", XMLNS_SI);
		
		QDomElement feature1 = doc.createElement("feature");
		feature1.setAttribute("var", XMLNS_FILETRANSFER);
		
		QDomElement feature2 = doc.createElement("feature");
		feature2.setAttribute("var", XMLNS_BYTESTREAMS);

		QDomElement feature3 = doc.createElement("feature");
		feature3.setAttribute("var", XMLNS_JINGLE);

		QDomElement feature4 = doc.createElement("feature");
		feature4.setAttribute("var", XMLNS_JINGLE_VIDEO);

		QDomElement feature5 = doc.createElement("feature");
		feature4.setAttribute("var", XMLNS_JINGLE_AUDIO);

		query.appendChild(identity);
		query.appendChild(feature);
		query.appendChild(feature1);
		query.appendChild(feature2);
		query.appendChild(feature3);
		query.appendChild(feature4);
		query.appendChild(feature5);
		node.appendChild(query);

		p->write(stanza);
	}

	if (s.element().namespaceURI() == XMLNS_SI && s.type() == "set")
	{
		f = s.from();
		QDomElement node = s.element();
		SID = node.attribute("id");
		
		node = node.firstChildElement();
		while (!node.isNull())
		{
			if (node.localName() == "file")
			{
				name = node.attribute("name");
				size = node.attribute("size").toInt();
				/*
				 * Hash is the MD5 sum of the file so, "NOHASH" cannot be it's value.
				 */
				hash = node.attribute("hash", "NOHASH");
				/*
				 * Date is the last modification time of the file specified using the DateTime profile so it can't be "NODATE"
				 */
				date = node.attribute("date", "NODATE");
				
				QDomNode n = node.firstChild();
				while (!n.isNull())
				{
					if (n.localName() == "desc")
					{
						desc = n.firstChild().toText().data();
					}
					n = n.nextSibling();
				}
			}
			if (node.localName() == "feature")
			{
				QDomNode n = node;
				if (n.firstChild().localName() != "x")
				{
					//emit error();
					qDebug() << "Error, No X Element, BAD STANZA !";
					return;
				}
				n = n.firstChild();

				if (n.firstChild().localName() != "field")
				{
					//emit error();
					qDebug() << "Error, No FIELD Element, BAD STANZA !";
					return;
				}
				n = n.firstChild();
				
				if (n.toElement().attribute("var") != "stream-method" || n.toElement().attribute("type") != "list-single")
				{
					//emit error();
					qDebug() << "Error, No FIELD Element, BAD STANZA !";
					return;
				}
				
				if (n.firstChild().localName() != "option")
				{
					//emit error();
					qDebug() << "Error, No OPTION Element, BAD STANZA !";
					return;
				}
				n = n.firstChild();
				

				while (!n.isNull())
				{
					pr.append(n.firstChild().firstChild().toText().data());
					n = n.nextSibling();
				}
			}
			node = node.nextSiblingElement();
		}
		emit fileTransferIncoming();
	}
	
	if (s.element().namespaceURI() == XMLNS_BYTESTREAMS && s.type() == "set")
	{
		QDomNode node = s.element();
		if (node.toElement().attribute("sid") != SID || node.toElement().attribute("mode") != "tcp")
		{
			//emit error();
			return;
		}
		node = node.firstChild();
		while (!node.isNull())
		{
			if (node.localName() == "streamhost")
			{
				StreamHost streamhost;
				streamhost.jid = Jid(node.toElement().attribute("jid"));
				streamhost.host = node.toElement().attribute("host");
				streamhost.port = node.toElement().attribute("port", "0").toInt();
				streamHostList << streamhost;
			}
			node = node.nextSibling();
		}

		emit receiveFileReady();
	}
}

void PullStreamTask::ftDecline(const QString&, const Jid&)
{
	Stanza stanza(Stanza::IQ, "error", id, f.full());
	QDomNode node = stanza.element();
	QDomDocument doc("");

	QDomElement error = doc.createElement("error");
	error.setAttribute("code", 403);

	QDomText text = doc.createTextNode("Declined");

	error.appendChild(text);
	node.appendChild(error);

	p->write(stanza);

}

void PullStreamTask::ftAgree(const QString&, const Jid&, const QString& saveFileName)
{
	sfn = saveFileName;
	bool ok = false;
	for (int i = 0; i < pr.count(); i++)
	{
		qDebug() << "Protocol :" << pr[i];
		if (pr[i] == XMLNS_BYTESTREAMS)
			ok = true;
	}

	if (!ok)
	{
		Stanza stanza(Stanza::IQ, "error", id, f.full());
		QDomNode node = stanza.element();
		QDomDocument doc("");
	
		QDomElement error = doc.createElement("error");
		error.setAttribute("code", 400);
		error.setAttribute("type", "cancel");

		QDomElement badrequest = doc.createElement("bad-request");
		badrequest.setAttribute("xmlns", "urn:ietf:params:xml:ns:xmpp-stanzas");

		QDomElement novalidstreams = doc.createElement("no-valid-streams");
		novalidstreams.setAttribute("xmlns", XMLNS_SI);

		error.appendChild(novalidstreams);
		error.appendChild(badrequest);
		node.appendChild(error);
	
		p->write(stanza);
		
		return;
	}
	
	Stanza stanza(Stanza::IQ, "result", id, f.full());
	QDomNode node = stanza.element();
	QDomDocument doc("");

	QDomElement si = doc.createElement("si");
	si.setAttribute("xmlns", XMLNS_SI);

	QDomElement feature = doc.createElement("feature");
	feature.setAttribute("xmlns", XMLNS_FEATURENEG);

	QDomElement x = doc.createElement("x");
	x.setAttribute("xmlns", "jabber:x:data");
	x.setAttribute("type", "submit");

	QDomElement field = doc.createElement("field");
	field.setAttribute("var", "stream-method");

	QDomElement value = doc.createElement("value");

	QDomText method = doc.createTextNode(XMLNS_BYTESTREAMS);
	value.appendChild(method);

	node.appendChild(si);
	si.appendChild(feature);
	feature.appendChild(x);
	x.appendChild(field);
	field.appendChild(value);
	
	p->write(stanza);
}

//-------------------------------
// JingleTask
//-------------------------------

JingleTask::JingleTask(TaskManager* parent, Xmpp *xmpp)
	: Task(parent)
{
	p = xmpp;
}

JingleTask::~JingleTask()
{

}

bool JingleTask::canProcess(const Stanza& s) const
{
	return s.id() == id;
}

void JingleTask::processStanza(const Stanza& s)
{
	//TODO:Check for errors.
	// If no error -> emit finished();
	emit finished();
}

void JingleTask::sendStanza(const JingleStanza& s)
{
	id = s.id();
	p->write(static_cast<Stanza>(s));
}

#include "jingle/jinglesession.h"

/*
 * PullJingleTask
 */
PullJingleTask::PullJingleTask(TaskManager* parent, Client* client)
	:Task(parent), m_client(client)
{

}

PullJingleTask::~PullJingleTask()
{

}

bool PullJingleTask::canProcess(const Stanza& s) const
{
	qDebug() << "[PullJingleTask]";
	JingleStanza js(s);

	return js.isValid();
}

JingleSession *PullJingleTask::getSessionId(const QString & id)
{
	foreach (JingleSession *sess, m_sessions)
		if (sess->id() == id)
			return sess;

	return 0;
}

void PullJingleTask::unknownSession(const JingleStanza& s)
{
	//TODO:implement-me
}

void PullJingleTask::processStanza(const Stanza& s)
{
	JingleStanza js(s);
	if (!js.isValid())
	{
		qFatal("Stanza is not a valid Jingle stanza");
	}

	Stanza stanza(Stanza::IQ, "result", s.id(), s.from().full());
	m_client->xmpp()->write(stanza);

	JingleSession *sess = getSessionId(js.sid());

	if (!sess && js.action() == JingleStanza::SessionInitiate)
	{
		sess = new JingleSession(m_client);
		m_sessions << sess;
		connect(sess, SIGNAL(sessionTerminated(JingleSession::Reason, JingleSession*)), SLOT(sessionTerminated(JingleSession::Reason, JingleSession*)));
	}
	else if (!sess)
	{
		unknownSession(js);
		return;
	}

	sess->write(js);

	if (js.action() == JingleStanza::SessionInitiate)
		emit sessionReady(sess);
}

void PullJingleTask::addSession(JingleSession* sess)
{
	connect(sess, SIGNAL(sessionTerminated(JingleSession::Reason, JingleSession*)), SLOT(jingleSessionTerminated(JingleSession::Reason, JingleSession*)));
	m_sessions << sess;
}

void PullJingleTask::jingleSessionTerminated(JingleSession::Reason reason, JingleSession* session)
{
	Q_UNUSED(reason)
	session->deleteLater();
}
