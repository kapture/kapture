#ifndef TASKS_H
#define TASKS_H

#include <QFile>
#include <QTimer>

#include "task.h"
#include "taskmanager.h"
#include "xmpp.h"
#include "jid.h"
#include "roster.h"
#include "presence.h"
#include "message.h"
#include "jingle/jinglestanza.h"
#include "jingle/jinglesession.h"
#include "socks5.h"

#define KAPTURE_VERSION			"svn"
#define XMLNS_FEATURENEG 		"http://jabber.org/protocol/feature-neg"
#define XMLNS_SI 			"http://jabber.org/protocol/si"
#define XMLNS_FILETRANSFER 		"http://jabber.org/protocol/si/profile/file-transfer"
#define XMLNS_DISCO_INFO 		"http://jabber.org/protocol/disco#info"
#define XMLNS_DISCO_ITEMS 		"http://jabber.org/protocol/disco#items"
#define XMLNS_SI "http://jabber.org/protocol/si"
#define XMLNS_FILETRANSFER "http://jabber.org/protocol/si/profile/file-transfer"
#define XMLNS_BYTESTREAMS 		"http://jabber.org/protocol/bytestreams"
#define XMLNS_IQ_VERSION		"jabber:iq:version"
/* FIXME: Warning:
 * Jingle Namespaces are temporary namespaces because the specification
 * is still in a "Draft" state. As soon as the specification is accepted
 * by the Xmpp foundation, the namespaces will change and become of the
 * form of "http://jabber.org/protocol/jingle[...]", [...] depending of
 * the jingle namespace. Or not...
 */
#define XMLNS_JINGLE 			"urn:xmpp:tmp:jingle"
#define XMLNS_VIDEO 			"urn:xmpp:tmp:jingle:apps:video-rtp"
#define XMLNS_AUDIO 			"urn:xmpp:tmp:jingle:apps:audio-rtp"
#define XMLNS_JINGLE_VIDEO		XMLNS_VIDEO
#define XMLNS_JINGLE_AUDIO		XMLNS_AUDIO
#define XMLNS_RAW_UDP 			"urn:xmpp:tmp:jingle:transports:raw-udp"

class RosterTask : public Task
{
public:
	RosterTask(Xmpp* p, TaskManager* parent = 0, Client *client = 0);
	~RosterTask();
	void getRoster(const Jid& j);
	bool canProcess(const Stanza& s) const;
	void processStanza(const Stanza& s);
	Roster roster() const;
	void addItem(const Jid& jid, const QString& name);
	void delItem(const Jid& jid);
private:
	Roster m_roster;
	QString id;
	Xmpp *p;
	bool addContact;
};



class PresenceTask : public Task
{
	Q_OBJECT
public:
	PresenceTask(Xmpp *xmpp, TaskManager* parent = 0);
	~PresenceTask();
	void setPresence(const QString& show, const QString& status, const QString& type); //FIXME:There should be a Status class.
	void setSubscription(const Jid& to, const QString&  type);
	bool canProcess(const Stanza& s) const;
	void processStanza(const Stanza& s);

signals:
	void subApproved();
	void subRefused();

private:
	bool waitSub;
	Xmpp *p;
};



class PullPresenceTask : public Task
{
	Q_OBJECT
public:
	PullPresenceTask(TaskManager* parent);
	~PullPresenceTask();
	bool canProcess(const Stanza& s) const;
	void processStanza(const Stanza& s);
	Presence getPresence();

signals:
	void presenceFinished();

private:
	Jid from;
	QString type;
	QString show;
	QString status;
};

class PullMessageTask : public Task
{
	Q_OBJECT
public:
	PullMessageTask(TaskManager* parent);
	~PullMessageTask();
	bool canProcess(const Stanza&) const;
	void processStanza(const Stanza&);
	Message getMessage();

signals:
	void messageFinished();
	
private:
	QString m;   	// Message
	Jid f;       	// From (Sender)
	Jid t;		// To (Receiver)
	QString ty;   	// Type (chat, ...)
	QString sub; 	// Subject (Not used yet)
	QString thr;  	// Thread (Not used yet)
};

class MessageTask : public Task
{
public:
	MessageTask(TaskManager* parent);
	~MessageTask();
	bool canProcess(const Stanza&) const {return false;}
	void processStanza(const Stanza&) {}
	void sendMessage(Xmpp* p, const Message& message);
};

class StreamTask : public Task
{
	Q_OBJECT
public:
	StreamTask(TaskManager* parent, Xmpp *xmpp, const Jid& t);
	~StreamTask();
	void initStream(const QFile&);
	void discoInfo();
	bool canProcess(const Stanza&) const;
	void processStanza(const Stanza&);
	bool supports(const QString& profile);
	QString negProfile() const;
	Jid toJid() const;
	QString sid() const;
	QStringList proxies() const;
	QStringList ports() const;
	QStringList ips() const;

signals:
	void infoDone();
	void error(int, const QString&);

private:
	/*
	 * Stream Initiation
	 *  - Discovers if Receiver implements the desired profile.
	 *  - Offers a stream initiation.
	 *  - Receiver accepts stream initiation.
	 *  - Sender and receiver prepare for using negotiated profile and stream.
	 *  See XEP 0095 : http://www.xmpp.org/extensions/xep-0095.html
	 */
	QFile f;
	enum States {
		WaitDiscoInfo = 0,
		WaitAcceptFileTransfer,
		WaitProxies,
		WaitIsProxy,
		WaitProxyIp
	} state;
	QString id;
	QStringList featureList;
	QStringList itemList;
	QStringList proxyList;
	QStringList proxyList2;
	QStringList ipList;
	QStringList portList;
	QStringList zeroconfList;
	Xmpp *p;
	Jid to;
	QString profileToUse;
	QString SID; //id
	void getProxies();
	void isAProxy(QString);
	void getProxyIp(QString);

};

class PullStreamTask : public Task
{
	Q_OBJECT
public:
	PullStreamTask(TaskManager* parent, Xmpp *xmpp);
	bool canProcess(const Stanza&) const;
	void processStanza(const Stanza&);
	void ftDecline(const QString&, const Jid&);
	void ftAgree(const QString&, const Jid&, const QString&);
	Jid from() const {return f;}
	QString fileName() const {return name;}
	int fileSize() const {return size;}
	QString fileDesc() const {return desc;}
	QList<StreamHost> streamHosts() const {return streamHostList;}
	QString sid() const {return SID;}
	QString lastId() const {return id;}
	QString saveFileName() const {return sfn;}

signals:
	void fileTransferIncoming();
	void receiveFileReady();
private:
	QString id;
	Xmpp *p;

	//Concerns File Transfer
	Jid f; //from
	QString SID;
	QStringList pr; //protocols
	QString d; // Desctription
	QString name;
	int size;
	QString hash;
	QString desc;
	QString date;
	QList<StreamHost> streamHostList;
	QString sfn; // Save File Name.
};

class FileTransferTask : public Task
{
	Q_OBJECT
public:
	FileTransferTask(TaskManager* parent, const Jid& t, Xmpp *xmpp);
	~FileTransferTask();
	void start(const QString&, const QString&, const QString&, const QStringList, const QStringList, const QStringList);
	bool canProcess(const Stanza&) const;
	void processStanza(const Stanza&);
	void connectToHosts(QList<PullStreamTask::StreamHost>, const QString& sid, const QString& id, const QString& saveTo);
	void setFileInfo(const QString& fileName, int fileSize);

public slots:
	void noConnection();
	void newConnection();
	void dataAvailable();
	void readS5();
	void bytesWrittenSlot(qint64);
	void connectedToProxy();
	void notifyStart();
	void s5Connected();
	void receptionNotify();
	void s5Error(QAbstractSocket::SocketError);
signals:
	void prcentChanged(Jid&, QString&, int);
	void notConnected();

private:
	QList<PullStreamTask::StreamHost> h;
	QList<QTcpServer*> serverList;
	QStringList proxies;
	QStringList ips;
	QStringList ports;
	QTcpSocket *socks5Socket;
	QString usedProxy;
	QString usedIP;
	QString usedPort;
	QString st; //Save to
	QString fileName; // Ouch !
	QString filename; // Ouch !
	QString s; //SID
	QString id;
	Jid usedJid;
	QTimer *timeOut;
	qint64 writtenData;
	Socks5 *socks5;
	QFile *f;
	QFile *fileOut;
	Xmpp *p;
	Jid to;
	bool connectToProxy;
	bool isRecept; // Tells if we are receiving (true) or sending (false) a file.
	bool fileOpened;
	void startByteStream();
	void cancel();
	void tryToConnect(PullStreamTask::StreamHost host);
	int prc, prc2, filesize;
};

/*-----------------------------------------------
 * JingleTask
 * ---------------------------------------------
 */

class JingleTask : public Task
{
	Q_OBJECT
public:
	JingleTask(TaskManager* parent, Xmpp* xmpp);
	~JingleTask();

	bool canProcess(const Stanza&) const;
	void processStanza(const Stanza&);

	void sendStanza(const JingleStanza&);

private:
	Xmpp *p;
	QString id;
};

class JingleSession;

class PullJingleTask : public Task
{
	Q_OBJECT
public:
	PullJingleTask(TaskManager* parent, Client *client);
	~PullJingleTask();
	bool canProcess(const Stanza&) const;
	void processStanza(const Stanza&);
	void addSession(JingleSession*);

signals:
	void sessionReady(JingleSession*);

private slots:
	void jingleSessionTerminated(JingleSession::Reason, JingleSession*);

private:
	Client *m_client;
	QList<JingleSession*> m_sessions;

	JingleSession* getSessionId(const QString&);
	void unknownSession(const JingleStanza& s);
};

#endif
