#include <QApplication>

#include "stunclient.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	StunClient stun(argv[1], argv[2], &app);

	stun.getResponse();
	app.exec();

	return 0;
}
