#ifndef XMLIODEVICE_H
#define XMLIODEVICE_H

#include <QDomDocument>
#include <QObject>

class XmlIODevice : public QObject
{
	Q_OBJECT
public:
	XmlIODevice(QObject *parent = 0);
	virtual ~XmlIODevice() {}

	virtual void write(const QDomElement&) = 0;
	virtual QDomElement read() = 0;

signals:
	void readyRead();
};

#endif // XMLIODEVICE_H
