/*
 *      Kapture -- xmpp.cpp
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include <QtXml>
#include <QDomDocument>
#include <QSslError>

#include "xmpp.h"
#include "authmanager.h"

/*
 * Establish connection to the server.
 */
Xmpp::Xmpp(const Jid &jid, const QString &pServer, const int pPort)
	: QObject(), m_timeOut(5000), m_errorWas(UnknownError)
{
	if (pPort != 5223)
		m_socket = new QTcpSocket(this);
	else
	{
		m_socket = new QSslSocket(this);
		static_cast<QSslSocket*>(m_socket)->setProtocol(QSsl::SslV3);
		connect(static_cast<QSslSocket*>(m_socket), SIGNAL(encrypted()), SLOT(socketEncrypted()));
	}

	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(connectionError(QAbstractSocket::SocketError)));
	connect(m_socket, SIGNAL(connected()), this, SLOT(start()));

	username = jid.node();
	server = jid.domain();

	usePersonnalServer = !pServer.isEmpty();

	if (usePersonnalServer)
		personnalServer = pServer;

	port = pPort;
	authenticated = false;
	isTlsing = false;
	tlsDone = false;
	saslDone = false;
	needBind = false;
	needSession = false;
	jidDone = false;
	useTls = true;
	m_jid = jid;

	xmlReader = 0;
	xmlSource = 0;
	xmlHandler = 0;

	state = waitStream;

	printf("[XMPP] JID = %s\n", jid.full().toLatin1().constData());
}

Xmpp::Xmpp()
{
	authenticated = false;
	isTlsing = false;
	tlsDone = false;
	saslDone = false;
	needBind = false;
	needSession = false;
	jidDone = false;
	useTls = true;
	state = waitStream;
}

/*
 * Closes the connection to the server.
 */
Xmpp::~Xmpp()
{
	//FIXME: Do it with a stanza class.
	/*if (state == active)
	{
		QDomDocument d("");
		QDomElement e = d.createElement("presence");
		e.setAttribute("type", "unavailable");
		QDomElement s = d.createElement("satuts");
		QDomText t = d.createTextNode("Logged out");

		s.appendChild(t);
		e.appendChild(s);
		d.appendChild(e);
		
		QByteArray data = d.toString().toLatin1();

		sendData(data);
	}*/

	delete xmlHandler;
	delete xmlSource;
	delete xmlReader;
}

void Xmpp::socketEncrypted()
{
	printf("[XMPP] Encryption ok.\n");
}

void Xmpp::sslError(QList<QSslError>& errors)
{
	printf("[XMPP] SSL errors : ");

	for (int i = 0; i < errors.count(); i++)
	{
		printf("[XMPP]    * Some Errors !!!\n");
		printf("[XMPP]    * %s\n", errors.at(i).errorString().toLatin1().constData());
	}

	if (errors.count() == 0)
	{
		printf("None.\n");
	}
}

/*
 * Start the authentication process to the server.
 */
void Xmpp::auth(const QString &pass, const QString &res)
{
	qDebug() << "[XMPP] Port = " << port << ", Server =" << server << ", Personnal Server (" << usePersonnalServer << ") =" << personnalServer;
	if (port != 5223)
		m_socket->connectToHost(usePersonnalServer ? personnalServer : server, port);
	else
		static_cast<QSslSocket*>(m_socket)->connectToHostEncrypted(usePersonnalServer ? personnalServer : server, port);

	password = pass;
	resource = res; //TODO:Should be removed.
	m_jid.setResource(res);
	connect(m_socket, SIGNAL(readyRead()), this, SLOT(dataReceived()));
}

void Xmpp::start()
{
//	if (tlsDone || !useTls)
	{
		delete xmlHandler;
		xmlHandler = 0;
		delete xmlSource;
		xmlSource = 0;
		delete xmlReader;
		xmlReader = 0;
	}

	xmlReader = new QXmlSimpleReader();
	xmlSource = new QXmlInputSource();
	xmlHandler = new XmlHandler();
	xmlReader->setContentHandler(xmlHandler);
	xmlReader->setFeature("http://trolltech.com/xml/features/report-whitespace-only-CharData", false);
	xmlSource->setData(QString(""));
	xmlReader->parse(xmlSource, true);
	
	QString firstXml = QString("<stream:stream xmlns:stream='http://etherx.jabber.org/streams' xmlns='jabber:client' to='%1' version=\"1.0\">").arg(server);
	if (state != PrepareRegistering)
		state = waitStream;
	QByteArray data = firstXml.toLatin1();
	sendData(data);
}

/*
 * Send QString to the server.
 */
int Xmpp::sendData(const QByteArray &mess)
{
	qDebug() << "SENT :" << mess.constData();

	if (tlsDone)
	{
		//printf("[XMPP] Giving clear data to tlsHandler\n");
		tls->write(mess);
	}
	else
	{
		//printf("[XMPP] Sending : %s\n", mess.constData());
		m_socket->write(mess);
	}

	return 0;
}

/*
 * Read data if it's ready to be read and return it.
 * Return an empty QByteArray if an error occur.
 */
QByteArray Xmpp::readData()
{
	/*
	 * If the readyRead signal has been emitted, it has not to wait
	 * for ready data to read !
	 * --> dataReceived will read data itself.
	 *
	 *  PS: as dataReceived is executed each time data is ready to be
	 *  read, the readData function becomes obsolete.
	 *
	 */
	if (m_socket->waitForReadyRead(m_timeOut))
		return m_socket->readAll();
	else
		return QByteArray();
}

bool Xmpp::connectedToServer()
{
	return isConnectedToServer;
}

void Xmpp::dataReceived()
{
	//printf("\n[XMPP] Data avaible !\n");
	QByteArray data;
	QString mess;
	
	data = m_socket->readAll();
	if (data == "" || data.isNull())
		return;
	qDebug() << data;
	if (state == isHandShaking || tlsDone)
	{
		/*printf(" * RECEIVED (%d bytes):\n", data.count());
		for (int i = 0; i < data.count(); i++)
			printf("0x%02hhx ", data.data()[i]);
		printf("\n");*/
		tls->writeIncoming(data);
	}
	else
		processXml(data);
}

void Xmpp::processXml(QByteArray &data)
{
	//qDebug() << "[XMPP] Data :" << data.constData();

	xmlSource->setData(data);

	if (!xmlReader->parseContinue())
	{
		printf("[XMPP] Parsing error\n");
		return;
	}

	Event *event;
	while (xmlHandler && (event = xmlHandler->read()) != 0)
		if (!processEvent(event))
			break;
}

bool Xmpp::processEvent(Event *event)
{
	//qDebug() << "Elem = %s\n", event->node().localName().toLatin1().constData());
	if (event->type() == Event::StreamError)
	{
		disconnect(m_socket);
		delete event;
		emit error(StreamError);
		return false;
	}

	QDomNode node;
	QDomNode mechsNode;

	switch (state)
	{
		case isHandShaking:
			break;
		case PrepareRegistering:
		case waitStream:
			if (event->type() == Event::Stream)
			{
				qDebug() << "[XMPP] Ok, received the stream tag.";
				if (state != PrepareRegistering)
					state = waitFeatures;
				else
				{
					state = active;
					emit registerReady();
				}
			}
			//else
			//	qDebug() << " ! Didn't receive the stream ! ";
			break;
		case waitFeatures:
			if (event->node().localName() != "features")
				break;

			qDebug() << "[XMPP] Ok, received the features tag.";

			// Check if there is a starttls node.
			node = event->node().firstChild();

			while(!node.isNull())
			{
				if (node.localName() == QString("starttls"))
				{
					// Even if TLS isn't required, I use TLS.
					qDebug() << "[XMPP] Ok, received the starttls tag.";
					// Send starttls tag
					QDomDocument doc("");
					QDomElement e = doc.createElement("starttls");
					doc.appendChild(e);
					e.setAttribute(QString("xmlns"), QString("urn:ietf:params:xml:ns:xmpp-tls"));
					QByteArray sData = doc.toString().toLatin1();
					sendData(sData);
					// Next state
					state = waitProceed;
					break;
				}

				node = node.nextSibling();
			}

			if (state != waitFeatures)
				break;

			node = event->node().firstChild();
			while(!node.isNull())
			{
				if (node.localName() == QString("mechanisms"))
				{
					mechsNode = node;
					break;
				}

				node = node.nextSibling();
			}

			if (!saslDone && mechsNode.isNull())
				break;

			if (!saslDone)
			{
				QDomNode mechNode = mechsNode.firstChild();
				qDebug() << "[XMPP] Tls done or not used. --> sasl";
				while(mechNode.localName() == QString("mechanism"))
				{
					qDebug() << "[XMPP] Ok, received a mechanism tag.";
					QString authMechanism = mechNode.firstChild().toText().data();
					if (AuthManager::isSupported(authMechanism))
					{
						m_authMechanism = AuthManager::getAuthMechanism(authMechanism, username, password);
						QDomDocument doc = m_authMechanism->read();
						sendData(doc.toString().toLatin1());
						state = waitAuth;
						break;
					}

					mechNode = mechNode.nextSibling();
				}

				if (state == waitFeatures)
				{
					m_errorWas = UnsupportedMechanisms;
					qDebug() << "[XMPP]  ! No supported mechanism found.";
					QByteArray sData = "</stream:stream>";
					sendData(sData);
				}

				break;
			}

			//		qDebug() << "Wait Ncessary";
			//		state = waitNecessary;
			node = event->node().firstChild();
			while(!node.isNull())
			{
				if (node.localName() == QString("bind"))
				{
					qDebug() << "[XMPP] Ok, bind needed.";
					needBind = true;
				}
				if (node.localName() == QString("session"))
				{
					qDebug() << "[XMPP] Ok, session needed.";
					needSession = true;
				}
				node = node.nextSibling();
			}

			if (needBind)
			{
				QDomDocument doc("");
				QDomElement e = doc.createElement("iq");
				e.setAttribute("type", "set"); // Trying without id.

				QDomElement e2 = doc.createElement("bind");
				e2.setAttribute("xmlns", "urn:ietf:params:xml:ns:xmpp-bind");

				QDomElement e3 = doc.createElement("resource");
				QDomText t = doc.createTextNode(resource);

				e3.appendChild(t);
				e2.appendChild(e3);
				e.appendChild(e2);
				doc.appendChild(e);
				QByteArray sData = doc.toString().toLatin1();
				sendData(sData);

				state = waitBind;
			}
			break;
		case waitProceed:
			if (event->node().localName() == QString("proceed"))
			{
				//qDebug() << " * Ok, received the proceed tag.";
				qDebug() << "[XMPP] Proceeding...";
				qDebug() << "[XMPP] Enabling TLS connection.";

				state = isHandShaking;
				tls = new TlsHandler(this);
				connect(tls, SIGNAL(readyRead()), this, SLOT(clearDataReceived()));
				connect(tls, SIGNAL(readyReadOutgoing()), this, SLOT(sendDataFromTls()));
				connect(tls, SIGNAL(connected()), this, SLOT(tlsIsConnected()));
				tls->connect();
				state = isHandShaking;
				isTlsing = true;
			}
			break;
		case waitAuth:
			if (!m_authMechanism)
				break;
			m_authMechanism->write(event->node().toElement());
			if (m_authMechanism->success())
			{
				qDebug() << "[XMPP] Ok, SASL established.";
				saslDone = true;
				delete m_authMechanism;
				m_authMechanism = 0;
				start();
			}
			else if (m_authMechanism->failure())
			{
				qDebug() << "[XMPP]  ! Check Username and password.";
				delete m_authMechanism;
				m_authMechanism = 0;
				QByteArray sData = "</stream:stream>";
				sendData(sData);
				m_errorWas = BadUsernameOrPassword;
			}
			else
				sendData(m_authMechanism->read().toString().toLatin1());

			break;
		case waitBind:
			if (event->node().localName() == QString("iq"))
			{
				if (event->node().toElement().attribute("type") != QString("result"))
				{
					qDebug() << "[XMPP] Authentification Error.";
					QByteArray sData = "</stream:stream>";
					sendData(sData);
					delete event;
					emit error(UnknownError);
					return false;
				}
				if (event->node().firstChild().localName() == QString("bind"))
				{
					QDomNode bindNode = event->node().firstChild();
					if (bindNode.firstChild().localName() == QString("jid"))
					{
						bindNode = bindNode.firstChild().firstChild();
						QString u, r, s;
						if (!bindNode.toText().data().isEmpty())
						{
							u = bindNode.toText().data().split('@')[0]; // Username
							s = bindNode.toText().data().split('@')[1].split('/')[0]; // Server
							r = bindNode.toText().data().split('/')[1]; // Resource
							qDebug() << "[XMPP] '" << u << "'@'" << s << "'/'" << r << "'";
						}
						if (u == username && s == server)
						{
							qDebug() << "[XMPP] Jid OK !";
							resource = r;
							jidDone = true;
							m_jid.setResource(r);
						}
					}

					if (needSession && jidDone)
					{
						qDebug() << "[XMPP] Launching Session...";
						QDomDocument doc("");
						QDomElement e = doc.createElement("iq");
						e.setAttribute("to", server);
						e.setAttribute("type", "set");
						e.setAttribute("id", "sess_1");

						QDomElement e2 = doc.createElement("session");
						e2.setAttribute("xmlns", "urn:ietf:params:xml:ns:xmpp-session");

						e.appendChild(e2);
						doc.appendChild(e);

						QByteArray sData = doc.toString().toLatin1();
						sendData(sData);

						state = waitSession;
					}

				}
			}
			break;
		case waitSession:
			if (event->node().localName() == QString("iq"))
			{
				if (event->node().toElement().attribute("type") == "result")
				{
					qDebug() << "[XMPP] Connection is now active !";

					/*
					 * Presence must be sent after getting the roster
					 * so we already have the contacts to assign their presence
					 * when receiving presence stanza's wich come after
					 * setting the first presence.
					 */

					state = active;
					emit connected();

				}
				else
				{
					if (event->node().toElement().attribute("type") == "error")
					{
						qDebug() << "[XMPP] An error occured ! ";
					}
				}
			}
			break;
		case active:
		{
			Stanza *s = new Stanza(event->node().toElement());
			//qDebug() << "[XMPP] Xmpp::processEvent : node =" << doc.toString();
			stanzaList << s;
			emit readyRead();
			break;
		}

		default :
			break;
	}

	delete event;

	return true;
}


QString Xmpp::getResource() const
{
	return resource;
}

void Xmpp::sendDataFromTls(/*QByteArray data*/)
{
	m_socket->write(tls->readOutgoing());
}

void Xmpp::clearDataReceived()
{
	QByteArray toSend = tls->read();
	processXml(toSend);
}

void Xmpp::tlsIsConnected()
{
	tlsDone = true;
	isTlsing = false;
	start();
}

void Xmpp::connectionError(QAbstractSocket::SocketError socketError)
{
	/*
	 * If there is this error, xmppwin should destroy this object and construct a new one
	 * with the correct data.
	 */

	if (m_errorWas != UnknownError)
	{
		emit error(m_errorWas);
		m_errorWas = UnknownError;
		return;
	}

	qDebug() << "[XMPP] Error code :" << (int)socketError;
	switch (socketError)
	{
		case QAbstractSocket::HostNotFoundError:
			qDebug() << "[XMPP] ! Error = Host not found !\n";
			emit error(HostNotFound);
			break;
		case QAbstractSocket::NetworkError:
			qDebug() << "[XMPP] ! Network error ! Will reconnect in 30 seconds.";
			emit error(NetworkIsDown);
			/*must try to reconnect itself....*/
			break;
		default:
			qDebug() << "[XMPP] ! An Unknown error occured. Sorry.";
			emit error(UnknownError);
	}

	//QList<QSslError> azerty = sslSocket->sslErrors();
	//sslError(azerty);
}

/*
 * Calling this function before the client is connected has
 * no sense.
 */
bool Xmpp::isSecured() const
{
	return useTls;
}

Stanza *Xmpp::getFirstStanza()
{
	return stanzaList.takeFirst();
}

void Xmpp::write(const Stanza& s)
{
	QDomDocument doc("");
	doc.appendChild(s.element());
	QByteArray sData = doc.toByteArray();
	qDebug() << sData.size();
	//printf("[XMPP] Write : %s\n", sData.constData());
	sendData(sData);
}

bool Xmpp::stanzaAvailable() const
{
	return !stanzaList.isEmpty();
}

Jid Xmpp::node() const
{
	return m_jid;
}

void Xmpp::prepareToRegister(const QString& s)
{
	server = s;
	state = PrepareRegistering;
	m_socket = new QTcpSocket();
	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(connectionError(QAbstractSocket::SocketError)));
	/*
	 * Must be an entity first, send <?xml version="1.0"?>
	 * receive, <?xml version='1.0'?><stream:stream xmlns:stream='http://etherx.jabber.org/streams' xmlns='jabber:client' from='localhost' id='id'>
	 * send iq get, id can be different of the previous one.
	 */
	connect(m_socket, SIGNAL(connected()), this, SLOT(start()));
	connect(m_socket, SIGNAL(readyRead()), this, SLOT(dataReceived()));
	m_socket->connectToHost(server, 5222);
	printf("[XMPP] Connection to %s\n", server.toLatin1().constData());
}

QString Xmpp::errorToMessage(ErrorType e)
{
	switch (e)
	{
	case StreamError:
		return "Stream error.";
	default :
		return "Unknown error";
	}
}
