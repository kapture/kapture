/*
 *      Kapture
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */
#include <QLabel>
#include <QAbstractScrollArea>
#include <QScrollBar>
#include <QDate>
#include <QTime>
#include <QMessageBox>
#include <QDebug>

#include "xmppcontact.h"
#include "message.h"
#include "client.h"

XmppContact::XmppContact(Client *client, const QString& j)
	: m_client(client)
{
	m_jid = new Jid(j);
	vcard = new VCard();
}

XmppContact::XmppContact(Client *client, const QString& j, const QString& n)
	: m_client(client)
{
	m_jid = new Jid(j);
	vcard = new VCard();
	vcard->setNickname(n);
}

XmppContact::XmppContact(Client *client, const QString& j, const QString& n, const QString& s)
	: m_client(client)
{
	m_jid = new Jid(j);
	vcard = new VCard();
	vcard->setNickname(n);
	m_subscription = s;
}

XmppContact::XmppContact(Client *client, const char *j)
	: m_client(client)
{
	m_jid = new Jid(j);
	vcard = new VCard();
}

XmppContact::~XmppContact()
{
	delete m_jid;
	delete vcard;
}

void XmppContact::addMessage(const Message& m /*FIXME:should be a Message*/)
{
	if (m.message() == NULL || m.message().isNull() || m.message() == "")
		return;

	emit newMessage(m);
}

void XmppContact::addPresence(const Presence& pr)
{
	qDebug() << "XmppContact<" << m_jid->full() << ">::setPresence(" << pr.from().full() << ")";

	if (pr.type() == "unavailable")
	{
		for (int i = 0; i < m_resources.size(); i++)
		{
			if (pr.from().resource() == m_resources[i].from().resource())
			{
				m_resources.removeAt(i);
				break;
			}
		}

		if (m_resources.size())
			m_jid->setResource(m_resources[0].from().resource());
		else
			m_jid->setResource("");

		emit presenceChanged();

		return;
	}

	if (pr.priority() == -1)
	{
		m_resources << pr;
		m_jid->setResource(m_resources[0].from().resource());
		emit presenceChanged();
		return;
	}

	for (int i = 0; i < m_resources.size(); i++)
	{
		Presence p = m_resources[i];
		if (p.priority() == pr.priority())
		{
			m_resources[i] = pr;
			break;
		}
		else if (p.priority() > pr.priority())
		{
			m_resources.insert(m_resources.begin() + i, pr);
			break;
		}
	}

	if (m_resources.isEmpty())
		m_resources << pr;
	else if (m_resources[0].priority() < pr.priority())
		m_resources.insert(0, pr);

	m_jid->setResource(m_resources[0].from().resource());

	emit presenceChanged();
}

Presence XmppContact::presence(const QString& resource) const
{
	if (m_resources.isEmpty())
		return Presence();

	foreach (Presence pr, m_resources)
	{
		if (pr.from().resource() == resource)
			return pr;
	}

	return m_resources[0];
}

bool XmppContact::isAvailable()
{
	return !m_resources.isEmpty();
}

void XmppContact::setFeatures(QStringList &c)
{
	features = c;
}

VCard *XmppContact::vCard() const
{
	return vcard;
}

void XmppContact::setSubscription(const QString& subscription)
{
	m_subscription = subscription;
}

void XmppContact::sendMessage(const QString &message)
{
	QString to = m_jid->full();
	m_client->sendMessage(to, message);
}

void XmppContact::sendFile(const QString &filename)
{

}

QString XmppContact::prettyName() const
{
	return vCard()->nickname().isEmpty() ? jid()->full() : vCard()->nickname();
}
