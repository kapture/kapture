#ifndef XMPP_CONTACT_H
#define XMPP_CONTACT_H

#include <QObject>

#include "presence.h"
#include "jid.h"
#include "vcard.h"
#include "message.h"

class Client;
class JingleSession;

class XmppContact : public QObject
{
	Q_OBJECT
public:
	XmppContact(Client *client, const QString&);
	XmppContact(Client *client, const QString&, const QString&);
	XmppContact(Client *client, const QString&, const QString&, const QString&);
	XmppContact(Client *client, const char*);
	~XmppContact();

	Jid *jid() const {return m_jid;}

	void addMessage(const Message& /*Message*/); //not a slot ???

	void addPresence(const Presence&);
	//void setResource(QString&);
	void setFeatures(QStringList&);

	bool isAvailable(); // available or unavailable.
	VCard *vCard() const;

	/**
	 * @brief presence
	 * @param resource the resource of which the presence is requested
	 * @return the presence of the resource. If no resource is given,
	 *         the presence of the resource with the highest priority is returned.
	 */
	Presence presence(const QString& resource = QString()) const;
	void setSubscription(const QString&);
	QString subscription() const {return m_subscription;}

	void sendMessage(const QString& message);
	void setTranferFileState(const QString&, int);

	QString prettyName() const;

private:
	Jid *m_jid;
	QList<Presence> m_resources;
	QString m_subscription;

	QStringList features;
	VCard *vcard;

	Client *m_client;

public slots:
	void sendFile(const QString& filename);

signals:
	void newMessage(const Message&);
	void presenceChanged();
	void featuresChanged();
	void incomingFile();
	void jingleSessionReady(JingleSession *session);
};

#endif //XMPP_CONTACT_H
