/*
 *      chatwin.cpp -- Kapture
 *
 *      Copyright (C) 2006-2007
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include <QAbstractScrollArea>
#include <QScrollBar>
#include <QCloseEvent>
#include <QLabel>
#include <QDate>
#include <QTime>
#include <QDebug>
#include <QFileDialog>

#include "chatwin.h"
#include "emoticons.h"
#include "client.h"
#include "contact.h"

#include "jingle/jinglesession.h"

ChatWin::ChatWin(Client *client, Contact *contact)
	: m_client(client), m_contact(contact), m_audioSession(0)
{
	ui.setupUi(this);
	connect(ui.sendBtn, SIGNAL(clicked()), this, SLOT(message()));
	connect(ui.sendFileBtn, SIGNAL(clicked()), this, SLOT(sendFile()));
	connect(ui.audioBtn, SIGNAL(clicked()), this, SLOT(slotStartAudioCall()));
	ui.sendBtn->setEnabled(false);
	connect(ui.messageLine, SIGNAL(textChanged(QString)), this, SLOT(composing(QString)));
	connect(ui.messageLine, SIGNAL(returnPressed()), this, SLOT(message()));

	//m_emoticons = new Emoticons();
}

ChatWin::~ChatWin()
{
}

void ChatWin::slotStartAudioCall()
{
	if (!m_audioSession)
	{
		m_audioSession = m_client->createJingleRtpSession(*(m_contact->contact()->jid()), JingleRtpContent::Audio);
		m_audioSession->initiate();
		connect(m_audioSession, SIGNAL(sessionTerminated(JingleSession::Reason,JingleSession*)), SLOT(audioSessionTerminated(JingleSession::Reason,JingleSession*)));
		ui.audioBtn->setText("Hang up");
	}
	else
	{
		m_audioSession->terminate(JingleSession::ReasonSuccess);
		ui.audioBtn->setText("Call");
		ui.audioBtn->setDisabled(true);
	}

}

void ChatWin::audioSessionTerminated(JingleSession::Reason r, JingleSession *s)
{
	if (s == m_audioSession)
	{
		ui.discutionText->insertHtml(QString("<font color='blue'>[")
					+ QDate::currentDate().toString(Qt::LocaleDate) + QString(" ")
					+ QTime::currentTime().toString() + QString("] ")
					+ QString("Audio call finished. </font><br>"));
		m_audioSession = 0;
		ui.audioBtn->setDisabled(false);
	}

}


void ChatWin::message()
{
	if (ui.messageLine->text() != "")
	{
		QString message = ui.messageLine->text();

		ui.discutionText->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
		//ui.discutionText->insertHtml(e->changeEmoticons(chatWin->ui.messageLine->text()).arg("You"));
		ui.discutionText->insertHtml(QString("<font color='blue'>[")
						+ QDate::currentDate().toString(Qt::LocaleDate) + QString(" ")
						+ QTime::currentTime().toString() + QString("] ")
						+ QString("You said : </font><br>%1<br>").arg(message));
		ui.discutionText->verticalScrollBar()->setValue(ui.discutionText->verticalScrollBar()->maximum());
		ui.messageLine->clear();
		ui.sendBtn->setEnabled(false);

		m_contact->contact()->sendMessage(message);

		//FIXME:should get message confirmation
		//emit sendMessage(ui.messageLine->text());
	}
}

void ChatWin::sendFile()
{
	QStringList files = QFileDialog::getOpenFileNames(this);
	if (files.empty())
		return;
	ui.discutionText->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
	//ui.discutionText->insertHtml(e->changeEmoticons(chatWin->ui.messageLine->text()).arg("You"));
	ui.discutionText->insertHtml(QString("<font color='blue'>[")
					+ QDate::currentDate().toString(Qt::LocaleDate) + QString(" ")
					+ QTime::currentTime().toString() + QString("] ")
				     + QString("Sending the file : </font><br>%1<br>").arg(files[0]));
	ui.discutionText->verticalScrollBar()->setValue(ui.discutionText->verticalScrollBar()->maximum());

	foreach (const QString& filename, files)
		m_contact->contact()->sendFile(filename);
}

void ChatWin::composing(QString text)
{
	if (text.length() > 0)
		ui.sendBtn->setEnabled(true);
	else
		ui.sendBtn->setEnabled(false);
	/*
	 * has to tell the server that client is composing
	 */
}

void ChatWin::writeEvent(const QString& event, EventType et)
{
	if (et == Error)
	{
		ui.discutionText->insertHtml(
		QString("<body bgcolor=\"#00FF55\"><font color='red'> *** [")
			+ QDate::currentDate().toString() + QString(" ")
			+ QTime::currentTime().toString() + QString("] ")
			+ event
			+ QString("</font></body><br>"));	
	}
	else
	{
		ui.discutionText->insertHtml(
		QString("<body bgcolor=\"#00FF55\"><font color='green'> * [")
			+ QDate::currentDate().toString() + QString(" ")
			+ QTime::currentTime().toString() + QString("] ")
			+ event
			+ QString("</font></body><br>"));	
	}
}

void ChatWin::changeEvent(QEvent* event)
{
	// FIXME : Does not work properly
	if (event->type() == QEvent::WindowStateChange || windowState() == Qt::WindowActive)
	{
		qDebug() << "[ChatWin] changeEvent\n";
		emit shown();
	}
}

void ChatWin::closeEvent(QCloseEvent* event)
{
	event->ignore();
	hide();
}
