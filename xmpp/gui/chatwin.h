#ifndef CHATWIN_H
#define CHATWIN_H

#include "ui_chatwin.h"
#include "videowidget.h"
#include "emoticons.h"
#include "jingle/jinglesession.h"

class Client;
class Contact;


class ChatWin : public QWidget
{
	Q_OBJECT
public:
	ChatWin(Client *client, Contact *contact);
	~ChatWin();
	Ui::chatWin ui;
	enum EventType
	{
		Info = 0,
		Error
	};
	void writeEvent(const QString&, ChatWin::EventType et = ChatWin::Info);

public slots:
	void message();
	void sendFile();
	void composing(QString);

private slots:
	void slotStartAudioCall();
	void audioSessionTerminated(JingleSession::Reason,JingleSession*);

signals:
	void shown();

private:
	void changeEvent(QEvent* event);
	void closeEvent(QCloseEvent*);
	VideoWidget *videoWidget;

	Client *m_client;
	Contact *m_contact;
	Emoticons *m_emoticons;
	JingleSession *m_audioSession;

};
#endif //CHATWIN_H
