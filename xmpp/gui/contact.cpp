/*
 *      Kapture
 *
 *      Copyright (C) 2006-2009
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */
#include <QLabel>
#include <QAbstractScrollArea>
#include <QScrollBar>
#include <QDate>
#include <QTime>
#include <QMessageBox>
#include <QDebug>

#include "contact.h"
#include "xmppcontact.h"

Contact::Contact(XmppContact *contact, Client *client)
{
	m_xmppContact = contact;
	m_client = client;
	connect(m_xmppContact, SIGNAL(newMessage(const Message&)), SLOT(newMessage(const Message&)));
	connect(m_xmppContact, SIGNAL(presenceChanged()), SLOT(presenceChanged()));
	connect(m_xmppContact, SIGNAL(jingleSessionReady(JingleSession*)), SLOT(jingleSession(JingleSession*)));
	isChatting = false;
}

Contact::~Contact()
{

}

void Contact::newMessage(const Message& m)
{
	if (m.message().isNull() || m.message().isEmpty())
		return;

	if (!isChatting)
	{
		m_chatWin = new ChatWin(m_client, this);
		m_chatWin->setWindowTitle(m_xmppContact->jid()->full());
		//connect(m_chatWin, SIGNAL(shown()), this, SLOT(updateChatWinTitle())); --> To chatwin...
	}

	m_chatWin->ui.discutionText->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
	m_chatWin->ui.discutionText->insertHtml(QString("<font color='red'>[")
			+ QDate::currentDate().toString(Qt::LocaleDate) + QString(" ")
			+ QTime::currentTime().toString() + QString("] ")
			+ QString("%1 says :</font><br>%2<br>").arg(
				m_xmppContact->vCard()->nickname() == "" ? m_xmppContact->jid()->full() : m_xmppContact->vCard()->nickname()).arg(m.message()));
	m_chatWin->ui.discutionText->verticalScrollBar()->setValue(m_chatWin->ui.discutionText->verticalScrollBar()->maximum());

	if (!m_chatWin->isActiveWindow() && newMessages == 0)
	{
		newMessages++;
		m_chatWin->setWindowTitle(QString("(%1) ").arg(newMessages) + m_xmppContact->jid()->full());
	}

	m_chatWin->show();

	isChatting = true;
}

void Contact::startChat()
{
	if (!isChatting)
	{
		m_chatWin = new ChatWin(m_client, this);
		m_chatWin->setWindowTitle(m_xmppContact->jid()->full());
		connect(m_chatWin, SIGNAL(shown()), this, SLOT(updateChatWinTitle()));

		isChatting = true;
	}

	m_chatWin->show();
}

QString Contact::showToPretty(const QString& show)
{
	if (show.toLower() == "dnd")
		return QString("busy");
	if (show.toLower() == "chat")
		return QString("available to chat");
	if (show.toLower() == "away")
		return QString("away");
	if (show.toLower() == "xa")
		return QString("far far away");
	return QString();
}

void Contact::jingleSession(JingleSession* session)
{
	if (!isChatting)
	{
		m_chatWin = new ChatWin(m_client, this);
		m_chatWin->setWindowTitle(m_xmppContact->jid()->full());
		connect(m_chatWin, SIGNAL(shown()), this, SLOT(updateChatWinTitle()));

		isChatting = true;
	}
	m_chatWin->show();
	int ret = QMessageBox::question(m_chatWin,
					QString("Jingle Video"),
					QString("The contact ") +
					session->to().full() +
					QString(" wants to start a Jingle audio chat with you\nDo you agree ?"),
					QMessageBox::Yes | QMessageBox::No);
	if (ret == QMessageBox::Yes)
		session->accept();
	else
		session->decline();
}

/*void Contact::setPresence(const Presence& pr)
{
	/*
	 * Type = [available, unavailable]
	 * Show = [away, chat, dnd, xa]
	 * Status = Text
	 *
	if (isChatting)
	{
		m_chatWin->ui.discutionText->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
		if (p->type() != pr.type())
			m_chatWin->ui.discutionText->insertHtml(QString("<font color='green'> * %1 is now %2</font><br>").arg(m_xmppContact->jid()->full()).arg(pr.type() == "available" ? "online" : "offline"));
		else
		{
			if (pr.show() != p->show() && pr.show() != "" && pr.status() != "")
				m_chatWin->ui.discutionText->insertHtml(
					QString("<body bgcolor=\"#00FF55\"><font color='green'> * [")
					+ QDate::currentDate().toString() + QString(" ")
					+ QTime::currentTime().toString() + QString("] ")
					+ m_xmppContact->jid()->full()
					+ QString(" is now ")
					+ showToPretty(pr.show())
					+ QString(" [")
					+ pr.status()
					+ QString("]</font></body><br>"));
			else
				m_chatWin->ui.discutionText->insertHtml(
				        QString("<body bgcolor=\"#00FF55\"><font color='green'> * [")
					+ QDate::currentDate().toString() + QString(" ")
					+ QTime::currentTime().toString() + QString("] ")
					+ m_xmppContact->jid()->full()
					+ QString(" is now ")
					+ pr.type() == "available" ? QString("online") : QString("offline")
					+ QString("</font></body><br>"));
		}
		m_chatWin->ui.discutionText->verticalScrollBar()->setValue(m_chatWin->ui.discutionText->verticalScrollBar()->maximum());
	}
	
	p->setType(pr.type());
	p->setShow(pr.show());
	p->setStatus(pr.status());
	qDebug() << "[CONTACT] Presence : type =" << p->type() << ", show = "<< p->show() <<", status ="<< p->status();
	qDebug() << "[CONTACT] WARNING: THIS IS NOT A VCARD NICKNAME !!!! Contact has a new nickname :", vcard->nickname();
}*/

bool Contact::isAvailable()
{
	return m_xmppContact->presence().type() == "available";
}

void Contact::setTranferFileState(QString fileName, int prc)
{
	//printf("prc = %d\n", prc);
	if (prc == 0 && !done)
	{
		//Add the transfer bar
		
		FileTransferWidget *ftw = new FileTransferWidget(fileName);
		transferList.append(ftw);

		m_chatWin->ui.vboxLayout->insertLayout(transferList.count(), transferList.last()->box());
		done = true;
	}
	
	for (int i = 0; i < transferList.count(); i++)
	{
		if (transferList.at(i)->fileName() == fileName)
		{
			transferList.at(i)->setPourcentage(prc);
			if (prc == 100)
			{
				m_chatWin->ui.vboxLayout->removeItem(transferList.at(i)->box());
				delete transferList.at(i);
				transferList.removeAt(i);
				done = false;
			}
			break;
		}
	}
}

void Contact::setEmoticons(Emoticons* emoticons)
{
	e = emoticons;
}

void Contact::presenceChanged()
{

}
