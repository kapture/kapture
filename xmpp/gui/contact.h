#ifndef CONTACT_H
#define CONTACT_H

#include <QObject>
#include <QProgressBar>
#include <QLabel>

#include "presence.h"
#include "emoticons.h"
#include "chatwin.h"
#include "jid.h"
#include "vcard.h"
#include "filetransferwidget.h"
#include "xmppcontact.h"
#include "message.h"

class Contact : public QObject
{
	Q_OBJECT
public:
	Contact(XmppContact *contact, Client *client);

	~Contact();
	
	void startChat();
	bool isAvailable(); // available or unavailable.
	void setTranferFileState(QString, int);
	void setEmoticons(Emoticons*);
	void sendMessage(QString&, QString&);

	XmppContact *contact() const {return m_xmppContact;}

private:
	XmppContact *m_xmppContact;

	ChatWin *m_chatWin;
	bool isChatting;

	QList<FileTransferWidget*> transferList;
	bool done;
	Emoticons *e;
	QString showToPretty(const QString&);
	int newMessages;
	Client *m_client;

private slots:
	void presenceChanged();
	void jingleSession(JingleSession*);

public slots:
	void newMessage(const Message &);
};

#endif //CONTACT_H
