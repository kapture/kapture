/*
 *      Kapture
 *
 *      Copyright (C) 2006-2007
 *          Detlev Casanova (detlev.casanova@gmail.com)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 */

#include <QList>

#include "rostermodel.h"
#include "xmppcontact.h"

RosterModel::RosterModel(QObject *parent)
	: QAbstractTableModel(parent)
{

}

RosterModel::~RosterModel()
{
	
}

/**
 * TODO: pass a reference : const QList<Contact*>& c
 */
void RosterModel::setData(QList<Contact*> *c)
{
	m_contacts = c;
}

void RosterModel::setData(QModelIndex index, QString value)
{
	Q_UNUSED(value);
	Q_UNUSED(index);
}

QString showToPretty(const QString& show)
{
	if (show.toLower() == "dnd")
		return QString("busy");
	if (show.toLower() == "chat")
		return QString("available to chat");
	if (show.toLower() == "away")
		return QString("away");
	if (show.toLower() == "xa")
		return QString("far far away");
	return QString();
}

QVariant RosterModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DecorationRole && index.column() == 0)
	{
		if (m_contacts->at(index.row())->contact()->isAvailable())
		{
			if (m_contacts->at(index.row())->contact()->presence().show() == "")
			{
				if (m_contacts->at(index.row())->contact()->subscription() == "both")
					return QImage(":/icons/pics/online.png");
				if (m_contacts->at(index.row())->contact()->subscription() == "from")
					return QImage(":/icons/pics/online-from.png");
				if (m_contacts->at(index.row())->contact()->subscription() == "to")
					return QImage(":/icons/pics/online-to.png");
				if (m_contacts->at(index.row())->contact()->subscription() == "none")
					return QImage(":/icons/pics/online-none.png");
			}
			if (m_contacts->at(index.row())->contact()->presence().show() == "away")
				return QImage(":/icons/pics/away.png");
			if (m_contacts->at(index.row())->contact()->presence().show() == "chat")
				return QImage(":/icons/pics/chat.png");
			if (m_contacts->at(index.row())->contact()->presence().show() == "xa")
				return QImage(":/icons/pics/xa.png");
			if (m_contacts->at(index.row())->contact()->presence().show() == "dnd")
				return QImage(":/icons/pics/dnd.png");

		}
		else
			return QImage(":/icons/pics/offline.png");
	}
	if (role == Qt::EditRole)
		return false;
	if (role == Qt::DisplayRole)
	{
		QString str;
		switch(index.column())
		{
			case 1: 
			str = m_contacts->at(index.row())->contact()->prettyName();
				//if (contacts->at(index.row())->isAvailable() && contacts->at(index.row())->show() != "")
				//	str = str + QString(" (") + showToPretty(contacts->at(index.row())->show()) + QString(")");
				return str;
				break;
			default :
				return QVariant();
		}
	}
	if (role == Qt::WhatsThisRole)
	{
		return m_contacts->at(index.row())->contact()->jid()->full();
	}

	return QVariant();
}

Qt::ItemFlags RosterModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;
	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant RosterModel::headerData(int, Qt::Orientation, int) const
{
	return QVariant();
}

QModelIndex RosterModel::index(int row, int column, const QModelIndex&) const
{
	return createIndex(row, column, row);
}

int RosterModel::rowCount(const QModelIndex&) const
{
	return m_contacts->count();
}

int RosterModel::columnCount(const QModelIndex&) const
{
	return 2;
}

QList<Contact*> RosterModel::getContactList()
{
	return *m_contacts;
}
