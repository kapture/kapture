#ifndef XMPPCONFIGDIALOG_H
#define XMPPCONFIGDIALOG_H

#include "ui_xmppconfigdialog.h"
#include "profile.h"
#include "xmpp.h"

class WebcamConfigWidget;
class XmppReg;
class ProfileModel;
class Config;
class TaskManager;

class XmppConfigDialog : public QDialog
{
	Q_OBJECT
public:
	XmppConfigDialog();
	~XmppConfigDialog();
public slots:
	void selectChange(QString&);
	void add();
	void regFinished();
	void del();
	void configWebcam();
	void saveConfig();
	void read();
	void registerError(Xmpp::ErrorType);
	void tabChanged(int);

signals:
	void accepted();
private:
	Ui::xmppConfigDialog ui;
	QList<Profile> profiles;
	Config *conf;
	ProfileModel *model;
	QString selectedProfile;
	XmppReg *xmppReg;
	void addProfile(const Profile&);
	void initWebcam();
	void stopWebcam();
	TaskManager *taskManager;
	Xmpp *xmpp;
	WebcamConfigWidget *wcw;
};

#endif
