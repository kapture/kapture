#include <QMessageBox>
#include <QDir>
#include <QFile>
#include <QMenu>
#include <QInputDialog>

#include "xmppwin.h"
#include "message.h"
#include "presence.h"
#include "joystick.h"
#include "filetransferhandler.h"

XmppWin::XmppWin()
	: m_jid(0), m_client(0), m_rosterModel(0), m_rosterDelegate(0), m_roster(0), m_waitingTimer(0)
{
	ui.setupUi(this);

	QIcon icon(":/icons/pics/16x16.png");
	setWindowIcon(icon);

	ui.statusBox->setCurrentIndex(Offline);

	connect(ui.statusBox, SIGNAL(currentIndexChanged(int)), this, SLOT(statusChanged()));
	connect(ui.configBtn, SIGNAL(clicked()), this, SLOT(showConfigDial()));

	ui.tableView->verticalHeader()->hide();
	ui.tableView->horizontalHeader()->hide();
	ui.tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.tableView->setSelectionMode(QAbstractItemView::SingleSelection);

	ui.addItemBtn->setEnabled(false);

	QPixmap pixmap = QPixmap(":/icons/pics/decrypted.png");
	ui.tlsIconLabel->setToolTip(tr("You are not connected right now."));
	ui.tlsIconLabel->setPixmap(pixmap);
	ui.tlsIconLabel->setEnabled(false);

	// Loads predifined Profiles.
	conf = new Config();
	
	connect(ui.profilesComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProfile(int)));
	secs = 0;
	emoticons = new Emoticons(this);
	connectionStatus = Offline;

	useSystemTray = conf->useSystemTray();
	res = conf->resource();
	if (useSystemTray)
	{
		/* Concerns QSystemTrayIcon */
		sti = new QSystemTrayIcon();
		sti->setIcon(QIcon(":/icons/pics/offline.png"));
		sti->setToolTip(QString("Kapture"));

		sysTrayMenu = new QMenu(0);
		sysTrayMenu->setTitle(QString("Kapture -- ") + pJid.full());

		for (int i = 0; i < conf->profileList().count(); i++)
		{
			/*QAction *contactActionMenu = */sysTrayMenu->addAction(conf->profileList().at(i).name());
			//connect(contactActionMenu, SIGNAL(triggered()), this, SLOT(jabberConnect()));
		}

		sysTrayMenu->addSeparator();

		QAction *sQuitAction = sysTrayMenu->addAction(QString("Quit"));
		connect(sQuitAction, SIGNAL(triggered()), this, SLOT(quitApp()));

		sysTrayMenu->setTitle("Kapture");
		sti->setContextMenu(sysTrayMenu);

		connect(sti, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), SLOT(systrayActivated(QSystemTrayIcon::ActivationReason)));

		sti->show();
	}
	else
		sti = 0;

	QTimer::singleShot(1, this, SLOT(checkConfig()));

	// Concerns Joystick.
	//Joystick *js0 = new Joystick("/dev/js0");
}

XmppWin::~XmppWin()
{
	delete conf;
	delete sti;
	delete m_rosterDelegate;
	delete m_rosterModel;
}

void XmppWin::checkConfig()
{
	if (conf->noConfig)
	{
		QMessageBox::information(this, tr("Configuration"),
			tr("It seems that it is the first time you"
			"run the Jabber part of Kapture. Let us add an account."),
			QMessageBox::Ok);
		showConfigDial();
	}

	updateConfig();
}

void XmppWin::systrayActivated(QSystemTrayIcon::ActivationReason reason)
{
	if (reason != QSystemTrayIcon::Trigger)
		return;

	if (!isVisible())
		show();
	else
		hide();
}

void XmppWin::quitApp()
{
	printf("\n * Exiting...\n");

	if (connectionStatus != Offline)
		jabberDisconnect();

	qApp->quit();
}

void XmppWin::statusChanged()
{
	connectionStatus = (State)ui.statusBox->currentIndex();
	setPresence();
}

void XmppWin::setPresence()
{
	QString show;
	QString status = "";
	switch(connectionStatus)
	{
	case Away:
		show = "away";
		if (sti)
			sti->setIcon(QIcon(":/icons/pics/away.png"));
		break;
	case Chat:
		show = "chat";
		if (sti)
			sti->setIcon(QIcon(":/icons/pics/chat.png"));
		break;
	case Dnd:
		show = "dnd";
		if (sti)
			sti->setIcon(QIcon(":/icons/pics/dnd.png"));
		break;
	case Xa:
		show = "xa";
		if (sti)
			sti->setIcon(QIcon(":/icons/pics/xa.png"));
		break;
	case Online:
		show = "";
		if (sti)
			sti->setIcon(QIcon(":/icons/pics/online.png"));
		break;
	case Offline:
		show = "";
		if (sti)
			sti->setIcon(QIcon(":/icons/pics/offline.png"));
		jabberDisconnect();
		return;
		break;
	default:
		show = "";
		if (sti)
			sti->setIcon(QIcon(":/icons/pics/online.png"));
	}

	if (!m_client) {
		if (jabberConnect() != 0)
			return;
	}

	m_client->setPresence(show, status);
}

/*!
 * Connects and authenticates the User to the server.
 */

int XmppWin::jabberConnect()
{
	if (!pJid.isValid())
	{
		QMessageBox::critical(this, tr("Jabber"), tr("There is no configration yet, cannot connect."), QMessageBox::Ok);
		return -1;
	}

	delete m_jid;
	m_jid = new Jid(pJid.bare() + QString("/") + res);
	
	if (!m_jid->isValid())
	{
		QMessageBox::critical(this, tr("Jabber"), tr("This is an invalid Jid."), QMessageBox::Ok);
		jabberDisconnect();
		printf("Invalid jid !\n");
		return -1;
	}
	
	delete m_client;
	m_client = new Client(*m_jid, serverEdit, portEdit);

	connect(m_client, SIGNAL(error(Xmpp::ErrorType)), SLOT(error(Xmpp::ErrorType)));
	connect(m_client, SIGNAL(connected()), SLOT(clientAuthenticated()));

	m_client->setResource(m_jid->resource());
	m_client->setPassword(password);
	m_client->authenticate();
	QList<JinglePayloadType*> payloads;
	payloads << new JinglePayloadType(110, "speex", 16000, 2);
	//TODO : memory-manage me !
	m_client->setJingleSupportedPayloads(payloads, JingleRtpContent::Audio);

	delete m_waitingTimer;
	m_waitingTimer = new QTimer(this);
	connect(m_waitingTimer, SIGNAL(timeout()), this, SLOT(connectingLogo()));
	m_waitingTimer->start(1000);

	return 0;
}

void XmppWin::connectingLogo()
{
	secs++;

	QString title = "Kapture -- Connecting";

	for (int i = 0; i < (secs % 5); i++)
	{
		title.append(".");
	}

	setWindowTitle(title);
}

void XmppWin::jabberDisconnect()
{
	delete m_client;
	delete m_jid;
	delete m_waitingTimer;

	m_client = 0;
	m_jid = 0;
	m_waitingTimer = 0;

	ui.tlsIconLabel->setEnabled(false);

	setWindowTitle("Kapture [Offline]");

	connectionStatus = Offline;

	if (sti)
		sti->setIcon(QIcon(":/icons/pics/offline.png"));

	if (!m_rosterModel)
		return;

	Presence pr(QString(""), QString(""), QString(""));

	for (int i = 0; i < contactList.count(); i++)
	{
		contactList[i]->contact()->addPresence(pr);
	}

	m_rosterModel->setData(&contactList);

	for (int i = 0; i < contactList.count(); i++)
	{
		ui.tableView->update(m_rosterModel->index(i, 0));
		ui.tableView->update(m_rosterModel->index(i, 1));
	}
}

void XmppWin::clientAuthenticated()
{
	QPixmap pixmap;

	if (m_client->isSecured())
	{
		pixmap = QPixmap(":/icons/pics/encrypted.png");
		ui.tlsIconLabel->setToolTip(tr("The connection with the server is encrypted."));
	}
	else
	{
		pixmap = QPixmap(":/icons/pics/decrypted.png");
		ui.tlsIconLabel->setToolTip(tr("The connection with the server is *not* encrypted."));
	}
	ui.tlsIconLabel->setPixmap(pixmap);
	ui.tlsIconLabel->setEnabled(true);

	connect(m_client, SIGNAL(rosterReady(Roster)), this, SLOT(setRoster(Roster)));
	//connect(m_client, SIGNAL(presenceReady(const Presence&)), this, SLOT(processPresence(const Presence&)));
	//connect(m_client, SIGNAL(messageReady(const Message&)), this, SLOT(processMessage(const Message&)));
	//connect(m_client, SIGNAL(signalUpdateItem(Contact*)), SLOT(updateRosterItem(Contact*)));
	//connect(m_client, SIGNAL(newJingleSessionReady()), SLOT(newJingleSessionReady()));

	m_client->getRoster();

	connectionStatus = Online;

	if (sti)
	{
		sti->setIcon(QIcon(":/icons/pics/online.png"));
		sti->showMessage("Kapture - Connected", QString("You are now connected with %1.").arg(pJid.full()));
	}
}

/*void XmppWin::newJingleSessionReady()
{
	js = client->getNextPendingJingleSession();
	// js should be in a list so all incoming connections are in the list.
	if (contactWithJid(js->to())->askForJingleStart(js->to(), "Video"))
	{
		js->startReceive();
	}
	else
	{
		js->decline();
	}
}*/

void XmppWin::updateRosterItem(Contact *c)
{
	/*printf("[XMPPWIN] updateRosterItem();\n");
	Contact *contact = contactWithJid(*(c->jid));
	if (contact == NULL)
	{
		printf("[XMPPWIN] Add %s\n", c->jid->full().toLatin1().constData());
		c->setEmoticons(emoticons);
		connect(c, SIGNAL(sendMessage(QString&, QString&)), this, SLOT(sendMessage(QString&, QString&)));
		connect(c, SIGNAL(sendFileSignal(QString&)), this, SLOT(sendFile(QString&)));
		//connect(c, SIGNAL(sendVideo(QString&)), this, SLOT(sendVideo(QString&)));

		contactList << c;
		sortContactList();
		m->setData(&contactList);
		ui.tableView->setModel(m);
		for (int j = 0; j < contactList.count(); j++)
		{
			ui.tableView->update(m->index(j, 0));
			ui.tableView->update(m->index(j, 1));
		}
		ui.tableView->resizeColumnsToContents();
	}
	else
	{
		printf("[XMPPWIN] Update %s\n", c->jid->full().toLatin1().constData());
		contact->setSubscription(c->subscription());
	}
*/
	// TODO:Update contacts features (DISCOINFO)
}

#include <math.h>
#define PI (3.141592653589793)

void XmppWin::setRoster(Roster roster)
{
	/*
	 * FIXME:
	 * setRoster should behave like updateRosterItem() as when disconnecting and reconnecting,
	 * the whole contactList is rebuild and actives contacts windows become obsolete.
	 * So, if a contact already exists when receiving the roster, just update it.
	 * Otherwise, add it to the contact list. If we are reconnecting (find a way to know it, for example, contact list not empty), contacts
	 * present in the list but not present in the new received roster should be removed (and their window eventually cloased)
	 */
	m_roster = roster;
	m_rosterModel = new RosterModel();
	m_rosterDelegate = new ContactWidgetDelegate();

	//Add new contacts
	QList<XmppContact*> xmppList = m_roster.contactList();
	for (int i = 0; i < xmppList.count(); i++)
	{
		Contact *newContact = new Contact(xmppList[i], m_client);
		bool found = false;
		for (int j = 0; j < contactList.count(); j++)
		{
			if (xmppList[i]->jid()->bare() == contactList[j]->contact()->jid()->bare())
			{
				found = true;
				break;
			}
		}

		if (!found)
		{
			newContact->setEmoticons(emoticons);
			//connect(newList[i], SIGNAL(sendMessage(QString&, QString&)), this, SLOT(sendMessage(QString&, QString&)));
			//connect(newList[i], SIGNAL(sendFileSignal(QString&)), this, SLOT(sendFile(QString&)));
			//connect(newList[i], SIGNAL(sendVideo(QString&)), this, SLOT(sendVideo(QString&)));
			connect(xmppList[i], SIGNAL(presenceChanged()), SLOT(processPresence()));
			contactList << newContact;
		}
	}
	
	//Delete old ones
	for (int i = 0; i < contactList.count(); i++)
	{
		bool found = false;
		for (int j = 0; j < xmppList.count(); j++)
		{
			if (xmppList[j]->jid()->bare() == contactList[i]->contact()->jid()->bare())
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			contactList.takeAt(i);
			// A contact has been removed, we must check the one which has taken it's place.
			i--;
		}
	}

	gScene = new QGraphicsScene(ui.graphicsView);
	
	//FIXME:Only if not already connected :
	connect(ui.tableView, SIGNAL(doubleClicked(const Jid&)), this, SLOT(startChat(const Jid&)));
	connect(ui.tableView, SIGNAL(rightClick(const Jid&, const QPoint&)), this, SLOT(showMenu(const Jid&, const QPoint&)));

	m_waitingTimer->stop();
	setWindowTitle("Kapture -- " + m_jid->full());
	ui.addItemBtn->setEnabled(true);
	connect(ui.addItemBtn, SIGNAL(clicked()), SLOT(addItem()));

	ui.graphicsView->setScene(gScene);

	sortContactList();
	m_rosterModel->setData(&contactList);
	ui.tableView->setModel(m_rosterModel);
	ui.tableView->setItemDelegate(m_rosterDelegate);
	
	ui.tableView->setColumnWidth(0, 22);
	ui.tableView->resizeColumnsToContents();
}

void XmppWin::addItem()
{
	bool ok;
	QString text = QInputDialog::getText(this, tr("Adding a contact"), tr("Jid [e.g. cazou88@jabber.org] :"), QLineEdit::Normal, QString(), &ok);
	if (ok && Jid(text).isValid())
	{
		m_client->addItem(Jid(text), QString(""), QString("")); //FIXME:Should create a dialog to get the name and the group.
	}
}

Contact* XmppWin::contactWithJid(const Jid& cJid)
{
	for (int i = 0; i < contactList.count(); i++)
	{
		if (contactList[i]->contact()->jid()->equals(cJid))
			return contactList[i];
	}
	return NULL;
}

void XmppWin::showMenu(const Jid& to, const QPoint& point)
{
	menuTo = to;
	printf("[XMPPWIN] Show Menu.\n");
	menu = new QMenu(0);
	menu->setTitle(to.full());
	QAction *vCardAction = menu->addAction("View vCard");
	connect(vCardAction, SIGNAL(triggered()), this, SLOT(showvCard()));
	menu->addSeparator();
	QAction *sChatAction = menu->addAction(QString("Start chat with ") + to.full());
	if (contactWithJid(to)->isAvailable())
		connect(sChatAction, SIGNAL(triggered()), this, SLOT(startChatFromMenu()));
	else
		sChatAction->setEnabled(false);

	QMenu *mSubsMenu = new QMenu(0);
	mSubsMenu->setTitle("Manage Subscriptions");
	menu->addMenu(mSubsMenu);
	QAction *delAction = menu->addAction("Delete contact");
	connect(delAction, SIGNAL(triggered()), SLOT(delItem()));

	if (contactWithJid(to)->contact()->subscription() == "both")
	{
		QAction *addAuthAction = mSubsMenu->addAction(QString("Resend authorization to ") + to.full());
		connect(addAuthAction, SIGNAL(triggered()), this, SLOT(slotAddAuth()));
		QAction *remAuthAction = mSubsMenu->addAction(QString("Remove authorization of ") + to.full());
		connect(remAuthAction, SIGNAL(triggered()), this, SLOT(slotRemAuth()));
	}

	if (contactWithJid(to)->contact()->subscription() == "to")
	{
		QAction *addAuthAction = mSubsMenu->addAction(QString("Resend authorization to ") + to.full());
		connect(addAuthAction, SIGNAL(triggered()), this, SLOT(slotAddAuth()));
	}

	if (contactWithJid(to)->contact()->subscription() == "from")
	{
		QAction *reqAuthAction = mSubsMenu->addAction(QString("Request authorization from ") + to.full());
		connect(reqAuthAction, SIGNAL(triggered()), this, SLOT(slotReqAuth()));
		QAction *remAuthAction = mSubsMenu->addAction(QString("Remove authorization of ") + to.full());
		connect(remAuthAction, SIGNAL(triggered()), this, SLOT(slotRemAuth()));
	}
	
	if (contactWithJid(to)->contact()->subscription() == "none")
	{
		QAction *addAuthAction = mSubsMenu->addAction(QString("Resend authorization to ") + to.full());
		connect(addAuthAction, SIGNAL(triggered()), this, SLOT(slotAddAuth()));
		QAction *reqAuthAction = mSubsMenu->addAction(QString("Request authorization from ") + to.full());
		connect(reqAuthAction, SIGNAL(triggered()), this, SLOT(slotReqAuth()));
	}
	menu->popup(point);
	menu->show();
	//FIXME:The menu should be destroid when it is clicked.
}

void XmppWin::delItem()
{
	m_client->delItem(menuTo);
}

void XmppWin::slotAddAuth()
{
	m_client->addAuthFor(menuTo.full());
	delete menu;
}

void XmppWin::slotRemAuth()
{
	m_client->removeAuthFor(menuTo.full());
	delete menu;
}

void XmppWin::slotReqAuth()
{
	m_client->requestAuthFor(menuTo.full());
	delete menu;
}

void XmppWin::startChatFromMenu()
{
	startChat(menuTo.full());
	delete menu;
}

void XmppWin::showvCard()
{
	printf("[XMPPWIN] Show vCard of %s\n", menuTo.full().toLatin1().constData());
}

/*void XmppWin::sendVideo(QString& to)
{
	client->sendVideo(to);
}*/

bool sortFct(Contact *c1, Contact *c2)
{
	if (c1->contact()->presence() == c2->contact()->presence())
		return c1->contact()->prettyName() >= c2->contact()->prettyName();

	return c1->contact()->presence() >= c2->contact()->presence();
}

void XmppWin::sortContactList()
{
	qSort(contactList.begin(), contactList.end(), sortFct);
}

void XmppWin::processPresence()
{
	// Reset contactList order.
	sortContactList();

	//FIXME:update the view properly...
	ui.tableView->update();

	/*
	int i;
	// FIXME:There is no feedback in the chatWin of a contact when it becomes available.

	// Looking for the contact in the contactList.
	for (i = 0; i < contactList.count(); i++)
	{
		if (contactList[i]->contact()->jid()->equals(presence.from()))
		{
			// Check if the presence has changed.
			if (contactList[i]->contact()->presence().type() != presence.type())
			{
				sti->showMessage("Kapture", 
					QString("%1 is now %2").arg(presence.from().full()).arg(presence.type() == "unavailable" ? "Offline" : "Online"));
			}

			// Set eventual new resource for this contact.
			// FIXME: should be done only if the contact has no resource yet.
			// TODO: should add the resource to a resourceList for this contact as it can be connected with more than 1 resource.
			contactList[i]->contact()->jid()->setResource(presence.from().resource());

			// Set contact's presence.
			contactList[i]->contact()->setPresence(presence);

			// Updating Table View.
			//m->setData(contactList);
			for (int j = 0; j < contactList.count(); j++)
			{
				ui.tableView->update(m_rosterModel->index(j, 0));
				ui.tableView->update(m_rosterModel->index(j, 1));
			}
			ui.tableView->resizeColumnsToContents();
			break;
		}
	}
	*/
}

void XmppWin::processMessage(const Message& m)
{
	/*for (int i = 0; i < contactList.count(); i++)
	{
		if (contactList[i]->contact()->jid()->equals(m.from()))
		{
			contactList[i]->newMessage(m);
		}
	}*/
}

void XmppWin::sendMessage(QString &to, QString &message)
{
	if (connectionStatus != Offline)
		m_client->sendMessage(to, message);
	else
		QMessageBox::critical(this, tr("Jabber"), tr("You are not logged in right now !!!"), QMessageBox::Ok);
}

void XmppWin::startChat(const Jid& to)
{
	for (int i = 0; i < contactList.count(); i++)
	{
		if (contactList[i]->contact()->jid()->equals(to))
		{
			contactList[i]->startChat();
		}
	}
}

void XmppWin::error(Xmpp::ErrorType e)
{
	// Still a lot of errors to manage...
	// Errors from the authentification process.
	if (m_waitingTimer->isActive())
		m_waitingTimer->stop();
	switch (e)
	{
	case Xmpp::UnsupportedMechanisms:
		QMessageBox::critical(this, tr("Jabber"), tr("An error occured while connecting : \nThe server doesn't support authentification."), QMessageBox::Ok);
		break;
	case Xmpp::BadUsernameOrPassword:
		QMessageBox::critical(this, tr("Jabber"), tr("An error occured while connecting : \nCheck username and password."), QMessageBox::Ok);
		break;
	case Xmpp::HostNotFound:
		QMessageBox::critical(this, tr("Jabber"), tr("An error occured while connecting : \nHost not found."), QMessageBox::Ok);
		break;
	default :
		QMessageBox::critical(this, tr("Jabber"), tr("An unknown error occured while connecting."), QMessageBox::Ok);
	}

	ui.statusBox->setCurrentIndex(Offline);

	jabberDisconnect();
}

void XmppWin::showConfigDial()
{
	XmppConfigDialog *dial = new XmppConfigDialog();
	dial->show();
	connect(dial, SIGNAL(accepted()), this, SLOT(updateConfig()));
}

void XmppWin::changeProfile(int p)
{
	pJid = profileList[p].jid();
	password = profileList[p].password();
	serverEdit = profileList[p].personnalServer();
	portEdit = profileList[p].port();
}

void XmppWin::updateConfig()
{
	// Update Profiles
	// Loads predifined Profiles.
	delete conf;
	disconnect(ui.profilesComboBox, SIGNAL(currentIndexChanged(int)), 0, 0);
	ui.profilesComboBox->clear();
	connect(ui.profilesComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeProfile(int)));
	conf = new Config();
	if (conf->noConfig)
		return;
	
	profileList = conf->profileList();
	
	for (int i = 0; i < profileList.count(); i++)
	{
		ui.profilesComboBox->addItem(profileList[i].name());
	}
	
	if (profileList.count() > 0)
	{
		changeProfile(0);
	}
}

void XmppWin::sendFile(QString &to)
{
	FileTransferHandler *ftHandler = m_client->createFileTransferHandler(to);
	//connect(ftHandler, SIGNAL(prctChanged()), prcentChanged()); //TODO:FIXME
}

void XmppWin::contactFeaturesSave(Xmpp::ContactFeatures c)
{
	int i = 0;
	while (!(c.jid == contactList[i]->contact()->jid()))
		i++;
	contactList[i]->contact()->setFeatures(c.features);
}

void XmppWin::prcentChanged(Jid& jid, QString& fileName, int prc)
{
	for (int i = 0; i < contactList.count(); i++)
	{
		if (contactList[i]->contact()->jid()->equals(jid))
		{
			contactList[i]->setTranferFileState(fileName, prc); //FileName should really be an ID
			break;
		}
	}
}

void XmppWin::closeEvent(QCloseEvent* event)
{
	if (!useSystemTray)
	{
		printf("\n * Exiting...\n");
		if (connectionStatus != Offline)
			jabberDisconnect();
		qApp->quit();
	}
	else
	{
		event->ignore();
		hide();
	}
}
